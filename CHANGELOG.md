CHANGELOG
=========

2020-11-18 (1.0.3)
------------------
- updated documentation
- removed not-working badge from README

2020-11-18 (1.0.2)
------------------
- fixed broken version dependency: `netsquid-nv` should have version 5.0.1 because 5.0.2 relies on a newer version of `netsquid-magic` (higher major version number) than specified in `requirements.txt`

2020-11-18 (1.0.1)
------------------
- added configuration file for the CI to work
- replaced .gitignore by the one from the [snippet template](https://gitlab.com/softwarequtech/netsquid-snippets/NetSquid-SnippetTemplate/-/blob/master/%7B%7Bcookiecutter.project_slug%7D%7D/.gitignore)
- added .bumpversion.cfg so that we can use bumpversion application for easy version bumping
- added badges to the README file

2020-11-01 (1.0.0)
------------------
- Snippet release
