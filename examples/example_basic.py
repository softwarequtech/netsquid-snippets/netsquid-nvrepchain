"""
Runs a single simulation of a 5-node repeater chain (including end nodes, i.e. 3 repeaters) and
prints the mean fidelity of the first produced entanglement between the end nodes of the chain
to the console.

Usage
-----
python3 example_basic.py
"""
from netsquid_nvrepchain.runtools.run_simulation import run_many_simulations
from netsquid_nvrepchain.runtools.data_collection_tools import get_statetapper_between_fixed_nodes


def run_simulation(number_of_runs, **sim_setup_parameters):
    """
    Runs simulations of an NV-based repeater chain.

    Parameters
    ----------
    number_of_runs: int
        Number of simulations to run
    sim_setup_parameters: dict
        Dictionary of the parameters that
        :fn:`~netsquid_nvrepchain.runtools.run_simulation.run_many_simulations` takes.

    Returns
    -------
    float
        Fidelity of the first produced entanglement between the end nodes.
    """
    # obtain a SimulationSetup object, which is a tool for running simulations
    sim_setup = run_many_simulations(**sim_setup_parameters)

    # Ensuring the simulation stops as soon as a single link has been created
    node_index_A = 0
    node_index_B = sim_setup.ln.number_of_nodes - 1
    for (nodeX, nodeY) in [(node_index_A, node_index_B), (node_index_B, node_index_A)]:
        sim_setup.protocols[nodeX]["logic"].should_stop_fn = \
            lambda nodeX=nodeX, nodeY=nodeY: \
            len(sim_setup.protocols[nodeX]["memmanager"].getAllEntanglementWith(nodeY)) > 0
    sim_setup.set_automatic_stop()

    # Add data collection tools: recording fidelity of the produced end-to-end link once the protocol has finished
    statetapper = get_statetapper_between_fixed_nodes(sim_setup=sim_setup)
    sim_setup.add_datatapper(statetapper)

    # Running the simulation(s)
    sim_setup.perform_many_runs(number_of_runs=number_of_runs, suppress_output=True)

    # Return the collected data during the simulation
    # as a Panda's DataFrame
    return statetapper.dataframe


def main(no_output=False):

    # The parameters that go into the simulation.
    # Each parameter has a brief description; for a more detailed
    # description of each of the hardware parameters of the components
    # of the chain, see the file `nv_parameter_set` in the NetSquid-NV snippet.

    sim_setup_parameters = {

        # number of nodes in the repeater chain, including end nodes
        "number_of_nodes": 5,

        # distance between the nodes
        "internode_distance": 125,

        # bright-state parameter
        "alpha": 0.000001,

        # number of qubits per NV
        "tot_num_qubits": 100,

        # ----------------------------------------------- #
        # Parameters that determine the quality of the NV:
        # ----------------------------------------------- #

        # initialization error of the electron spin
        "electron_init_depolar_prob": 0,

        # error of the single-qubit gate
        "electron_single_qubit_depolar_prob": 0,

        # measurement errors (prob_error_X is the probability that outcome X is flipped to 1 - X)
        "prob_error_0": 0,
        "prob_error_1": 0,

        # initialization error of the carbon nuclear spin
        "carbon_init_depolar_prob": 0,

        # error of the Z-rotation gate on the carbon nuclear spin
        "carbon_z_rot_depolar_prob": 0,

        # error of the native NV two-qubit gate
        "ec_gate_depolar_prob": 0.0,

        # coherence times
        "electron_T1": 0,
        "electron_T2": 0,
        "carbon_T1": 0,
        "carbon_T2": 0,

        # Parameters that determine the phase on the nuclear memory qubits induced by performing an
        # attempt at generating entanglement with the electron spin
        "product_tau_decay_delta_w": 0,
        "initial_nuclear_phase": 0,

        # time until the photon is emitted from the NV
        "photon_emission_delay": 3800,

        # probability that the NV is excited twice
        "p_double_exc": 0,

        "p_fail_class_corr": 0,
        "coherent_phase": 0,

        # probability that a photon is detected, excluding fibre loss,
        # in the presence of an optical cavity
        "prob_detect_excl_transmission_with_conversion_with_cavities": 1,

        # ...and in the absence of cavities (currently unused)
        "prob_detect_excl_transmission_no_conversion_no_cavities": 1,

        # ----------------------------------------- #
        # Parameters of other hardware in the chain
        # ----------------------------------------- #
        "visibility": 1,

        # phase drift of the interferometer
        "std_phase_interferometric_drift": 0,

        # dark count probability
        "prob_dark_count": 0,

        # attenuation of the fibre
        "p_loss_lengths_with_conversion": 0,

        # ----------------------------------------- #
        # Parameters of other hardware in the chain
        # ----------------------------------------- #

        # use of the SWAP-ASAP protocol (no distillation, value=1)
        # or a nested protocol with distillation at each nesting level (value=0, only works in number of nodes
        # equal 2^n + 1 for some integer n>1)
        "use_swap_only": 1,

        # parameter that determines when an entanglement swap should be performed (should not change)
        "swap_fidelity_threshold": 0.8}

    # Run simulation(s) with the above parameters and print the mean fidelity
    # of the produced end-to-end link to the console
    collected_data = run_simulation(number_of_runs=1, **sim_setup_parameters)
    print(collected_data['fidelity'].mean())


if __name__ == "__main__":
    main(no_output=False)
