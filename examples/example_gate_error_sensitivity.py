"""
Runs simulations of a 5-node repeater chain (including end nodes, i.e. 3 repeaters) with varying error size
of the NV-two qubit gate, and outputs the resulting fidelity.


Usage
-----
python3 example_gate_error_sensitivity.py
"""
import numpy as np
from example_basic import run_simulation


def main(no_output=False):

    # The parameters that are inputted into the simulation.
    # For a description, see `examples/example_basic.py` and
    # file `nv_parameter_set` in the NetSquid-NV snippet.
    sim_setup_parameters_except_for_ec_gate_error = {
        "tot_num_qubits": 100,
        "number_of_nodes": 5,
        "internode_distance": 125,
        "electron_init_depolar_prob": 0,
        "electron_single_qubit_depolar_prob": 0,
        "prob_error_0": 0,
        "prob_error_1": 0,
        "carbon_init_depolar_prob": 0,
        "carbon_z_rot_depolar_prob": 0,
        "electron_T1": 0,
        "electron_T2": 0,
        "carbon_T1": 0,
        "carbon_T2": 0,
        "visibility": 1,
        "product_tau_decay_delta_w": 0,
        "photon_emission_delay": 3800,
        "p_double_exc": 0,
        "p_fail_class_corr": 0,
        "coherent_phase": 0,
        "std_phase_interferometric_drift": 0,
        "initial_nuclear_phase": 0,
        "alpha": 0.000001,
        "swap_fidelity_threshold": 0.8,
        "use_swap_only": 1,
        "prob_dark_count": 0,
        "prob_detect_excl_transmission_with_conversion_with_cavities": 1,
        "prob_detect_excl_transmission_no_conversion_no_cavities": 1,
        "p_loss_lengths_with_conversion": 0
    }

    # define the gate error values we want to investigate
    ec_gate_depolar_probs = np.linspace(start=0.0, stop=0.02, num=5)

    # sweep over the gate error values
    fidelities = []
    times = []
    for ec_gate_depolar_prob in ec_gate_depolar_probs:
        collected_data = \
            run_simulation(number_of_runs=10,
                           **sim_setup_parameters_except_for_ec_gate_error,
                           ec_gate_depolar_prob=ec_gate_depolar_prob)
        fidelity = collected_data['fidelity'].mean()
        time = collected_data['time'].mean()
        fidelities.append(fidelity)
        times.append(time)

    if not no_output:
        # print the result to the console
        print('ec_gate_depolar_prob fidelity rate')
        for ec_gate_depolar_prob, fidelity, time in zip(ec_gate_depolar_probs, fidelities, times):
            print('{} {} {}'.format(ec_gate_depolar_prob, fidelity, 1 / time))


if __name__ == "__main__":
    main(no_output=False)
