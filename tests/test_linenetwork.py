from netsquid_nvrepchain.network.linenetwork import LineNetwork
import unittest
from netsquid.components.cchannel import ClassicalChannel
from netsquid.nodes.node import Node
from netsquid.components.qprocessor import QuantumProcessor
import netsquid as ns
from netsquid_nvrepchain.utils.logging_tools import logging
from netsquid_nvrepchain.utils.repchain_parameter_tools import get_nv_dict_perfect_version, filter_on_qmem_config_params


class Test_LineNetwork(unittest.TestCase):

    def setUp(self):
        ns.set_qstate_formalism(ns.QFormalism.DM)
        self.qmem_config_params = \
            get_nv_dict_perfect_version(filter_on_qmem_config_params)

    def test_init(self):
        for (connlistname, connection_cls) in [("class_channels_receive", ClassicalChannel),
                                               ("class_channels_send", ClassicalChannel),
                                               # ("heralded_connections", MiddleHeraldedFibreConnection)
                                               ]:
            self.check_init(connlistname=connlistname,
                            connection_cls=connection_cls)

    def check_init(self, connlistname, connection_cls):
        for number_of_nodes in range(0, 10):
            ln = LineNetwork(number_of_nodes=number_of_nodes,
                             qmem_config_params=self.qmem_config_params)
            self.assertEqual(len(ln.qnodes), number_of_nodes)
            for qnode in ln.qnodes:
                self.assertTrue(isinstance(qnode, Node))

            for index, qnode in enumerate(ln.qnodes):
                # assert every node has a QuantumProcessingDevice
                self.assertTrue(isinstance(qnode.qmemory, QuantumProcessor))
                self._assert_connections_correct(ln=ln,
                                                 number_of_nodes=number_of_nodes,
                                                 index=index,
                                                 connlistname=connlistname,
                                                 connection_cls=connection_cls)

    def _assert_connections_correct(self, connlistname, ln, number_of_nodes, index, connection_cls):
        """Assert the every node has the right amount and kind of connections"""
        connlist = getattr(ln, connlistname)
        logging.debug("ConnectionList:{}".format(connlist))
        for direction in ["L", "R"]:
            logging.info("Num:{}\tIndex:{}\tDir:{}\tConnectionClass:{}".format(
                number_of_nodes, index, direction, connection_cls))
            if (index, direction) in [(0, "L"), (number_of_nodes - 1, "R")]:
                direction not in list(connlist[index].keys())
            else:
                self.assertTrue(isinstance(connlist[index][direction], connection_cls))


if __name__ == "__main__":
    unittest.main()
