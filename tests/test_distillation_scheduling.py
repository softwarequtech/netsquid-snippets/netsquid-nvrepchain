import unittest
from netsquid_nvrepchain.logic.distillation_scheduling import GreedyDistillationStrategy, BandedStrategy
from netsquid_nvrepchain.utils.memorymanager import Link


class TryoutGreedyStrategy(GreedyDistillationStrategy):
    pass


class Test_Distillation_Scheduling(unittest.TestCase):

    LINKS = [Link(remote_nodeID=0, linkID=k) for k in range(10)]
    for link in LINKS:
        link.estimated_fidelity = link.linkID / 10.

    def test_greedy(self):
        gs = TryoutGreedyStrategy(lower_threshold=0., upper_threshold=1.1)
        links = self.LINKS

        # inputting no links doesn't give any links in return
        self.assertEqual(gs.choose_links_to_distill(links=[]), [])

        # checking boundary cases where second link is always/never better than first
        for (expected_output, ret) in [([links[-1], links[-2]], True),
                                       ([links[0], links[1]], False)]:

            gs._majorizes = lambda fidelityA, fidelityB: ret
            self.assertEqual(gs.choose_links_to_distill(links=links), expected_output)

        # checking boundary cases where link should always have fidelity between
        # 0.3 and 0.4.
        gs._majorizes = lambda fidelityA, fidelityB: fidelityA >= 0.3 and fidelityA <= 0.4
        expected_output = [links[4], links[3]]
        self.assertEqual(gs.choose_links_to_distill(links=links), expected_output)

        # checking boundary cases where link should always have fidelity between
        # 0.3 and 0.4, but strictly smaller than 0.4
        gs._majorizes = lambda fidelityA, fidelityB: fidelityA >= 0.3 and fidelityA < 0.4
        expected_output = [links[3], links[0]]
        self.assertEqual(gs.choose_links_to_distill(links=links), expected_output)


class Test_BandedStrategy(unittest.TestCase):

    def test_init(self):
        for boundaries, expected_bands in [([0.5], [(0., 0.5), (0.5, 1.)]),
                                           ([0.3, 0.7], [(0., 0.3), (0.3, 0.7), (0.7, 1.0)])]:
            bs = BandedStrategy(band_boundaries=boundaries)
            self.assertEqual(bs._bands, expected_bands)

    def test_choose_links_to_distill(self):
        links = Test_Distillation_Scheduling.LINKS
        for band_boundaries, expected_output in [([0.4], [links[1], links[0]]),
                                                 ([0.1], [links[1], links[0]]),
                                                 ([0.05], [links[2], links[1]]),
                                                 ([0.05, 0.1, 0.15], [links[3], links[2]])]:
            bs = BandedStrategy(band_boundaries=band_boundaries)
            output = bs.choose_links_to_distill(links=links)
            self.assertEqual(output, expected_output)


if __name__ == "__main__":
    unittest.main()
