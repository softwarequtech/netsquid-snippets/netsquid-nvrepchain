import unittest
from netsquid_nvrepchain.logic.swap_asap_action_generator import SwapAsapActionGenerator
from netsquid_nvrepchain.utils.memorymanager import MemoryManager


class TestSwapAsapActionGenerator(unittest.TestCase):

    def setUp(self):
        self.memman = MemoryManager(num_positions=100)
        self.number_of_nodes = 17
        self.ag_leftmost = \
            SwapAsapActionGenerator(number_of_nodes=self.number_of_nodes,
                                    index=0,
                                    memorymanager=self.memman)
        self.ag_rightmost = \
            SwapAsapActionGenerator(number_of_nodes=3,
                                    index=2,
                                    memorymanager=self.memman)
        self.middle_index = 9
        self.ag_middle = \
            SwapAsapActionGenerator(number_of_nodes=self.number_of_nodes,
                                    index=self.middle_index,
                                    memorymanager=self.memman)

    def test_get_action_without_message(self):

        # no entanglement; middle node
        action, is_message_used = \
            self.ag_middle.get_action(first_msg_on_inbox=None)
        self.assertFalse(is_message_used)
        self.assertEqual(action.topic, "ENTGEN")
        self.assertEqual(action.parameters["remote_nodeID"],
                         self.middle_index - 1)

        # no entanglement; outermost nodes
        for ag in [self.ag_leftmost, self.ag_rightmost]:
            action, is_message_used = \
                ag.get_action(first_msg_on_inbox=None)
            self.assertFalse(is_message_used)
            self.assertEqual(action, None)

        # single entanglement with left
        self.memman.reset()
        self.memman.setEntangled(pos=0, remote_nodeID=self.middle_index - 1)
        action, is_message_used = \
            self.ag_middle.get_action(first_msg_on_inbox=None)
        self.assertFalse(is_message_used)
        self.assertEqual(action.topic, "ENTGEN")
        self.assertEqual(action.parameters["remote_nodeID"],
                         self.middle_index + 1)

        # single entanglement with left and right
        self.memman.reset()
        self.memman.setEntangled(pos=0, remote_nodeID=self.middle_index - 1)
        self.memman.setEntangled(pos=1, remote_nodeID=self.middle_index + 1)
        action, is_message_used = \
            self.ag_middle.get_action(first_msg_on_inbox=None)
        self.assertFalse(is_message_used)
        self.assertEqual(action.topic, "SWAP")

        # check that after swap, no more actions are given
        for __ in range(10):
            action, is_message_used = \
                self.ag_middle.get_action(first_msg_on_inbox=None)
            self.assertFalse(is_message_used)
            self.assertEqual(action, None)


if __name__ == "__main__":
    unittest.main()
