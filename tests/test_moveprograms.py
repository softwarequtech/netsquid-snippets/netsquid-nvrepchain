import unittest
from netsquid.qubits.qubitapi import operate, create_qubits
from netsquid.qubits.operators import H
from netsquid_nvrepchain.protocols.move_protocol import (
    ThreeCNOTMoveProgram,
    TwoCNOTMoveProgram,
    CXDirectionMoveProgram,
    ReverseCXDirectionMoveProgram,
    MagicalMoveProgram,
    MagicalMoveRotatedTargetProgram)
from netsquid_nvrepchain.network.linenetwork import LineNetwork
from netsquid_nvrepchain.utils.repchain_parameter_tools import get_nv_dict_perfect_version, filter_on_qmem_config_params
import netsquid as ns
from netsquid.qubits.operators import X
from netsquid.qubits.ketstates import s0, s1, h0, h1
from netsquid.qubits.qubitapi import fidelity
from netsquid_nvrepchain.utils.logging_tools import logging
from abc import ABCMeta
import numpy as np


def get_perfect_linenetwork():
    number_of_nodes = 3
    # We only get the perfect state in the limit of
    # the bright state parameter going to zero
    cycle_time = 100

    qmem_config_params = get_nv_dict_perfect_version(filter_on_qmem_config_params)
    linenetwork_params = {
        "number_of_nodes": number_of_nodes,
        "internode_distance": 0.00001,
        "tot_num_qubits": 100,
        "qmem_config_params": qmem_config_params,
        "distributor_params": {"cycle_time": cycle_time}}
    return LineNetwork(**linenetwork_params)


class ITestMovePrograms(unittest.TestCase, metaclass=ABCMeta):

    prgm_cls = None

    electron = 0
    carbon = 1

    possible_electron_states = ["0", "1"]
    possible_carbon_states = ["0", "1"]

    def setUp(self):
        ns.set_qstate_formalism(ns.QFormalism.DM)
        self.positions = [self.electron, self.carbon]

    def test_outcome(self):
        if self.prgm_cls is None:
            return
        self.linenetwork = get_perfect_linenetwork()
        self.qmemory = self.linenetwork.qnodes[0].qmemory
        logging.info("Now testing {}".format(self.prgm_cls))
        for state_electron in self.possible_electron_states:
            for state_carbon in self.possible_carbon_states:
                logging.info('Testing the following states: electron |{}>, carbon |{}>'.format(
                    state_electron, state_carbon))
                ns.sim_reset()
                self.qmemory.reset()

                # put the physical qubits in the quantum memory
                qubits = create_qubits(2)
                self.qmemory.put(qubits=qubits, positions=self.positions)

                self._apply_intermediate_quantum_operations(state_electron, state_carbon)

                # run the circuit
                if self.prgm_cls in [CXDirectionMoveProgram, ReverseCXDirectionMoveProgram]:
                    self.prgm = self.prgm_cls(inplace=True)
                else:
                    self.prgm = self.prgm_cls()
                self.qmemory.execute_program(self.prgm, qubit_mapping=[self.electron, self.carbon])
                self.qmemory.set_program_done_callback(callback=self._check_outcome,
                                                       initial_state_electron=state_electron,
                                                       initial_state_carbon=state_carbon)
                ns.sim_run()

    def _apply_intermediate_quantum_operations(self, state_electron, state_carbon):
        for (state, pos) in [(state_electron, self.electron),
                             (state_carbon, self.carbon)]:
            if state == "1":
                [q] = self.qmemory.peek(positions=[pos])
                operate(q, X)
            if state == "+":
                [q] = self.qmemory.peek(positions=[pos])
                operate(q, H)
            if state == "-":
                [q] = self.qmemory.peek(positions=[pos])
                operate(q, X)
                operate(q, H)

    @classmethod
    def _get_pos_vs_state_expectation(cls, initial_state_electron, initial_state_carbon):
        """
        Meant to be subclassed.

        Returns
        -------
        list of (int, str)
            Tuple (position, state) where state is either "0" or "1" or "+" or "-"
        """
        pass

    def _check_outcome(self, initial_state_electron, initial_state_carbon):
        tuples_to_check = self._get_pos_vs_state_expectation(initial_state_electron, initial_state_carbon)
        for (pos, state) in tuples_to_check:
            [q] = self.qmemory.peek(positions=[pos])
            if state == "0":
                expected_outcome_ket = s0
            elif state == "1":
                expected_outcome_ket = s1
            elif state == "+":
                expected_outcome_ket = h0
            elif state == "-":
                expected_outcome_ket = h1
            else:
                raise ValueError

            fid = fidelity([q], reference_state=expected_outcome_ket)
            self.assertTrue(np.isclose(fid, 1.0))


class TestMagicalMoveProgram(ITestMovePrograms):
    prgm_cls = MagicalMoveProgram

    @classmethod
    def _get_pos_vs_state_expectation(cls, initial_state_electron, initial_state_carbon):
        return [(cls.electron, initial_state_carbon),
                (cls.carbon, initial_state_electron)]


class TestMagicalMoveRotatedTargetProgram(ITestMovePrograms):
    prgm_cls = MagicalMoveRotatedTargetProgram
    possible_carbon_states = ["+", "-"]

    @classmethod
    def _get_pos_vs_state_expectation(cls, initial_state_electron, initial_state_carbon):
        if initial_state_electron == "0":
            expected_state_carbon = "+"
        elif initial_state_electron == "1":
            expected_state_carbon = "-"
        else:
            raise ValueError

        if initial_state_carbon == "+":
            expected_state_electron = "+"
        elif initial_state_carbon == "-":
            expected_state_electron = "-"
        else:
            raise ValueError

        return [(cls.electron, expected_state_electron),
                (cls.carbon, expected_state_carbon)]


class TestThreeCNOTMoveProgram(TestMagicalMoveProgram):
    prgm_cls = ThreeCNOTMoveProgram


class TestTwoCNOTMoveProgram(ITestMovePrograms):
    prgm_cls = TwoCNOTMoveProgram

    @classmethod
    def _get_pos_vs_state_expectation(cls, initial_state_electron, initial_state_carbon):
        return [(cls.electron, initial_state_carbon)]


class TestCXDirectionMoveProgram(ITestMovePrograms):
    prgm_cls = CXDirectionMoveProgram
    possible_electron_states = ["0", "1"]
    possible_carbon_states = ["0"]

    @classmethod
    def _get_pos_vs_state_expectation(cls, initial_state_electron, initial_state_carbon):
        if initial_state_electron == "0":
            expected_state_carbon = "+"
        elif initial_state_electron == "1":
            expected_state_carbon = "-"
        else:
            raise ValueError

        if initial_state_carbon == "0":
            expected_state_electron = "0"
        else:
            raise ValueError

        return [(cls.carbon, expected_state_carbon),
                (cls.electron, expected_state_electron)]


class TestReverseCXDirectionMoveProgram(ITestMovePrograms):
    prgm_cls = ReverseCXDirectionMoveProgram
    possible_electron_states = ["0"]
    possible_carbon_states = ["+", "-"]

    @classmethod
    def _get_pos_vs_state_expectation(cls, initial_state_electron, initial_state_carbon):
        if initial_state_carbon == "+":
            expected_state_electron = "0"
        elif initial_state_carbon == "-":
            expected_state_electron = "1"
        else:
            raise ValueError

        if initial_state_electron == "0":
            expected_state_carbon = "0"
        else:
            raise ValueError

        return [(cls.electron, expected_state_electron),
                (cls.carbon, expected_state_carbon)]


if __name__ == "__main__":
    unittest.main()
