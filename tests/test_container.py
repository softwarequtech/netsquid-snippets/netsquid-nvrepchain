import unittest
from netsquid_nvrepchain.runtools.container import Container


global_value = 9


class MyClass:

    def __init__(self, myparameter):
        self.myparameter = myparameter

    def start(self):
        self.myparameter = 42


class TestContainer(unittest.TestCase):

    def setUp(self):
        self.container = Container()

    def test_add_cls_error(self):

        # error when parameter cannot be found
        with self.assertRaises(Exception):
            self.container.add_cls(name='myclass', cls=MyClass)

    def test_add_cls_no_error(self):
        # no error when parameter is found
        value = 3
        self.container.add_simple_item(name='myparameter', value=value)
        self.container.add_cls(name='myclass', cls=MyClass)
        myclass = self.container['myclass']
        self.assertEqual(myclass.myparameter, value)

    def test_start(self):
        # check if the 'start' function is called,
        # including callbacks that we added
        self.test_add_cls_no_error()
        myclass = self.container['myclass']

        def change_global_value():
            global global_value
            global_value = 4

        self.container.add_callback_during_start(
            callback_fn=change_global_value)
        self.assertEqual(global_value, 9)
        self.container.start()
        self.assertEqual(myclass.myparameter, 42)
        self.assertEqual(global_value, 4)


if __name__ == "__main__":
    unittest.main()
