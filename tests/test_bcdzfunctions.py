import unittest
from netsquid_nvrepchain.utils.bcdzfunctions import (
    num2level, level2num, num2numOneLower,
    get_height, get_ngbh, is_initiator,
    get_table, get_levels)


class TestBCDZFunctions(unittest.TestCase):

    def test_num2level_and_level2num(self):
        values = [(d, 2**d + 1) for d in range(1, 10)]
        for (d, number_of_nodes) in values:
            self.assertEqual(num2level(number_of_nodes=number_of_nodes), d)
            self.assertEqual(number_of_nodes, level2num(level=d))

    def test_num2numOneLower(self):
        vals = [(2**d + 1, 2**(d + 1) + 1) for d in range(1, 10)]
        for (numonelower, number_of_nodes) in vals:
            self.assertEqual(num2numOneLower(number_of_nodes), numonelower)

    def test_get_height(self):
        for (number_of_nodes, index, height) in [(3, 0, 1),
                                                 (3, 1, 1),
                                                 (3, 2, 1),
                                                 (5, 0, 2),
                                                 (5, 1, 1),
                                                 (5, 2, 2),
                                                 (5, 3, 1),
                                                 (5, 4, 2),
                                                 (9, 0, 3),
                                                 (9, 1, 1),
                                                 (9, 2, 2),
                                                 (9, 3, 1),
                                                 (9, 4, 3),
                                                 (9, 5, 1),
                                                 (9, 6, 2),
                                                 (9, 7, 1),
                                                 (9, 8, 3)]:
            self.assertEqual(get_height(number_of_nodes=number_of_nodes, index=index),
                             height)

    def test_get_ngbh(self):
        nonevalue = -1
        tuples = [(3, 0, 1, [nonevalue, 1]),
                  (3, 1, 1, [0, 2]),
                  (3, 2, 1, [1, nonevalue]),
                  (5, 0, 1, [nonevalue, 1]),
                  (5, 0, 2, [nonevalue, 2]),
                  (5, 1, 1, [0, 2]),
                  (5, 2, 1, [1, 3]),
                  (5, 2, 2, [0, 4]),
                  (5, 3, 1, [2, 4]),
                  (5, 4, 1, [3, nonevalue]),
                  (5, 4, 2, [2, nonevalue])]
        for (number_of_nodes, index, level, values) in tuples:
            for (direction, dirint) in [("L", 0), ("R", 1)]:
                self.assertEqual(get_ngbh(number_of_nodes=number_of_nodes,
                                          index=index,
                                          level=level,
                                          direction=direction,
                                          nonevalue=nonevalue),
                                 values[dirint])

    def test_initiator(self):
        tuples = [(3, 0, 1, False),
                  (3, 1, 1, True),
                  (3, 2, 1, False),
                  (5, 0, 1, False),
                  (5, 0, 2, False),
                  (5, 1, 1, True),
                  (5, 2, 1, False),
                  (5, 2, 2, True),
                  (5, 3, 1, True),
                  (5, 4, 1, False),
                  (5, 4, 2, False)]
        for (number_of_nodes, index, level, whether_initiator) in tuples:
            ini = is_initiator(number_of_nodes=number_of_nodes,
                               index=index,
                               level=level)
            self.assertEqual(ini, whether_initiator)

    def test_get_table(self):
        nonevalue = -1
        expected_table = {number_of_nodes: {} for number_of_nodes in [3, 5]}

        expected_table[3][0] = {"L": {1: nonevalue},
                                "R": {1: 1}}
        expected_table[3][1] = {"L": {1: 0},
                                "R": {1: 2}}
        expected_table[3][2] = {"L": {1: 1},
                                "R": {1: nonevalue}}
        expected_table[5][0] = {"L": {1: nonevalue, 2: nonevalue},
                                "R": {1: 1, 2: 2}}
        expected_table[5][1] = {"L": {1: 0},
                                "R": {1: 2}}
        expected_table[5][2] = {"L": {1: 1, 2: 0},
                                "R": {1: 3, 2: 4}}
        expected_table[5][3] = {"L": {1: 2},
                                "R": {1: 4}}
        expected_table[5][4] = {"L": {1: 3, 2: 2},
                                "R": {1: nonevalue, 2: nonevalue}}
        #
        for number_of_nodes in [3, 5]:
            for index in range(number_of_nodes):
                table = get_table(number_of_nodes=number_of_nodes,
                                  index=index,
                                  nonevalue=nonevalue)
                levels = get_levels(number_of_nodes=number_of_nodes, index=index)
                for direction in ["L", "R"]:
                    for level in levels:
                        self.assertEqual(expected_table[number_of_nodes][index][direction][level],
                                         table[direction][level])


if __name__ == "__main__":
    unittest.main()
