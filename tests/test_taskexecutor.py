from netsquid_nvrepchain.logic.taskexecutor import TaskExecutor
from netsquid_nvrepchain.logic.task import Task
import unittest
from netsquid_nvrepchain.utils.message import getNewMessage
from netsquid_nvrepchain.utils.testtools import StoreObject
import netsquid as ns
from netsquid_nvrepchain.utils.logging_tools import logging


class FakeTaskExecutor:

    def __init__(self):
        self.received_msgs = None

    def process_msgs(self, direction, msglist):
        self.received_msgs = (direction, msglist)


class FakeTaskDirector:

    def __init__(self):
        self.gate_restricted = False
        action = None
        parameters = [None, None]
        delay = 0
        self.next = [(action, parameters, delay)]

    def get_next(self):
        if len(self.next) == 0:
            return None
        else:
            return self.next.pop(0)


class FakeChannel(StoreObject):

    def send(self, items):
        if isinstance(items, list):
            for item in items:
                self.append(element=item)
        else:
            self.append(element=items)

    def receive(self):
        return self.locallist[0]


class FakeConnection:

    def __init__(self):
        self._data = []

    def put_from(self, sender, data):
        self._data.append(data)

    def get_as(self, receiver):
        """The parameter `receiver` is not used!"""
        return self._data.pop(0)

    @property
    def data(self):
        if len(self._data) == 0:
            return None
        if len(self._data) == 1:
            return self._data[0]
        else:
            return self._data


class DummySubProtocol:

    _CORRECT_OUTPUT = "CorrectOutput"
    _NAME = "Dummy"

    def __init__(self):
        self.performed = False
        self.evtype_finished = ns.EventType("FIN", "FIN")

    def reset(self):
        self.__init__()

    def __str__(self):
        return "PROT[{}]".format(self._NAME)

    def trigger(self, **kwargs):
        self.performed = True
        ns.Entity()._schedule_now(self.evtype_finished)
        self.performed_time = ns.sim_time()
        logging.info("Perf. subprot {}:{}".format(self._NAME, kwargs))


class EntSwapDummy(DummySubProtocol):

    _NAME = "EntSwap"

    def trigger(self, qmem_posA, qmem_posB):
        super().trigger(qmem_posA=qmem_posA, qmem_posB=qmem_posB)
        return self._CORRECT_OUTPUT


class DistillDummy(DummySubProtocol):

    _NAME = "Distill"

    def trigger(self, qmem_posA, qmem_posB):
        super().trigger(qmem_posA=qmem_posA, qmem_posB=qmem_posB)
        return self._CORRECT_OUTPUT


class EntGenDummy(DummySubProtocol):

    _NAME = "EntGen"

    def trigger(self, remoteID, free_qmem_pos):
        super().trigger(remoteID=remoteID, free_qmem_pos=free_qmem_pos)
        return self._CORRECT_OUTPUT


class MoveDummy(DummySubProtocol):

    _NAME = "MOVE"

    def trigger(self, remoteID, free_qmem_pos):
        super().trigger(remoteID=remoteID, free_qmem_pos=free_qmem_pos)
        return self._CORRECT_OUTPUT


class Test_TaskExecutor(unittest.TestCase):

    def test_perform_tasks(self):
        ns.sim_reset()
        entgenprot = EntGenDummy()
        channels = {direction: FakeChannel() for direction in ["L", "R"]}
        te = TaskExecutor(index=2,
                          entswapprot=EntSwapDummy(),
                          distilprot=DistillDummy(),
                          entgenprot=entgenprot,
                          moveprot=MoveDummy(),
                          class_channels=channels)

        # put a new todo on the list
        te.start()
        delay = 13.0
        remote_nodeID = 1
        sendmsgtask = Task(topic="SENDMSG",
                           parameters={"remote_nodeID": remote_nodeID,
                                       "header_type": "ENTGEN",
                                       "content_type": "CONFIRM"})
        expected_msg = getNewMessage(sender=te._index,
                                     header_type="ENTGEN",
                                     content_type="CONFIRM",
                                     intended_receiver=remote_nodeID)
        sleeptask = Task(topic="SLEEP",
                         parameters={"duration": delay})
        entgentask = Task(topic="ENTGEN",
                          parameters={"remote_nodeID": remote_nodeID,
                                      "free_pos": 12})
        te.todolist.append(sendmsgtask)
        te.todolist.append(sleeptask)
        te.todolist.append(entgentask)

        self.assertFalse(entgenprot.performed)

        # start the simulation
        te.continue_performing_tasks()
        ns.sim_run()
        self.assertEqual(ns.sim_time(), delay)
        self.assertTrue(entgenprot.performed)
        self.assertEqual(entgenprot.performed_time, delay)
        self.assertEqual(channels["L"].receive(), expected_msg)
        self.assertEqual(len(channels["R"].locallist), 0)

    def test_task_expiry(self):
        remote_nodeID = 42
        helper_entity = ns.Entity()
        entgenprot = EntGenDummy()

        # case: request is not responded to, so it will expire
        ###########################################
        ns.sim_reset()
        channels = {direction: FakeChannel() for direction in ["L", "R"]}
        te = TaskExecutor(index=2,
                          entswapprot=entgenprot,
                          distilprot=DistillDummy(),
                          entgenprot=EntGenDummy(),
                          moveprot=MoveDummy(),
                          class_channels=channels)

        expires_after = 17
        entgentask = Task(topic="ENTGEN",
                          parameters={"remote_nodeID": remote_nodeID,
                                      "free_pos": 53},
                          halt_until_confirm=True,
                          halt_expires_after=expires_after)
        te.todolist.append(entgentask)

        # run simulations
        self.assertFalse(entgenprot.performed)
        te.start()
        te.continue_performing_tasks()
        ns.sim_run()
        self.assertEqual(ns.sim_time(), expires_after)
        self.assertFalse(entgenprot.performed)

        # case: there is a response to the request
        ###########################################
        ns.sim_reset()
        te = TaskExecutor(index=2,
                          entswapprot=EntSwapDummy(),
                          distilprot=DistillDummy(),
                          entgenprot=entgenprot,
                          moveprot=MoveDummy(),
                          class_channels=channels)

        te.start()
        te.todolist = [entgentask]
        self.assertFalse(entgenprot.performed)

        # respond to the pending task
        respond_time = 11

        def respond(taskexecutor):
            logging.debug("*In Test*: {}: responding to message".format(ns.sim_time()))
            response_msg = getNewMessage(sender=remote_nodeID,
                                         header_type="ENTGEN",
                                         content_type="CONFIRM",
                                         intended_receiver=taskexecutor._index)
            taskexecutor.process_msgs(direction="R", msglist=[response_msg])

        evtype_respond = ns.EventType("RESPOND", "Respond to ASK-message")
        helper_entity._wait_once(ns.EventHandler(lambda event, taskexecutor=te: respond(taskexecutor=taskexecutor)),
                                 event_type=evtype_respond)
        helper_entity._schedule_after(respond_time, evtype_respond)

        # run the simulation
        te.continue_performing_tasks()
        ns.sim_run()

        self.assertEqual(ns.sim_time(), expires_after)
        self.assertTrue(entgenprot.performed)
        self.assertEqual(respond_time, entgenprot.performed_time)


if __name__ == "__main__":
    unittest.main()
