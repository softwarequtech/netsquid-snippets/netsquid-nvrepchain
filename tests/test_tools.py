import unittest
from netsquid_nvrepchain.utils.tools import index_of_Pauli_product


class TestTools(unittest.TestCase):

    def test_bell_index_to_stabilizer_coefficients(self):
        pass
        # TODO

    def test_stabilizer_coefficients_to_bell_index(self):
        pass
        # TODO

    def test_index_of_Pauli_product(self):
        for (Pauli_index_A, Pauli_index_B, expected_output_index) in\
                [(0, 0, 0),
                 (0, 1, 1),
                 (0, 2, 2),
                 (0, 3, 3),
                 (1, 0, 1),
                 (1, 1, 0),
                 (1, 2, 3),
                 (1, 3, 2),
                 (2, 0, 2),
                 (2, 1, 3),
                 (2, 2, 0),
                 (2, 3, 1),
                 (3, 0, 3),
                 (3, 1, 2),
                 (3, 2, 1),
                 (3, 3, 0)]:
            computed_output_index = \
                index_of_Pauli_product(Pauli_index_A=Pauli_index_A,
                                       Pauli_index_B=Pauli_index_B)
            self.assertEqual(computed_output_index, expected_output_index)


if __name__ == "__main__":
    unittest.main()
