import unittest
from netsquid_nvrepchain.logic.actiongenerator import IActionGenerator, SingleNVActionGenerator
from netsquid_nvrepchain.utils.message import getNewMessage
from netsquid_nvrepchain.utils.memorymanager import MemoryManager, ELECTRON_POSITION


class TestIActionGenerator(unittest.TestCase):

    def setUp(self):
        self.ag = IActionGenerator()

    def test(self):
        self.assertFalse(self.ag.has_stopped)

        self.ag.should_stop_fn = lambda: True

        self.assertTrue(self.ag.has_stopped)


class TestSingleNVActionGenerator(unittest.TestCase):

    def setUp(self):
        self.memman = MemoryManager(num_positions=100)
        self.ag_init = SingleNVActionGenerator(number_of_nodes=9, index=1, memorymanager=self.memman)
        self.ag_resp = SingleNVActionGenerator(number_of_nodes=9, index=4, memorymanager=self.memman)

    def test_get_action_without_message(self):

        # no entanglement => entgen left
        self.memman.reset()
        action, is_message_used = self.ag_init.get_action(first_msg_on_inbox=None)
        self.assertEqual(action.parameters["remote_nodeID"], 0)
        self.assertEqual(action.topic, "ENTGEN")
        self.assertEqual(is_message_used, False)

        # single bad entanglement with left => entgen left
        self.memman.reset()
        self.memman.setEntangled(pos=0, remote_nodeID=0)
        action, is_message_used = self.ag_init.get_action(first_msg_on_inbox=None)
        self.assertEqual(action.topic, "ENTGEN")
        self.assertEqual(action.parameters["remote_nodeID"], 0)
        self.assertEqual(is_message_used, False)

        # twice bad entanglement with left => distill left
        self.memman.reset()
        self.memman.setEntangled(pos=0, remote_nodeID=0)
        self.memman.setEntangled(pos=1, remote_nodeID=0)
        action, is_message_used = self.ag_init.get_action(first_msg_on_inbox=None)
        self.assertEqual(action.parameters["remote_nodeID"], 0)
        self.assertEqual(action.topic, "DIST")
        self.assertEqual(is_message_used, False)

        # single good entanglement with left => entgen right
        self.memman.reset()
        self.memman.setEntangled(pos=0, remote_nodeID=0)
        link = self.memman.getLink(pos=0)
        link.estimated_fidelity = 1.0
        action, is_message_used = self.ag_init.get_action(first_msg_on_inbox=None)
        self.assertEqual(action.topic, "ENTGEN")
        self.assertEqual(action.parameters["remote_nodeID"], 2)
        self.assertEqual(is_message_used, False)

        # single good entanglement with left + single bad with right => entgen right
        self.memman.reset()
        self.memman.setEntangled(pos=0, remote_nodeID=0)
        link = self.memman.getLink(pos=0)
        link.estimated_fidelity = 1.0
        self.memman.setEntangled(pos=1, remote_nodeID=2)
        action, is_message_used = self.ag_init.get_action(first_msg_on_inbox=None)
        self.assertEqual(action.topic, "ENTGEN")
        self.assertEqual(action.parameters["remote_nodeID"], 2)
        self.assertEqual(is_message_used, False)

        # single good entanglement with left + twice bad with right => distill right
        self.memman.reset()
        self.memman.setEntangled(pos=0, remote_nodeID=0)
        link = self.memman.getLink(pos=0)
        link.estimated_fidelity = 1.0
        self.memman.setEntangled(pos=1, remote_nodeID=2)
        self.memman.setEntangled(pos=2, remote_nodeID=2)
        action, is_message_used = self.ag_init.get_action(first_msg_on_inbox=None)
        self.assertEqual(action.topic, "DIST")
        self.assertEqual(action.parameters["remote_nodeID"], 2)
        self.assertEqual(is_message_used, False)

        # single good entanglement with left & right => swap
        self.memman.reset()
        posL, posR = 0, 1
        for (pos, remote_nodeID) in [(posL, 0), (posR, 2)]:
            self.memman.setEntangled(pos=pos, remote_nodeID=remote_nodeID)
            link = self.memman.getLink(pos=pos)
            link.estimated_fidelity = 1.0
        action, is_message_used = self.ag_init.get_action(first_msg_on_inbox=None)
        self.assertEqual(action.topic, "SWAP")
        self.assertEqual(is_message_used, False)

    def test_get_action_return_electron_if_possible(self):
        # we define a few scenarios of quantum memory state;
        # in each scenario, the actiongenerator should return
        # the electron spin as `qmem_posA` if possible

        # scenario is a list of (memory position, remote node id)
        assert(ELECTRON_POSITION == 0)
        scenario_a = [(ELECTRON_POSITION, 0),
                      (1, 0),
                      (2, 2)]
        expected_control_a = ELECTRON_POSITION
        expected_target_a = 2

        scenario_b = [(2, 0),
                      (ELECTRON_POSITION, 0),
                      (1, 2)]
        expected_control_b = ELECTRON_POSITION
        expected_target_b = 1

        scenario_c = [(1, 0), (2, 2), (3, 0)]
        expected_control_c = 1
        expected_target_c = 2

        for scenario, expected_control, expected_target in \
                [(scenario_a, expected_control_a, expected_target_a),
                 (scenario_b, expected_control_b, expected_target_b),
                 (scenario_c, expected_control_c, expected_target_c)]:
            self.memman.reset()
            for (pos, remote_nodeID) in scenario:
                self.memman.setEntangled(pos=pos, remote_nodeID=remote_nodeID)
                link = self.memman.getLink(pos=pos)
                link.estimated_fidelity = 1.0
            action, is_message_used = \
                self.ag_init.get_action(first_msg_on_inbox=None)
            self.assertEqual(action.topic, "SWAP")
            self.assertEqual(action.parameters["qmem_posA"],
                             expected_control)
            self.assertEqual(action.parameters["qmem_posB"],
                             expected_target)
            self.assertEqual(is_message_used, False)

    def test_get_action_with_ask_message(self):
        sender = 123
        msg = getNewMessage(sender=sender,
                            header_type="ENTGEN",
                            content_type="ASK",
                            intended_receiver=None)

        # msg used
        self.memman.reset()
        action, is_message_used = self.ag_resp.get_action(first_msg_on_inbox=msg)
        self.assertEqual(action.parameters["remote_nodeID"], sender)
        self.assertEqual(action.topic, "ENTGEN")
        self.assertEqual(is_message_used, True)

        self.memman.reset()
        sender = 145
        linkIDA = 5
        linkIDB = 17
        for (pos, linkID) in [(12, linkIDA), (13, linkIDB)]:
            self.memman.setEntangled(pos=pos, remote_nodeID=sender)
            link = self.memman.getLink(pos=pos)
            link.linkID = linkID
        msg = getNewMessage(sender=sender,
                            header_type="DIST",
                            content_type="ASK",
                            intended_receiver=None,
                            additional_content=[linkIDA, linkIDB])

        # msg used
        action, is_message_used = self.ag_resp.get_action(first_msg_on_inbox=msg)
        self.assertEqual(action.parameters["remote_nodeID"], sender)
        self.assertEqual(action.topic, "DIST")
        self.assertEqual(is_message_used, True)


if __name__ == "__main__":
    unittest.main()
