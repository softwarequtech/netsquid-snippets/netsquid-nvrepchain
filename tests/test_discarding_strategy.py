import unittest
from netsquid_nvrepchain.utils.testtools import StoreObject
from netsquid_nvrepchain.logic.discarding_strategy import (
    FixedCutoffTimeDiscardingStrategy,
    LevelDependentFidelityBasedDiscardingStrategy)


class Test_DiscardingStrategy(unittest.TestCase):

    def test_fixedcutoff(self):
        for (cutoff_time, link_creation_time, expected_whether_discard) \
                in [(10., -15., True),
                    (10, -8., False)]:
            ds = FixedCutoffTimeDiscardingStrategy(cutoff_time=cutoff_time)
            ds._current_time = lambda: 0.
            link = StoreObject()
            link.creation_time = link_creation_time
            self.assertEqual(ds.should_discard(link=link), expected_whether_discard)

    def test_levelfidelities(self):
        # imagine a 17-node chain
        node_index = 8
        level_fidelity_thresholds = [0.9, 0.8, 0.5, 0.4]
        ds = LevelDependentFidelityBasedDiscardingStrategy(node_index=node_index,
                                                           level_fidelity_thresholds=level_fidelity_thresholds)
        for (remote_index, estimated_fidelity, expected_whether_discard) in \
                [(7, 0.91, False),
                 (7, 0.89, True),
                 (6, 0.81, False),
                 (6, 0.79, True),
                 (4, 0.51, False),
                 (4, 0.49, True),
                 (0, 0.41, False),
                 (0, 0.39, True)]:
            link = StoreObject()
            link.estimated_fidelity = estimated_fidelity
            link.remote_nodeID = remote_index
            self.assertEqual(ds.should_discard(link=link), expected_whether_discard)


if __name__ == "__main__":
    unittest.main()
