"""
An integration test for entanglement swapping, involving three nodes
"""
import unittest
from netsquid_nvrepchain.protocols.entswap_protocol import EntSwapProtocol, UnrestrictedBSMProgram, RestrictedBSMProgram
from netsquid_nvrepchain.network.linenetwork import LineNetwork
from netsquid_nvrepchain.utils.repchain_parameter_tools import get_nv_dict_perfect_version, filter_on_qmem_config_params
from netsquid_nvrepchain.utils.memorymanager import MemoryManager
from netsquid_nvrepchain.utils.tools import (
    bell_index_to_ket_state,
    apply_Pauli_to_one_side_of_bell_state,
    index_of_Pauli_product,
    apply_Hadamard_on_each_qubit)
from netsquid_nvrepchain.utils.testtools import _add_Bell_pair
from netsquid.qubits.qubitapi import fidelity, operate
from netsquid.qubits.operators import H
import numpy as np
import netsquid as ns
from netsquid_nvrepchain.utils.logging_tools import logging


def get_perfect_linenetwork():
    number_of_nodes = 3
    # We only get the perfect state in the limit of
    # the bright state parameter going to zero
    cycle_time = 100

    qmem_config_params = get_nv_dict_perfect_version(filter_on_qmem_config_params)
    linenetwork_params = {
        "number_of_nodes": number_of_nodes,
        "internode_distance": 0.00001,
        "tot_num_qubits": 100,
        "qmem_config_params": qmem_config_params,
        "distributor_params": {"cycle_time": cycle_time}}
    return LineNetwork(**linenetwork_params)


class TestIntegrationEntSwap(unittest.TestCase):

    def setUp(self):
        ns.sim_reset()
        self.linenetwork = get_perfect_linenetwork()

    def _pass_on_channel_output_to_protocol(self, msg, channel, protocol):
        # TODO I think `direction` is irrelevant
        msg = msg.items[0]
        protocol.process_incoming_message(msg=msg)

    def test_on_Bell_states(self):
        # two links:
        # node 0          node 1           node 2
        #   0 <------------> 0
        #                    1 <------------> 1
        # and node 1 will perform the entanglement swap
        for P_node0_pos0 in range(4):
            for P_node1_pos0 in range(4):
                for P_node1_pos2 in range(4):
                    for P_node2_pos2 in range(4):
                        for is_move_magical in [False]:
                            self._perform_test(P_node0_pos0,
                                               P_node1_pos0,
                                               P_node1_pos2,
                                               P_node2_pos2,
                                               is_move_magical=is_move_magical)

    def _perform_test(self,
                      P_node0_pos0,
                      P_node1_pos0,
                      P_node1_pos2,
                      P_node2_pos2,
                      is_move_magical):

        protocols = [
            EntSwapProtocol(
                index=index,
                qmemory=self.linenetwork.qnodes[index].qmemory,
                memmanager=MemoryManager(num_positions=100),
                swap_update_receivers=None,
                # TODO should also test not-None case
                class_channels=self.linenetwork.class_channels_send[index],
                respect_NV_structure=True,
                is_move_magical=is_move_magical)
            for index in range(3)]
        # tie the classical channels for sending to the
        # `process_msgs` of the protocols
        for (receiving_node_index, receiving_direction) in \
                [(0, "R"), (1, "L"), (1, "R"), (2, "L")]:
            receive_channel = \
                self.linenetwork.class_channels_receive[receiving_node_index][receiving_direction]
            receiving_protocol = protocols[receiving_node_index]
            receive_channel.ports['recv'].bind_output_handler(
                msg_handler=lambda msg, channel=receive_channel, protocol=receiving_protocol:
                self._pass_on_channel_output_to_protocol(msg=msg, channel=channel, protocol=protocol))
        pos_0 = 0
        pos_2 = 1

        for node_ID in range(3):
            protocols[node_ID].memmanager.reset()
            protocols[node_ID].reset()
        for qnode in self.linenetwork.qnodes:
            qnode.qmemory.reset()

        # compute the bell indices by applying the local Paulis to Psi^+
        bell_index = 1
        virtual_bell_index_0 = \
            apply_Pauli_to_one_side_of_bell_state(bell_index, index_of_Pauli_product(P_node0_pos0, P_node1_pos0))
        virtual_bell_index_2 = \
            apply_Pauli_to_one_side_of_bell_state(bell_index, index_of_Pauli_product(P_node1_pos2, P_node2_pos2))

        physical_bell_index_0 = virtual_bell_index_0
        physical_bell_index_2 = virtual_bell_index_2
        if not is_move_magical:
            physical_bell_index_2 = apply_Hadamard_on_each_qubit(virtual_bell_index_2)

        # add states
        for (nodeID_A, nodeID_B, posA, posB, bell_index) in \
                [(0, 1, pos_0, pos_0, physical_bell_index_0),
                 (1, 2, pos_2, pos_2, physical_bell_index_2)]:
            _add_Bell_pair(qmemA=self.linenetwork.qnodes[nodeID_A].qmemory,
                           qmemB=self.linenetwork.qnodes[nodeID_B].qmemory,
                           posA=posA,
                           posB=posB,
                           bell_index=bell_index)
        for (nodeID_self, remote_nodeID, pos, cor_Pauli) in \
            [(0, 1, pos_0, P_node0_pos0),
             (1, 0, pos_0, P_node1_pos0),
             (1, 2, pos_2, P_node1_pos2),
             (2, 1, pos_2, P_node2_pos2)]:
            protocols[nodeID_self].memmanager.setEntangled(pos=pos,
                                                           remote_nodeID=remote_nodeID,
                                                           cor_Pauli=cor_Pauli)

        # start the entanglement swapping
        protocols[1].trigger(qmem_posA=pos_0, qmem_posB=pos_2)
        ns.sim_run()

        # assert that we end up with the correct Bell state afterwards
        resulting_link_0 = protocols[0].memmanager.getLink(pos=pos_0)
        resulting_link_2 = protocols[2].memmanager.getLink(pos=pos_2)

        self.assertEqual(resulting_link_0.remote_nodeID, 2)
        self.assertEqual(resulting_link_2.remote_nodeID, 0)
        for pos in [pos_0, pos_2]:
            self.assertTrue(protocols[1].memmanager.isFree(pos=pos))

        [q0] = self.linenetwork.qnodes[0].qmemory.peek(positions=[pos_0])
        [q2] = self.linenetwork.qnodes[2].qmemory.peek(positions=[pos_2])
        cor_Pauli_0 = resulting_link_0.cor_Pauli
        cor_Pauli_2 = resulting_link_2.cor_Pauli
        expected_resulting_virtual_bell_index = \
            apply_Pauli_to_one_side_of_bell_state(
                bell_index=1,
                pauli_index=index_of_Pauli_product(cor_Pauli_0, cor_Pauli_2))
        expected_virtual_ket = bell_index_to_ket_state(expected_resulting_virtual_bell_index)

        if not is_move_magical:
            # apply a hadamard on one side
            operate(q2, H)

        expected_resulting_ket = expected_virtual_ket

        fid = fidelity([q0, q2], reference_state=expected_resulting_ket)
        self.assertTrue(np.isclose(fid, 1.0))


class TestBSMPrograms(unittest.TestCase):

    def test_outcome(self):
        ns.set_qstate_formalism(ns.QFormalism.DM)

        self.linenetwork = get_perfect_linenetwork()
        qmemory = self.linenetwork.qnodes[0].qmemory
        electron = 0
        carbon = 1

        for prgm_cls in [UnrestrictedBSMProgram, RestrictedBSMProgram]:
            logging.info("Now checking {}".format(prgm_cls))
            for bell_index in [0, 1, 2, 3]:
                ns.sim_reset()
                qmemory.reset()

                # put the physical qubits in the quantum memories
                _add_Bell_pair(qmemA=qmemory,
                               qmemB=qmemory,
                               posA=electron,
                               posB=carbon,
                               bell_index=bell_index)
                if prgm_cls == RestrictedBSMProgram:
                    [q_carbon] = qmemory.peek(positions=[carbon])
                    operate(q_carbon, H)

                # run the circuit
                self.prgm = prgm_cls()
                qmemory.execute_program(self.prgm, qubit_mapping=[electron, carbon])
                qmemory.set_program_done_callback(callback=self._check_outcome, expected_outcome=bell_index)
                ns.sim_run()

    def _check_outcome(self, expected_outcome):
        qmemory = self.linenetwork.qnodes[0].qmemory
        [q_carbon] = qmemory.peek(positions=[1])
        self.assertEqual(expected_outcome, self.prgm.get_outcome_as_bell_index)


if __name__ == "__main__":
    unittest.main()
