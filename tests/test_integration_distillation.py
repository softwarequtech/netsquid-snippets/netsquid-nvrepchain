"""
An integration test for distillation: two nodes performing distillation on
two Bell pairs they share.
"""

import unittest
from netsquid_nvrepchain.network.linenetwork import LineNetwork
from netsquid_nvrepchain.utils.repchain_parameter_tools import get_nv_dict_perfect_version, filter_on_qmem_config_params
from netsquid_nvrepchain.utils.memorymanager import MemoryManager
from netsquid_nvrepchain.protocols.distillation_protocol import LocalDistillationProtocol
from netsquid_nvrepchain.utils.tools import (
    bell_index_to_ket_state,
    apply_Pauli_to_one_side_of_bell_state,
    index_of_Pauli_product,
    apply_Hadamard_on_each_qubit,
    pauli_index_to_get_to_psiplus)
from netsquid_nvrepchain.utils.testtools import _add_Bell_pair
from netsquid.qubits.qubitapi import fidelity
import numpy as np
import netsquid as ns


def get_perfect_linenetwork():
    number_of_nodes = 3
    cycle_time = 100
    qmem_config_params = get_nv_dict_perfect_version(filter_on_qmem_config_params)
    linenetwork_params = {
        "number_of_nodes": number_of_nodes,
        "internode_distance": 0.00001,
        "tot_num_qubits": 100,
        "qmem_config_params": qmem_config_params,
        "distributor_params": {"cycle_time": cycle_time}}
    return LineNetwork(**linenetwork_params)


class TestIntegrationDistillation(unittest.TestCase):
    """
    Testing the distillation protocol as a black box:
    the input is given by two different Bell states
    and four correction Paulis; if the correction Paulis
    were applied to the Bell states, the two Bell states
    would equal Psi^+.

    The output of the black box is either (a) failure of
    distillation or (b) successful distillation with a
    Bell state with two correction Paulis; again, if the
    correction Paulis were applied to the Bell state,
    it would equal Psi^+.

    In this test, we check for many combinations of
    inputs, the resulting output indeed equals Psi^+
    after application of the correction Paulis.
    We perform this check many times since distillation
    is a randomized process.
    """

    POS_LOSE = 0
    POS_KEEP = 1

    def initialize(self, respect_NV_structure):
        """
        Set us a LineNetwork of two nodes, each of which holds
        a LocalDistillationProtocol.
        The nodes are connected with two classical channels, whose
        output is directly passed on to the LocalDistillationProtocols.
        """
        ns.sim_reset()
        self.linenetwork = get_perfect_linenetwork()

        self.protocols = [LocalDistillationProtocol(index=index,
                                                    qmemory=self.linenetwork.qnodes[index].qmemory,
                                                    memmanager=MemoryManager(num_positions=100),
                                                    class_channels=self.linenetwork.class_channels_send[index],
                                                    respect_NV_structure=respect_NV_structure)
                          for index in range(2)]

        # tie the classical channels for sending to the
        # `process_incoming_message` of the protocols
        for receiving_node_index in [0, 1]:
            receiving_direction = "R" if receiving_node_index == 0 else "L"
            receive_channel = self.linenetwork.class_channels_receive[receiving_node_index][receiving_direction]
            receiving_protocol = self.protocols[receiving_node_index]
            receive_channel.ports['recv'].bind_output_handler(
                msg_handler=lambda msg, channel=receive_channel, protocol=receiving_protocol:
                self._pass_on_channel_output_to_protocol(msg=msg, channel=channel, protocol=protocol))

    def _pass_on_channel_output_to_protocol(self, msg, channel, protocol):
        msg = msg.items[0]
        protocol.process_incoming_message(msg=msg)

    def test_on_Bell_states(self):
        for __ in range(10):
            for respect_NV_structure in [False, True]:
                self.initialize(respect_NV_structure=respect_NV_structure)
                for virtual_bell_index_lose in [0, 1, 2, 3]:
                    for virtual_bell_index_keep in [0, 1, 2, 3]:
                        for P_lose_0 in [0, 1, 2, 3]:
                            for P_keep_0 in [0, 1, 2, 3]:

                                # Compute the correction Paulis on the other side so
                                # that the resulting state, after the Pauli correction,
                                # is always Psi^+
                                P_lose_1 = \
                                    index_of_Pauli_product(
                                        pauli_index_to_get_to_psiplus(virtual_bell_index_lose),
                                        P_lose_0)
                                P_keep_1 = \
                                    index_of_Pauli_product(
                                        pauli_index_to_get_to_psiplus(virtual_bell_index_keep),
                                        P_keep_0)
                                self._perform_test_run(respect_NV_structure=respect_NV_structure,
                                                       virtual_bell_index_lose=virtual_bell_index_lose,
                                                       virtual_bell_index_keep=virtual_bell_index_keep,
                                                       P_lose_0=P_lose_0,
                                                       P_lose_1=P_lose_1,
                                                       P_keep_0=P_keep_0,
                                                       P_keep_1=P_keep_1)

    def _perform_test_run(self, respect_NV_structure,
                          virtual_bell_index_lose, virtual_bell_index_keep,
                          P_lose_0, P_lose_1, P_keep_0, P_keep_1):
        for node_ID in [0, 1]:
            self.protocols[node_ID].memmanager.reset()
        for qnode in self.linenetwork.qnodes:
            qnode.qmemory.reset()

        self._setup_simulation(virtual_bell_index_lose=virtual_bell_index_lose,
                               virtual_bell_index_keep=virtual_bell_index_keep,
                               P_lose_0=P_lose_0,
                               P_lose_1=P_lose_1,
                               P_keep_0=P_keep_0,
                               P_keep_1=P_keep_1,
                               respect_NV_structure=respect_NV_structure)

        # run the distillation
        ns.sim_run()

        was_successfull = not self.protocols[0].memmanager.isFree(pos=self.POS_KEEP)
        if was_successfull:
            self._check_result(respect_NV_structure)
        else:
            for nodeID in [0, 1]:
                self.assertTrue(self.protocols[nodeID].memmanager.isFree(pos=self.POS_KEEP))

    def _check_result(self, respect_NV_structure):

        # get real resulting state
        [q0] = self.linenetwork.qnodes[0].qmemory.peek(positions=[self.POS_KEEP])
        [q1] = self.linenetwork.qnodes[1].qmemory.peek(positions=[self.POS_KEEP])

        # get the state that the protocols think they hold
        expected_index = 1
        for nodeID in [0, 1]:
            link_keep = self.protocols[nodeID].memmanager.getLink(pos=self.POS_KEEP)
            expected_index = \
                apply_Pauli_to_one_side_of_bell_state(
                    bell_index=expected_index,
                    pauli_index=link_keep.cor_Pauli)

        if respect_NV_structure:
            expected_index = apply_Hadamard_on_each_qubit(bell_index=expected_index)

        # assert
        expected_ket = bell_index_to_ket_state(expected_index)
        fid = fidelity([q0, q1], reference_state=expected_ket)
        self.assertTrue(np.isclose(fid, 1.0))

    def _setup_simulation(self,
                          virtual_bell_index_lose,
                          virtual_bell_index_keep,
                          respect_NV_structure,
                          P_lose_0, P_lose_1,
                          P_keep_0, P_keep_1):
        """
        Prepares the simulation:
          * add states to the QuantumProcessors
          * set the corresponding information in the MemoryManagers
          * trigger the two protocols

        Note: if `respect_NV_structure` is true, then the
        physical bell index on the 'keep' qubits is found
        by performing a Hadamard gate on each of the two
        'keep' qubits.

        The setup is as follows (where `pos` are memory positions
        and `P_{...}` are the correction Paulis).
        +----------------------------------------------------------------+
        |                                                                |
        |     Node 0                               Node 1                |
        |     ------                               ------                |
        |                                                                |
        |  P_lose_0, pos_lose <-------------------> pos_lose, P_lose_1   |
        |                                                                |
        |  P_lose_1, pos_keep <-------------------> pos_keep, P_keep_1   |
        |                                                                |
        +----------------------------------------------------------------+
        """
        for (pos, virtual_bell_index) in \
                [(self.POS_LOSE, virtual_bell_index_lose),
                 (self.POS_KEEP, virtual_bell_index_keep)]:
            if respect_NV_structure and pos == self.POS_KEEP:
                physical_bell_index = apply_Hadamard_on_each_qubit(virtual_bell_index_keep)
            else:
                physical_bell_index = virtual_bell_index
            _add_Bell_pair(qmemA=self.linenetwork.qnodes[0].qmemory,
                           qmemB=self.linenetwork.qnodes[1].qmemory,
                           posA=pos,
                           posB=pos,
                           bell_index=physical_bell_index)
        for (nodeID_self, pos, cor_Pauli) in [
                (0, self.POS_LOSE, P_lose_0),
                (0, self.POS_KEEP, P_keep_0),
                (1, self.POS_LOSE, P_lose_1),
                (1, self.POS_KEEP, P_keep_1)]:
            remote_nodeID = 1 - nodeID_self
            self.protocols[nodeID_self].memmanager.setEntangled(pos=pos,
                                                                remote_nodeID=remote_nodeID,
                                                                cor_Pauli=cor_Pauli)

        # trigger the nodes
        for nodeID in [0, 1]:
            self.protocols[nodeID].trigger(pos_lose=self.POS_LOSE, pos_keep=self.POS_KEEP)


if __name__ == "__main__":
    unittest.main()
