from netsquid_nvrepchain.runtools.simulation_setup import SimulationSetup
import unittest
from netsquid_nvrepchain.runtools.data_collection_tools import get_statetapper
import netsquid as ns
import numpy as np
from netsquid_nvrepchain.utils.repchain_parameter_tools import get_nv_dict_perfect_version, filter_on_qmem_config_params


def should_stop_fn(simsetup, index, remote_index):
    memmanager = simsetup.protocols[index]['memmanager']
    links = memmanager.getAllEntanglementWith(remote_nodeID=remote_index)
    return len(links) > 0


class Test_SimulationSetup(unittest.TestCase):

    def _get_simsetup(self, number_of_nodes, use_swap_only):
        ns.sim_reset()

        qmem_config_params = get_nv_dict_perfect_version(filter_on_qmem_config_params)

        # We only get the perfect state in the limit of
        # the bright state parameter going to zero
        alpha = 0.00000001
        cycle_time = 100

        linenetwork_params = {
            "number_of_nodes": number_of_nodes,
            "internode_distance": 0.00001,
            "tot_num_qubits": 100,
            "qmem_config_params": qmem_config_params,
            "distributor_params": {"cycle_time": cycle_time}}

        logic_params = {"use_swap_only": use_swap_only,
                        "delivery_params": {"alpha": alpha}}

        simsetup = SimulationSetup(linenetwork_params=linenetwork_params,
                                   logic_params=logic_params)

        for (index, remote_index) in [(0, number_of_nodes - 1), (number_of_nodes - 1, 0)]:
            simsetup.protocols[index]['logic'].should_stop_fn = \
                lambda simsetup=simsetup, index=index, remote_index=remote_index: \
                should_stop_fn(simsetup=simsetup, index=index, remote_index=remote_index)

        simsetup.set_automatic_stop()

        statetapper = get_statetapper(
            memorymanager_A=simsetup.protocols[0]['memmanager'],
            memorymanager_B=simsetup.protocols[simsetup.ln.number_of_nodes - 1]['memmanager'],
            qmemory_A=simsetup.ln.qnodes[0].qmemory,
            qmemory_B=simsetup.ln.qnodes[simsetup.ln.number_of_nodes - 1].qmemory,
            node_index_A=0,
            node_index_B=simsetup.ln.number_of_nodes - 1,
            collect_postsimulation=True,
            apply_Hadamard_if_data_qubit=True)

        simsetup.add_datatapper(statetapper)

        return simsetup

    def test_single_run_many_nodes(self, number_of_nodes=2 ** 3 + 1, uses_swap_only='default'):
        if uses_swap_only == 'default':
            uses_swap_only = [True, False]
        for use_swap_only in uses_swap_only:
            simsetup = self._get_simsetup(number_of_nodes=number_of_nodes,
                                          use_swap_only=use_swap_only)
            statetapper = simsetup.datatappers[0]
            simsetup.perform_run()
            self.assertEqual(len(statetapper.dataframe.index), 1)
            fidelity = statetapper.dataframe["fidelity"][0]
            self.assertTrue(np.isclose(fidelity, 1.0))

    def test_many_runs(self):
        for use_swap_only in [True, False]:
            simsetup = self._get_simsetup(number_of_nodes=2 ** 1 + 1,
                                          use_swap_only=use_swap_only)
            statetapper = simsetup.datatappers[0]

            number_of_runs = 10
            simsetup.perform_many_runs(number_of_runs, suppress_output=True)
            self.assertEqual(len(statetapper.dataframe.index), number_of_runs)
            fidelities = statetapper.dataframe["fidelity"]
            for fidelity in fidelities:
                self.assertTrue(np.isclose(fidelity, 1.0))


if __name__ == "__main__":
    unittest.main()
