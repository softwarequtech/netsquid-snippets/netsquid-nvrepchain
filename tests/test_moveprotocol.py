import unittest
from netsquid_nvrepchain.protocols.move_protocol import MoveProtocol
from netsquid_nvrepchain.utils.memorymanager import MemoryManager
from netsquid.components.qprocessor import QuantumProcessor
from netsquid_nvrepchain.utils.testtools import _add_qubit
import netsquid as ns


class TestMoveProtocol(unittest.TestCase):

    def setUp(self):
        self.memorymanager = MemoryManager(num_positions=10)
        self.qmemory = QuantumProcessor(name="TestProcessor",
                                        num_positions=10,
                                        fallback_to_nonphysical=True)

    def check_finished(self, posA, posB, qubitA, qubitB):
        [qA, qB] = self.qmemory.peek(positions=[posA, posB])
        self.assertEqual(qA, qubitB)
        self.assertEqual(qB, qubitA)

    def test_move(self):
        for is_move_magical in [True, False]:
            for respect_NV_structure in [True, False]:
                # setup the protocol and have it call the check-function when done

                prot = MoveProtocol(index=3,
                                    qmemory=self.qmemory,
                                    memmanager=self.memorymanager,
                                    respect_NV_structure=False,
                                    is_move_magical=is_move_magical)

                # try the move_protocol twice
                for (posA, posB) in [(0, 1), (2, 3)]:
                    self.qmemory.reset()
                    self.memorymanager.reset()
                    # add two distinct qubits
                    for (pos, state) in [(posA, "|0>"),
                                         (posB, "|1>")]:
                        _add_qubit(memorymanager=self.memorymanager,
                                   procDev=self.qmemory,
                                   pos=pos,
                                   remote_nodeID=5,
                                   state=state)
                    qubitA = self.qmemory.peek(posA)[0]
                    qubitB = self.qmemory.peek(posB)[0]
                    handler = ns.EventHandler(lambda event, qubitA=qubitA, qubitB=qubitB, posA=posA, posB=posB:
                                              self.check_finished(posA, posB, qubitA, qubitB))
                    ns.Entity()._wait(handler,
                                      event_type=prot.evtype_finished,
                                      once=True)

                    # start the protocol and simulation
                    prot.trigger(old_pos=posA, new_pos=posB)
                    ns.sim_run()


if __name__ == "__main__":
    unittest.main()
