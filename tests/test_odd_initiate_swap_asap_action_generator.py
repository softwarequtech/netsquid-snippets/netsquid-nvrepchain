import unittest
from netsquid_nvrepchain.logic.odd_initiate_swap_asap_action_generator \
    import OddInitiateSwapAsapActionGenerator
from netsquid_nvrepchain.utils.memorymanager import MemoryManager, ELECTRON_POSITION


class TestOddInitiateSwapAsapActionGenerator(unittest.TestCase):

    def setUp(self):
        self.memorymanager = MemoryManager(num_positions=100)

    def test_get_action(self):
        # CASE: no entanglement present
        for number_of_nodes in [4, 5]:
            for index in range(number_of_nodes):
                ag = \
                    OddInitiateSwapAsapActionGenerator(
                        number_of_nodes=number_of_nodes,
                        index=index,
                        memorymanager=self.memorymanager)
                action, __ = \
                    ag.get_action(first_msg_on_inbox=None)
                if index % 2 == 0:
                    self.assertEqual(action, None)
                else:
                    self.assertEqual(action.topic, "ENTGEN")
                    self.assertEqual(action.parameters["remote_nodeID"],
                                     index - 1)

        # CASE: node has already entanglement with left
        for number_of_nodes in [4, 5]:

            # check: middle node, odd index
            self.memorymanager.setEntangled(ELECTRON_POSITION, 0)
            ag = \
                OddInitiateSwapAsapActionGenerator(
                    number_of_nodes=number_of_nodes,
                    index=1,
                    memorymanager=self.memorymanager)
            action, __ = ag.get_action(first_msg_on_inbox=None)
            self.assertEqual(action.parameters['remote_nodeID'], 2)

            # check: middle node, even index
            self.memorymanager.setEntangled(ELECTRON_POSITION, 3)
            ag = \
                OddInitiateSwapAsapActionGenerator(
                    number_of_nodes=number_of_nodes,
                    index=2,
                    memorymanager=self.memorymanager)
            action, __ = ag.get_action(first_msg_on_inbox=None)
            self.assertEqual(action, None)

            # check: rightmost node
            self.memorymanager.setEntangled(ELECTRON_POSITION,
                                            number_of_nodes - 2)
            ag = \
                OddInitiateSwapAsapActionGenerator(
                    number_of_nodes=number_of_nodes,
                    index=number_of_nodes - 1,
                    memorymanager=self.memorymanager)
            action, __ = ag.get_action(first_msg_on_inbox=None)
            self.assertEqual(action, None)

        # CASE: node in middle of chain already has
        # entanglement with both left and right
        for number_of_nodes in [6, 7]:
            self.memorymanager.setEntangled(ELECTRON_POSITION, 2)
            self.memorymanager.setEntangled(ELECTRON_POSITION + 1, 4)
            ag = \
                OddInitiateSwapAsapActionGenerator(
                    number_of_nodes=number_of_nodes,
                    index=3,
                    memorymanager=self.memorymanager)
            action, __ = ag.get_action(first_msg_on_inbox=None)
            self.assertEqual(action.topic, "SWAP")


if __name__ == "__main__":
    unittest.main()
