from netsquid_nvrepchain.utils.tools import stop_simulation
import netsquid as ns
import unittest


class TestSimStop(unittest.TestCase):
    """
    Testing the function `tools.stop_simulation`, which stops
    the running simulation when it is called.
    """

    def setUp(self):
        ns.sim_reset()
        self.helper = ns.Entity("Helper entity")
        self.evtype = ns.EventType("INCR", "Increment counter")
        self.timeSteps = 10
        self.counter = 0
        self.handler_off = ns.EventHandler(lambda event: self.increment_counter(event))
        self.handler_on = ns.EventHandler(lambda event, stop_on=True: self.increment_counter(event, stop_on=stop_on))

    def reset(self):
        self.counter = 0

    def test_test_works_correctly(self):
        # testing if this test itself works correctly
        self.reset()
        self.helper._wait(self.handler_off, event_type=self.evtype)
        self.helper._schedule_now(self.evtype)
        ns.sim_run(110)
        self.assertEqual(self.counter, 11)

    def test_simstop(self):
        # testing if the function `tools.stop_simulation` works
        self.reset()
        self.helper._wait(self.handler_on, event_type=self.evtype)
        self.helper._schedule_now(self.evtype)
        ns.sim_run(70)
        self.assertEqual(self.counter, 3)

    def increment_counter(self, event, stop_on=False):
        self.counter += 1
        if stop_on and self.counter == 3:
            stop_simulation()
        self.helper._schedule_after(self.timeSteps, self.evtype)


if __name__ == "__main__":
    unittest.main()
