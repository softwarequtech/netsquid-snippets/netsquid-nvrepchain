import unittest
from netsquid_nvrepchain.utils.memorymanager import Link, MemoryTracker
import math


class TestLink(unittest.TestCase):

    def test_init(self):
        Link(remote_nodeID=42, linkID=3)


class TestMemoryTracker(unittest.TestCase):

    def setUp(self):
        self.num_positions = 30
        self.mt = MemoryTracker(num_positions=self.num_positions)

    def test_setting_entanglement(self):
        self.mt.reset()
        positions = [1, 3, 5, 7, 11, 13, 17, 19, 23, 29]
        remotes = [11, 13, 13, 13, 17, 17, 17, 17, 17, 17]
        for (pos, remote_nodeID) in [(positions[i], remotes[i]) for i in range(len(positions))]:
            self.mt.setEntangled(pos=pos, remote_nodeID=remote_nodeID)
        # asserting only the entangled positions are not free
        for pos in range(self.num_positions):
            if pos in positions:
                self.assertFalse(self.mt.isFree(pos=pos))
            else:
                self.assertTrue(self.mt.isFree(pos=pos))

        # test correctness of assigned linkIDs
        counter = {remote: 1 for remote in remotes}
        for i in range(len(positions)):
            pos = positions[i]
            remote_nodeID = remotes[i]
            self.assertEqual(self.mt.getLink(pos=pos).linkID, counter[remote_nodeID])
            counter[remote_nodeID] += 1

    def test_swap(self):
        self.mt.reset()
        # set the entanglement with remote
        posA, posB = 13, 17
        remote_nodeID_A, remote_nodeID_B = posA, posB
        for (pos, remote_nodeID) in [(posA, remote_nodeID_A),
                                     (posB, remote_nodeID_B)]:
            self.mt.setEntangled(pos=pos, remote_nodeID=remote_nodeID)
        # swap the entanglement with remote
        self.mt.swap(posA=posA, posB=posB)
        # assert
        for (pos, new_remote_nodeID) in [(posA, remote_nodeID_B),
                                         (posB, remote_nodeID_A)]:
            self.assertEqual(self.mt.getLink(pos=pos).remote_nodeID, new_remote_nodeID)

    def test_hasFreePositions(self):
        self.mt.reset()
        for n in range(self.num_positions):
            self.mt.reset()
            for pos in range(n):
                self.mt.setEntangled(pos=pos, remote_nodeID=n)
            for k in range(self.num_positions):
                if k > self.num_positions - n:
                    self.assertFalse(self.mt.hasFreePositions(number=k))
                else:
                    self.assertTrue(self.mt.hasFreePositions(number=k))

    def test_getAllEntanglementWith(self):
        remote_nodeID = 3

        # without fidelity constraint
        self.mt.reset()
        for pos in range(self.num_positions):
            self.mt.setEntangled(pos=pos, remote_nodeID=remote_nodeID)
            all_ent = self.mt.getAllEntanglementWith(remote_nodeID=remote_nodeID)
            self.assertEqual(all_ent, list(range(pos + 1)))

        # including fidelity constraint
        self.mt.reset()
        positions_that_satisfy_condition = []
        for pos in range(self.num_positions):

            # set entanglement
            self.mt.setEntangled(pos=pos, remote_nodeID=remote_nodeID)
            condition = pos < self.num_positions / 2.
            self.mt.getLink(pos=pos).estimated_fidelity = 0.5 if condition else 1.
            if condition:
                positions_that_satisfy_condition.append(pos)

            # check correctness
            all_ent = self.mt.getAllEntanglementWith(remote_nodeID=remote_nodeID,
                                                     fidelity_range=[0.3, 0.7])
            self.assertEqual(all_ent, positions_that_satisfy_condition)

    def test_get_free(self):
        self.mt.reset()
        free_positions = list(range(self.num_positions))

        for pos in range(self.num_positions):

            # set entanglement
            remote_nodeID = pos
            self.mt.setEntangled(pos=pos, remote_nodeID=remote_nodeID)
            free_positions.remove(pos)

            # check correctness
            self.assertEqual(self.mt.getAllFree(), free_positions)
            excluded = [2 * pos for pos in range(int(math.ceil(self.num_positions / 2.)))]
            expected_output = sorted([pos for pos in free_positions if pos not in excluded])
            self.assertEqual(sorted(self.mt.getAllFree(excluded=excluded)), expected_output)


if __name__ == "__main__":
    unittest.main()
