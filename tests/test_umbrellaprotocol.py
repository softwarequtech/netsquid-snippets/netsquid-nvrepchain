import unittest
from netsquid_nvrepchain.runtools.umbrellaprotocol import create_umbrellaprot
from netsquid_nvrepchain.utils.testtools import StoreObject
from netsquid_nvrepchain.utils.message import getNewMessage
from netsquid_nvrepchain.network.linenetwork import LineNetwork
import netsquid as ns
from netsquid_nvrepchain.utils.repchain_parameter_tools import get_nv_dict_perfect_version, filter_on_qmem_config_params


class Test_UmbrellaProtocol(unittest.TestCase):

    def setUp(self):
        ns.set_qstate_formalism(ns.QFormalism.DM)
        ns.sim_reset()
        self.number_of_nodes = 5
        self.qmem_config_params = \
            get_nv_dict_perfect_version(filter_on_qmem_config_params)
        self.ln = LineNetwork(number_of_nodes=self.number_of_nodes,
                              qmem_config_params=self.qmem_config_params,
                              distributor_params={"cycle_time": 10})

    def test_receiving_messages(self):
        for use_swap_only in [True, False]:
            ns.sim_reset()
            index = self.number_of_nodes - 1
            linenetwork = LineNetwork(number_of_nodes=self.number_of_nodes,
                                      qmem_config_params=self.qmem_config_params,
                                      distributor_params={"cycle_time": 10})
            up = create_umbrellaprot(index=index,
                                     linenetwork=linenetwork,
                                     use_swap_only=use_swap_only)
            up.start()

            up.remove_item('logic')
            up['logic'] = StoreObject()
            up['logic'].process_msgs = lambda direction, msglist: \
                up['logic'].set(obj=(direction, msglist))

            sender = 4
            msg_sent = getNewMessage(sender=sender,
                                     header_type="ENTGEN",
                                     content_type="ASK")
            linenetwork.class_channels_receive[index]["L"].send(items=msg_sent)
            ns.sim_run()
            (direction, [msg_received]) = up['logic'].object
            self.assertEqual(direction, "L")
            self.assertEqual(msg_sent, msg_received)


if __name__ == "__main__":
    unittest.main()
