[![pipeline status](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-nvrepchain/badges/master/pipeline.svg)](https://gitlab..nl/softwarequtech/netsquid-snippets/netsquid-nvrepchain/commits/master)

NetSquid-NVRepchain (1.0.3)
================================

Description
-----------

This is a user contributed _snippet_ for the [NetSquid quantum network simulator](https://netsquid.org) and contains tools for simulating repeater chains of nitrogen-vacancy (NV) centers in diamond. 
As example, the script `example_basic.py` in the `examples` folder sets up a repeater chain of an arbitrary number of nodes, each of which holds a single NV center. Following the comments in the script, it is straightforward to modify:

- the number of nodes in the chain;
- the hardware parameters of the NV center and other network components. For example: the coherence time T2 of the carbon nuclear memories and the error models of the native NV gates, and the dark count of the detectors;
- the protocol: either SWAP-ASAP (no distillation) or a nested protocol with distillation at each nesting level, as well as a choice for the brigh-state parameter &alpha;
- the distance between the nodes.

The physical modelling of the NV centers and the repeater protocols are explained in the article [NetSquid, a discrete-event simulation platform for quantum networks](https://arxiv.org/abs/2010.12535).

Installation
------------

See the [INSTALL file](INSTALL.md) for instruction of how to install this snippet.

Usage
-----

Starting to use this snippet is most easily done by (playing with) the script `example_basic.py` in the `examples` folder. See also `Description` above.

Documentation
-------------

To build and see the docs see the [docs README](docs/README.md).

Contributors
------------

Tim Coopmans (t.j.coopmans[at]tudelft.nl)

License
-------

`netsquid-nvrepchain` has the following license:

> Copyright 2020 QuTech (TUDelft and TNO)
>
>   Licensed under the Apache License, Version 2.0 (the "License");
>   you may not use this file except in compliance with the License.
>   You may obtain a copy of the License at
>
>     http://www.apache.org/licenses/LICENSE-2.0
>
>   Unless required by applicable law or agreed to in writing, software
>   distributed under the License is distributed on an "AS IS" BASIS,
>   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>   See the License for the specific language governing permissions and
>   limitations under the License.
