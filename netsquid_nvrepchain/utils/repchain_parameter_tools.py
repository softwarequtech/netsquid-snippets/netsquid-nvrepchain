from netsquid_nv.delft_nvs.delft_nv_2019 import NVParameterSet2019


_QMEM_CONFIG_PARAM_NAMES = [
    "electron_init_depolar_prob",
    "electron_single_qubit_depolar_prob",
    "prob_error_0",
    "prob_error_1",
    "carbon_init_depolar_prob",
    "carbon_z_rot_depolar_prob",
    "ec_gate_depolar_prob",
    "electron_T1",
    "electron_T2",
    "carbon_T1",
    "carbon_T2"]

_MAGIC_DISTRIBUTOR_PARAM_NAMES = [
    "prob_dark_count",
    "prob_detect_excl_transmission_with_conversion_with_cavities",  # NOTE this one is renamed in the function below
    "visibility",
    "product_tau_decay_delta_w",
    "photon_emission_delay",  # NOTE this one is renamed in the function below
    "p_double_exc",
    "p_fail_class_corr",
    "coherent_phase",
    "std_phase_interferometric_drift",
    "initial_nuclear_phase",
    "p_loss_lengths_with_conversion"]


def filter_on_qmem_config_params(nv_param_dict):
    """
    Parameters
    ----------
    nv_param_dict: dict (str: Any)

    Returns
    -------
    dict (str: Any)
    """
    return {name: nv_param_dict[name] for name in _QMEM_CONFIG_PARAM_NAMES}


def get_nv_dict_perfect_version(func):
    """
    Parameters
    ----------
    func: function that takes a single argument dict (str: Any)
        The input argument is a dictionary of NV parameters.

    Returns
    -------
    dict (str: Any)

    Example
    -------

    >>> import netsquid_nvrepchain.utils.repchain_parameter_tools as param_tools
    >>> get_nv_dict_perfect_version(param_tools.filter_on_qmem_config_params)
    """
    return func(NVParameterSet2019().to_perfect_dict())


def filter_on_magic_distributor_params(nv_param_dict):
    """
    Parameters
    ----------
    nv_param_dict: dict (str: Any)

    Returns
    -------
    dict (str: Any)
    """
    new_param_dict = \
        {name: nv_param_dict[name] for name in _MAGIC_DISTRIBUTOR_PARAM_NAMES}

    for old_name, new_name in \
        [("prob_detect_excl_transmission_with_conversion_with_cavities", "p_det"),
         ("photon_emission_delay", "cycle_time"),
         ("product_tau_decay_delta_w", "tau_decay"),
         ("prob_dark_count", "p_dark_count")]:
        new_param_dict[new_name] = new_param_dict[old_name]
        del new_param_dict[old_name]
    new_param_dict["delta_w"] = 1
    return new_param_dict
