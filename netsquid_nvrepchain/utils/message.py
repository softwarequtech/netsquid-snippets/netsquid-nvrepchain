class Message:
    """
    Classical message interface for sending over a classical connection.

    There are three different "subjects" (`header`) for a message:
      * swapping
      * distilling
      * neighbouring entanglement generation

    In addition to this, there are different contents for a message:
      * ask: the sender requests action
      * confirm: the receiver confirms that it will start the action
      * reject: the receiver rejects the sender's request
      * (only for distillation) 'distillation outcome': message that contains the distillation outcome
        in `additional_content`.

    Attributes
    ----------
    header : string
        Indicates the topic of the message: entanglement generation/distillation/swapping. Should be among
        one of the following: \"ENTGEN\", \"DIST\", \"SWAP\".
    sender : int
        The nodeID of the node that sent that message.
    intended_receiver : int
        Optional.
    content : string
        Should be among one of the following: \"ASK\", \"CONFIRM\", \"REJECT\", \"DISTOUTCOME\".
        It contains (linkID, new_remote_nodeID)

    additional_content : string
        Can be used to store any addional data. In the case of a distillation outcome, this contains the
        distillation outcome. In the case of a swap update,  and possibly a correction Pauli.
    """

    # standard header types
    _SWAP_MSG_HDR = "SWAP"
    _ENTGEN_MSG_HDR = "ENTGEN"
    _DIST_MSG_HDR = "DIST"
    #
    _HEADER_DICT = {"SWAP": _SWAP_MSG_HDR,
                    "ENTGEN": _ENTGEN_MSG_HDR,
                    "DIST": _DIST_MSG_HDR}

    # standards content types
    _ASK = "ASK"
    _CONFIRM = "CONFIRM"
    _REJECT = "REJECT"
    _DISTOUTCOME = "DISTOUTCOME"
    #
    _CONTENT_DICT = {"ASK": _ASK,
                     "CONFIRM": _CONFIRM,
                     "REJECT": _REJECT,
                     "DISTOUTCOME": _DISTOUTCOME}

    @staticmethod
    def _check_content_type(header_type, content_type):
        if header_type == "ENTGEN":
            if content_type not in ["ASK", "CONFIRM", "REJECT"]:
                raise ValueError("content_type should be among [\"ASK\", \"CONFIRM\", \"REJECT\"]")
        if header_type == "DIST":
            if (not isinstance(content_type, int)) and \
               (content_type not in ["ASK", "CONFIRM", "REJECT", "DISTOUTCOME"]):
                raise ValueError("content_type should be among [\"ASK\", \"CONFIRM\", \"REJECT\", \"DISTOUTCOME\"]")

    @staticmethod
    def _check_header_type(header_type):
        if header_type not in ["ENTGEN", "DIST", "SWAP"]:
            raise ValueError("header_type should be among [\"ENTGEN\", \"DIST\", \"SWAP\"]")

    def __init__(self, header, sender, content, additional_content=None,
                 intended_receiver=None, expires_after=None):
        # the header is the topic of the message
        self.header = header
        # the sender is the index of the node, and should be of type `int`
        assert(isinstance(sender, int))
        self.sender = sender
        self.intended_receiver = intended_receiver
        self.content = content
        self.additional_content = additional_content
        self.expires_after = expires_after

    def __eq__(self, other):
        return self.header == other.header and\
            self.sender == other.sender and\
            self.intended_receiver == other.intended_receiver and\
            self.content == other.content and\
            self.additional_content == other.additional_content

    def __neq__(self, other):
        return not (self == other)

    def __str__(self):
        string = "({}, {}>{}, {})".format(self.header, self.sender, self.intended_receiver, self.content)
        if self.additional_content is not None:
            string = "{};{})".format(string[:-1], self.additional_content)
        return string

    def isOfContentType(self, content_type):
        """content_type should be among [\"ASK\", \"CONFIRM\", \"REJECT\", \"DISTOUTCOME\"]"""

        # DIST has the most possible content_types
        Message._check_content_type(content_type=content_type, header_type="DIST")

        return self.content == Message._CONTENT_DICT[content_type]

    def isOfHeaderType(self, header_type):
        """header_type should be among [\"ENTGEN\", \"DIST\", \"SWAP\"]"""
        Message._check_header_type(header_type=header_type)
        return self.header == Message._HEADER_DICT[header_type]

    def isOfType(self, header_type, content_type):
        return self.isOfHeaderType(header_type=header_type) and self.isOfContentType(content_type=content_type)


def getNewMessage(sender, header_type="DIST", content_type="ASK", additional_content=None, intended_receiver=None,
                  expires_after=None):
    """
        header_type should be among [\"ENTGEN\", \"DIST\", \"SWAP\"]
    """
    Message._check_header_type(header_type)
    Message._check_content_type(header_type, content_type)
    header = Message._HEADER_DICT[header_type]
    if header == Message._SWAP_MSG_HDR:
        content = content_type
    else:
        # TODO: improve: right now, a content type is fixed, but there is only one type of swap messages
        content = Message._CONTENT_DICT[content_type]
    return Message(header=header, sender=sender, content=content, additional_content=additional_content,
                   intended_receiver=intended_receiver, expires_after=expires_after)
