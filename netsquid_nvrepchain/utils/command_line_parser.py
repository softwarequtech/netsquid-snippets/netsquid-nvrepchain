"""
This script holds the function `parse_parameters_from_command_line`, which
can be used to directly pass on arguments given in the command line into
a python method.

Example
-------


>>> from netsquid_nvrepchain.utils.command_line_parser import parse_parameters_from_command_line
>>>
>>>
>>> # Function used in *example without decorator* (see below)
>>> def compute_sum(firstnumber, secondnumber, filebasename=None):
>>>     print(f"Filebasename: {filebasename}")
>>>     return firstnumber + secondnumber
>>>
>>>
>>> # Function used in *example with decorator* (see below)
>>> @parse_parameters_from_command_line
>>> def compute_product(firstnumber, secondnumber):
>>>     return firstnumber * secondnumber
>>>
>>>
>>> if __name__ == "__main__":
>>>
>>>     # First example, without decorator:
>>>     # the function `compute_sum` is given its parameters by
>>>     # the function `parse_parameters_from_command_line`, which
>>>     # gets its parameters while calling this script
>>>     print("Example without decorator:")
>>>     computed_sum = parse_parameters_from_command_line(compute_sum)()
>>>     print(f"Computed sum: {computed_sum}")
>>>
>>>     input("\nPress any key to continue to next example: using the decorator")
>>>
>>>     # Second example, with decorator:
>>>     # the function `compute_product` has the decorator
>>>     # (the line `@parse_parameters_from_command_line`
>>>     # written before it. This makes that we need
>>>     # not call the `parse_parameters_from_command_line`
>>>     # explicitly.
>>>     print("\nExample with decorator:")
>>>     computed_product = compute_product()
>>>     print(f"Computed product: {computed_product}")
"""


import sys


_NUM_ERROR = \
    TypeError("Needs even number of arguments on command line: names & values")


STR_NAMES = ["filebasename", "long_description", "descr"]


def _parameter_input_list_to_dict(params, str_names=None):
    ret = {}
    if str_names is None:
        str_names = []
    if len(params) % 2 != 0:
        raise _NUM_ERROR
    for k in range(int(len(params) / 2)):
        name = params[2 * k]
        val = params[2 * k + 1]
        if name not in str_names:
            # make into float or int
            val = float(val)
            if val.is_integer():
                val = int(val)
        ret[name] = val
    return ret


def parse_parameters_from_command_line(func):
    d = _parameter_input_list_to_dict(sys.argv[1:],
                                      str_names=STR_NAMES)
    return lambda: func(**d)
