import logging
# logging.basicConfig(level=logging.INFO)
# logging.basicConfig(level=logging.DEBUG)
import netsquid as ns
# ns.set_console_debug()

# import coloredlogs
# coloredlogs.install()

_previous_time = None


def _make_spaces(num_of_spaces):
    logstring = ""
    for __ in range(num_of_spaces):
        logstring += "\t\t\t"
    return logstring


def _get_timestring():
    time = ns.sim_time()
    global _previous_time
    if _previous_time is None or time > _previous_time:
        _previous_time = time
        logging.info("-" * 178)
    num_of_spaces = len(str(time))
    string = ""
    for __ in range(0, 10 - num_of_spaces):
        string += " "
    return string + str(time)


def _get_logstring(index_sender, index_receiver, msg, position):
    """logstring for sending classical message"""
    msg = msg[0]
    if isinstance(msg, list):
        msgstring = ""
        for m in msg:
            msgstring += str(m)
    else:
        msgstring = str(msg)
    logstring = "{} --[{}]--> {}".format(index_sender, msgstring, index_receiver)
    return _get_timestring() + _make_spaces(position + 1) + logstring


def log_send(myindex, remotename, msg):
    return _get_logstring(index_sender=myindex,
                          index_receiver=remotename,
                          msg=msg,
                          position=myindex)


def log_received(myindex, remotename, msg):
    return _get_logstring(index_sender=remotename,
                          index_receiver=myindex,
                          msg=msg,
                          position=myindex)


def log_localinfo(myindex, info):
    return _get_timestring() + _make_spaces(myindex + 1) + info
