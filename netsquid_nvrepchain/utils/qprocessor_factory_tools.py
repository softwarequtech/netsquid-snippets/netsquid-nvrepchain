import numpy as np
from netsquid.components.instructions import (
    INSTR_X, INSTR_Y, INSTR_Z, INSTR_ROT_X,
    INSTR_ROT_Y, INSTR_ROT_Z, INSTR_H,
    INSTR_INIT, INSTR_CXDIR, INSTR_MEASURE,
    INSTR_SWAP)
from netsquid.components.qprocessor import QuantumProcessor
from netsquid.qubits.operators import Operator
from netsquid.components.models.qerrormodels import (
    DepolarNoiseModel,
    T1T2NoiseModel)
from netsquid.components.qprocessor import PhysicalInstruction


def create_QPD(name, tot_num_qubits, electron_init_depolar_prob,
               electron_single_qubit_depolar_prob, prob_error_0,
               prob_error_1, carbon_init_depolar_prob,
               carbon_z_rot_depolar_prob, ec_gate_depolar_prob,
               electron_T1, electron_T2, carbon_T1, carbon_T2,
               use_magical_swap=False):

    """
    Parameters
    ----------
    tot_num_qubits: int
    electron_init_depolar_prob: float
    electron_single_qubit_depolar_prob: float
    prob_error_0: float
    prob_error_1: float
    carbon_init_depolar_prob: float
    carbon_z_rot_depolar_prob: float
    ec_gate_depolar_prob: float
    electron_T1: float
    electron_T2: float
    carbon_T1: float
    carbon_T2: float

    Returns
    -------
    :obj:`netsquid_physlayer.util.QPD_Configs.qpdconfig.QPDConfig`
    """

    # noise models for single- and multi-qubit operations
    electron_init_noise = \
        DepolarNoiseModel(depolar_rate=electron_init_depolar_prob,
                          time_independent=True)

    electron_single_qubit_noise = \
        DepolarNoiseModel(depolar_rate=electron_single_qubit_depolar_prob,
                          time_independent=True)

    carbon_init_noise = \
        DepolarNoiseModel(depolar_rate=carbon_init_depolar_prob,
                          time_independent=True)

    carbon_z_rot_noise = \
        DepolarNoiseModel(depolar_rate=carbon_z_rot_depolar_prob,
                          time_independent=True)

    ec_noise = \
        DepolarNoiseModel(depolar_rate=ec_gate_depolar_prob,
                          time_independent=True)

    electron_qubit_noise = \
        T1T2NoiseModel(T1=electron_T1, T2=electron_T2)

    carbon_qubit_noise = \
        T1T2NoiseModel(T1=carbon_T1, T2=carbon_T2)

    # defining gates and their gate times

    phys_instructions = []

    electron_position = 0
    carbon_positions = [pos + 1 for pos in range(tot_num_qubits - 1)]

    phys_instructions.append(
        PhysicalInstruction(INSTR_INIT,
                            parallel=False,
                            topology=carbon_positions,
                            q_noise_model=carbon_init_noise,
                            apply_q_noise_after=True,
                            duration=310e3))

    phys_instructions.append(
        PhysicalInstruction(INSTR_ROT_Z,
                            parallel=False,
                            topology=carbon_positions,
                            q_noise_model=carbon_z_rot_noise,
                            apply_q_noise_after=True,
                            duration=20e3))

    phys_instructions.append(
        PhysicalInstruction(INSTR_INIT,
                            parallel=False,
                            topology=[electron_position],
                            q_noise_model=electron_init_noise,
                            apply_q_noise_after=True,
                            duration=2e3))

    for instr in [INSTR_X, INSTR_Y, INSTR_Z, INSTR_ROT_X, INSTR_ROT_Y, INSTR_ROT_Z, INSTR_H]:
        phys_instructions.append(
            PhysicalInstruction(instr,
                                parallel=False,
                                topology=[electron_position],
                                q_noise_model=electron_single_qubit_noise,
                                duration=5))

    electron_carbon_topologies = \
        [(electron_position, carbon_pos) for carbon_pos in carbon_positions]
    phys_instructions.append(
        PhysicalInstruction(INSTR_CXDIR,
                            parallel=False,
                            topology=electron_carbon_topologies,
                            q_noise_model=ec_noise,
                            apply_q_noise_after=True,
                            duration=500e3))

    M0 = Operator("M0", np.diag([np.sqrt(1 - prob_error_0), np.sqrt(prob_error_1)]))
    M1 = Operator("M1", np.diag([np.sqrt(prob_error_0), np.sqrt(1 - prob_error_1)]))

    # hack to set imperfect measurements
    INSTR_MEASURE._meas_operators = [M0, M1]

    phys_instr_measure = PhysicalInstruction(INSTR_MEASURE,
                                             parallel=False,
                                             topology=[electron_position],
                                             q_noise_model=None,
                                             duration=3.7e3)

    phys_instructions.append(phys_instr_measure)

    if use_magical_swap:
        # the noise in the swap circuit (fig. 1C, orange box in 'entanglement
        # distillation between solid-state
        # quantum network nodes') is dominated by the noise of twice the
        # electron-carbon gate
        magical_swap_gate_depolar_prob = 1 - (1 - ec_gate_depolar_prob) ** 2
        magic_swap_noise = DepolarNoiseModel(depolar_rate=magical_swap_gate_depolar_prob,
                                             time_independent=True)
        phys_instructions.append(
            PhysicalInstruction(INSTR_SWAP,
                                parallel=False,
                                topology=electron_carbon_topologies,
                                q_noise_model=magic_swap_noise,
                                apply_q_noise_after=True,
                                duration=2 * 500e3 + 2 * 5))

    # add qubits
    mem_noise_models = [electron_qubit_noise] + \
                       [carbon_qubit_noise] * len(carbon_positions)
    qmem = QuantumProcessor(name=name,
                            num_positions=tot_num_qubits,
                            mem_noise_models=mem_noise_models,
                            phys_instructions=phys_instructions)
    qmem._fail_exception = True
    return qmem
