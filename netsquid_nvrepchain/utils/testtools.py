import netsquid as ns
import numpy as np
from netsquid.components.instructions import INSTR_X
from netsquid.qubits.qubitapi import create_qubits, operate
from netsquid.qubits.operators import Operator, CNOT, H, I, X, Y, Z
from netsquid.qubits.dmtools import DMState
from netsquid_nvrepchain.utils import tools
from netsquid_nvrepchain.utils.logging_tools import logging


def _add_qubit(memorymanager, procDev, pos, remote_nodeID, state="|0>"):
    assert(state in ["|0>", "|1>"])
    qubits = ns.qubits.qubitapi.create_qubits(1)
    procDev.put(qubits=qubits, positions=[pos])
    if state == "|1>":
        procDev.execute_instruction(INSTR_X, [pos], physical=False)
    memorymanager.setEntangled(pos=pos,
                               remote_nodeID=remote_nodeID)


def _add_bell_state(memorymanager, qmem, posX, posY, remote_nodeID_X, remote_nodeID_Y, bell_index):
    tools._add_Bell_pair(qmemA=qmem,
                         qmemB=qmem,
                         posA=posX,
                         posB=posY,
                         bell_index=bell_index)
    for (pos, remote_nodeID) in [(posX, remote_nodeID_X),
                                 (posY, remote_nodeID_Y)]:
        memorymanager.setEntangled(pos=pos,
                                   remote_nodeID=remote_nodeID)


class StoreObject:

    evtype = ns.EventType("EVT", "Some event type")

    def __init__(self):
        self.object = None
        self.locallist = []

    def set(self, obj):
        logging.debug('StoreObject with id {}: setting to {}'.format(id(self), obj))
        self.object = obj

    def append(self, element):
        self.locallist.append(element)

    def reset(self):
        self.__init__()


def _add_Bell_pair(qmemA, qmemB, posA, posB, bell_index=0):
    """
    bell_index : int
        0=Phi_plus, 1=Psi_plus, 2=Phi_minus, 3=Psi_minus
    """
    # TODO refer to _add_noisy_Bell_pair
    # putting a Bell pairs (Phi_plus) in the node's memories by
    # - let each node have a qubits in state |0>
    # - nodeA applies H to its qubit
    # - we apply a nonlocal CNOT
    for (qmem, pos) in [(qmemA, posA), (qmemB, posB)]:
        qubits = create_qubits(1)
        qmem.put(qubits=qubits, positions=[pos])
    [qA] = qmemA.peek(positions=posA)
    [qB] = qmemB.peek(positions=posB)
    operate(qubits=[qA], operator=H)
    operate(qubits=[qA, qB], operator=CNOT)
    bellindex2operator = {0: I,
                          1: X,
                          2: Z,
                          3: Y}
    operate(qubits=[qB], operator=bellindex2operator[bell_index])


_oneoneprojector = \
    np.array([[0., 0., 0., 0.],
              [0., 0., 0., 0.],
              [0., 0., 0., 0.],
              [0., 0., 0., 1.]])
_oneoneprojop = Operator(name="oneoneproj_unnormalized", matrix=_oneoneprojector)


def _add_noisy_Bell_pair(qmemA, qmemB, posA, posB, p, bell_index=1):
    """
    Outputs the state F * |b> + (1-F) * |11><11|
    where |b> is a bell state,
    where F^2 = p.

    Parameters
    ----------
    bell_index : int
        Either 0, 1, 2 or 3, where:
          * 0 : Phi^+
          * 1 : Psi^+
          * 2 : Phi^-
          * 3 : Psi^-
    """
    F = np.sqrt(p)
    qubits = {}
    for (qmem, pos, name) in [(qmemA, posA, "A"), (qmemB, posB, "B")]:
        qmem.create_qubits_at(positions=[pos])
        [qubits[name]] = qmem.peek(positions=pos)
    np.array([[0., 0., 0., 0.],
              [0., 0., 0., 0.],
              [0., 0., 0., 0.],
              [0., 0., 0., 1.]])

    if bell_index >= 2:
        operate(qubits=[qubits["A"]], operator=X)
    if bell_index % 2 == 1:
        operate(qubits=[qubits["B"]], operator=X)
    operate(qubits=[qubits["A"]], operator=H)
    operate(qubits=[qubits["A"], qubits["B"]],
            operator=CNOT)
    bell_state_dm = qubits["A"].qstate.dm
    DMState(qubits=[qubits["A"], qubits["B"]], dm=F * bell_state_dm + (1 - F) * _oneoneprojector)
    # stochastic_operate(qubits=[qubits["A"], qubits["B"]],
    #                    operators = [I^I, oneoneproj_renormalized],
    #                    p_weights=[p, 1-p])
    assert(np.isclose(np.trace(qubits["A"].qstate.dm), 1.))
