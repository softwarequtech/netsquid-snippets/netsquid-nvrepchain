"""
Convention: counting levels starts at 1. That is, a node with table of height 1 has a single row, to which we refer as
row 1 (instead of row 0).
"""
import numpy as np


def num2level(number_of_nodes):
    """
    Input: number of nodes 2^d+1
    Output: d
    """
    if number_of_nodes < 2:
        raise ValueError
    if number_of_nodes == 2:
        return 0
    else:
        return num2level((number_of_nodes - 1) / 2 + 1) + 1


def level2num(level):
    """
    Input: level d
    Output: number of nodes 2^d + 1
    """
    return 2**level + 1


def num2numOneLower(number_of_nodes):
    """
    Input: 2^(d+1) + 1
    Output: 2^d + 1
    """
    level = num2level(number_of_nodes)
    return level2num(level - 1)


def _get_mirrorindex(number_of_nodes, index):
    return (number_of_nodes - 1) - index


def get_height(number_of_nodes, index):
    """
    Parameters:
    num : int
        Number of nodes in the chain
        (must be of the form 2^d+1 for some d>=1, but this is not checked!)
    index : int
        Index of one of the nodes (must be a number in the range [0, 1, ..., num-1]
    """
    assert(index >= 0 and index < number_of_nodes)
    if number_of_nodes == 3:
        return 1
    else:
        middlenode = (number_of_nodes - 1) / 2
        middleheight = num2level(number_of_nodes=number_of_nodes)
        numOneLower = num2numOneLower(number_of_nodes=number_of_nodes)
        if index in [0, middlenode, number_of_nodes - 1]:
            return middleheight
        elif index > middlenode:
            mirrorindex = _get_mirrorindex(number_of_nodes=number_of_nodes, index=index)
            return get_height(number_of_nodes=numOneLower, index=mirrorindex)
        else:
            return get_height(number_of_nodes=numOneLower, index=index)


def get_ngbh(number_of_nodes, index, level, direction, nonevalue=-1):
    """
    'level' is a number in the range [1, 2, ..., height]
    """
    assert(direction in ["L", "R"])
    assert(isinstance(nonevalue, int))
    assert(number_of_nodes >= 2)
    if index == 0 and direction == "L":
        return nonevalue
    if index == number_of_nodes - 1 and direction == "R":
        return nonevalue
    return _get_ngbh(number_of_nodes, index, level, direction)


def _get_ngbh(number_of_nodes, index, level, direction):
    if number_of_nodes == 3:
        return index - 1 if direction == "L" else index + 1
    else:
        middleNode = (number_of_nodes - 1) / 2
        if index < middleNode:
            if level < num2level(number_of_nodes=number_of_nodes):
                return _get_ngbh(number_of_nodes=num2numOneLower(number_of_nodes=number_of_nodes),
                                 index=index, level=level, direction=direction)
            else:
                # must be leftmost node, highest level
                return 0 if direction == "L" else middleNode
        elif index > middleNode:
            if level < num2level(number_of_nodes=number_of_nodes):
                mirrorindex = _get_mirrorindex(number_of_nodes=number_of_nodes, index=index)
                mirrordirection = "L" if direction == "R" else "R"
                ngbh = _get_ngbh(number_of_nodes=num2numOneLower(number_of_nodes=number_of_nodes),
                                 index=mirrorindex,
                                 level=level,
                                 direction=mirrordirection)
                return _get_mirrorindex(number_of_nodes=number_of_nodes, index=ngbh)
            else:
                # must be rightmost node, highest level
                return middleNode if direction == "L" else (number_of_nodes - 1) + middleNode
        elif index == middleNode:
            if level < num2level(number_of_nodes=number_of_nodes):
                return _get_ngbh(number_of_nodes=num2numOneLower(number_of_nodes=number_of_nodes),
                                 index=index, level=level, direction=direction)
            else:
                return 0 if direction == "L" else number_of_nodes - 1


def is_initiator(number_of_nodes, index, level):
    """
    Returns whether node with index `index` is
    an initiator at level `level`.

    Parameters
    ----------
    number_of_nodes : int
        Number of nodes in the chain.
    index : int
        Index of the node in the chain,
        where we start to count from the left
        and count 0, 1, 2, ..., number_of_nodes - 1
    level : int
    """
    # outermost nodes are always responders
    if index == 0 or index == number_of_nodes - 1:
        return False
    else:
        height = get_height(number_of_nodes=number_of_nodes, index=index)
        return level == height


def get_levels(number_of_nodes, index):
    height = get_height(number_of_nodes=number_of_nodes, index=index)
    return [level + 1 for level in range(height)]


def get_table(number_of_nodes, index, nonevalue=-1):
    """
    Returns a dict of dicts:
      * first parameter is "direction"
      * second parameter is "level"

    Examples
    --------
    >>> table = get_table(number_of_nodes=5, index=2)
    >>> print(table["L"][2])
    >>> # 0
    """
    levels = get_levels(number_of_nodes=number_of_nodes, index=index)
    table = {direction: None for direction in ["L", "R"]}

    for direction in ["L", "R"]:
        table[direction] = {level: None for level in levels}
        for level in levels:
            table[direction][level] = get_ngbh(number_of_nodes=number_of_nodes,
                                               index=index,
                                               level=level,
                                               direction=direction,
                                               nonevalue=nonevalue)
    return table


def remote_index2table_position(number_of_nodes, index, remote_index):
    table = get_table(number_of_nodes=number_of_nodes,
                      index=index)
    for direction in ["L", "R"]:
        for level in get_levels(number_of_nodes=number_of_nodes, index=index):
            if remote_index == table[direction][level]:
                return (direction, level)
    return (None, None)


def get_level(index, remote_index):
    """
    Returns
    -------
    int

    Note
    ----
    If nodes are neighbours, their level is 1 (not zero).
    """
    return int(np.log(np.abs(remote_index - index)) / np.log(2.)) + 1
