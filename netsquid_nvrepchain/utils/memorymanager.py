from netsquid import sim_time


ELECTRON_POSITION = 0


class Link:
    """
    Represents an entangled pair, as seen locally from a node.

    Attributes
    ----------
    remote_nodeID : int
        ID of the remote node that this link is entangled with.
    linkID : int
        ID of this link. This linkID should be identical for each
        of the two nodes that share the link.
    cor_Pauli: int
        Indicates which single-qubit Pauli *one* of the two nodes
        should apply in order to get the (expected) state Psi^+,
        where 0=Id, 1=X, 2=Y, 3=Z.
    """

    def __init__(self, remote_nodeID, linkID, estimated_fidelity=0.7, cor_Pauli=0):
        self.remote_nodeID = remote_nodeID
        self.linkID = linkID
        self.estimated_fidelity = estimated_fidelity
        self.cor_Pauli = cor_Pauli
        self.last_accessed = sim_time()
        self.creation_time = self.last_accessed

    def __str__(self):
        return "<--{}-->{} ({}, {})".format(self.linkID, self.remote_nodeID,
                                            self.estimated_fidelity,
                                            self.creation_time)

    def __eq__(self, other):
        return self.remote_nodeID == other.remote_nodeID and\
            self.linkID == other.linkID

    def __hash__(self):
        return hash(str(self.remote_nodeID) + "---" + str(self.linkID))


class MemoryTracker:
    """
    Datatype that holds Link objects for every qubit memory position
    in the ProcessingDevice of a QuantumNode. Could also be called
    "entanglement tracker". Its purpose is to keep track of which
    qubits in the ProcessingDevice are currently entangled with a
    remote node, and if so, with which node in particular.

    Notes
    -----
    The MemoryManager is completely independent of the ProcessingDevice
    in the sense that if a qubit is added to the ProcessingDevice,
    one should manually also add this fact to the MemoryManager.

    Attributes
    ----------
    num_positions : int
        Number of qubit memory positions that the memory holds.
    """

    def __init__(self, num_positions):
        self.num_positions = num_positions

        # Storing the links that this MemoryTracker holds
        # dictionary: keys=positions, values=Link objects
        self._links = {}

        # Keeping a counter of the number of links that this MemoryTracker holds
        # with every remote node
        # dictionary: keys=remote_nodeID, values=number of links shared with this remote node
        self._num_links = {}

        # for logging purposes
        self._max_number_of_pos_in_use = 0

    def reset(self):
        self.start()

    def start(self):
        self.__init__(num_positions=self.num_positions)

    def __str__(self):
        linksstring = "Links:\n"
        counterstring = "Counters:\n"
        remotes = set()
        positions = list(self._links.keys())
        sorted_positions = sorted(positions)
        for pos in sorted_positions:
            remote_nodeID = self.getRemoteNodeID(pos=pos)
            linksstring += "{} : {}\n".format(pos, self._links[pos])
            remotes.add(remote_nodeID)
        for remote_nodeID in remotes:
            counterstring += "{} : {}\n".format(remote_nodeID, self._num_links[remote_nodeID])
        return linksstring + counterstring

    ##############################################
    # *Setting* information in the MemoryTracker #
    ##############################################

    def setEntangled(self, pos, remote_nodeID, cor_Pauli=0):
        assert(0 <= pos <= self.num_positions)
        linkID = self._getNewLinkID(remote_nodeID=remote_nodeID)
        self._links[pos] = Link(remote_nodeID=remote_nodeID, linkID=linkID)
        self._links[pos].cor_Pauli = cor_Pauli
        self._max_number_of_pos_in_use = \
            max(self._max_number_of_pos_in_use,
                len(self._links))
        return self._links[pos]

    def setFree(self, pos):
        """
        Note
        ----
        Removes the link entirely from memory.
        """
        if not self.isFree(pos):
            del self._links[pos]
        else:
            raise Exception("Trying to set a position free\
                             which is not used")

    def _getNewLinkID(self, remote_nodeID):
        if not (remote_nodeID in self._num_links):
            self._num_links[remote_nodeID] = 1
        else:
            self._num_links[remote_nodeID] += 1
        return self._num_links[remote_nodeID]

#    def move(self, old_pos, new_pos):
#        assert(self.isFree(pos=new_pos))
#        link = self.getLink(pos=old_pos)
#        self._links.pop(old_pos)
#        self._links[new_pos] = link

    def swap(self, posA, posB):
        """
        Swap the links that position `posA` and
        `posB` hold.

        Note
        ----
        Does **not** physically move the qubits,
        only the information that the memorymanager
        has about them.
        """
        if self.isFree(pos=posA):
            if self.isFree(pos=posB):
                raise Exception("Trying two swap two empty memory positions")
            else:
                self._links[posA] = self.getLink(pos=posB)
                self._links.pop(posB)
        else:
            linkA = self.getLink(pos=posA)
            if not self.isFree(pos=posB):
                self._links[posA] = self.getLink(pos=posB)
            else:
                self._links.pop(posA)
            self._links[posB] = linkA

#    def update_cor_Pauli(self, pos, new_cor_Pauli):
#        link = self.getLink(pos=pos)
#        link.cor_Pauli = index_of_Pauli_product(Pauli_index_A=link.cor_Pauli,
#                                                Pauli_index_B=new_cor_Pauli)

    ################################################
    # *Getting* information from the MemoryTracker #
    ################################################

    def isFree(self, pos):
        """
        Parameters
        ----------
        pos : int
            Position of the qubit in memory.

        Returns
        -------
        bool
            Whether a link is associated with the qubit, or that it is considered free.

        Notes
        -----
        Does **not** make use of :obj:`easysquid.quantumMemory.QuantumProcessingDevice.in_use`.

        """
        return not (pos in self._links)

    def getRemoteNodeID(self, pos):
        return self.getLink(pos=pos).remote_nodeID

    def getLink(self, pos):
        assert(not self.isFree(pos=pos))
        return self._links[pos]

    def hasFreePositions(self, number=1):
        freeposcounter = 0
        pos = 0
        while pos < self.num_positions and freeposcounter < number:
            if self.isFree(pos=pos):
                freeposcounter += 1
            pos += 1
        return freeposcounter == number

    def findPos(self, remote_nodeID, linkID):
        """
        Returns
        -------
        int or None
        """
        for pos in range(0, self.num_positions):
            if not self.isFree(pos=pos):
                link = self.getLink(pos=pos)
                if link.remote_nodeID == remote_nodeID and link.linkID == linkID:
                    return pos
        return None

    def link2pos(self, link):
        for pos in range(0, self.num_positions):
            if not self.isFree(pos=pos) and self.getLink(pos=pos) == link:
                return pos
        return None

    def numLinks(self, remote_nodeID, fidelity_range=None):
        return len(self.getAllEntanglementWith(remote_nodeID=remote_nodeID,
                                               fidelity_range=fidelity_range))

    def getAllEntanglementWith(self, remote_nodeID, fidelity_range=None):
        """
        Returns
        -------
        list of int
            Memory positions.

        Note
        ----
        Includes the end points of the fidelity interval range.
        """
        if fidelity_range is None:
            fidelity_range = [0., 1.]
        posList = []
        for pos in range(0, self.num_positions):
            if not self.isFree(pos=pos) and self.getRemoteNodeID(pos=pos) == remote_nodeID:
                link = self.getLink(pos=pos)
                if fidelity_range[0] <= link.estimated_fidelity <= fidelity_range[1]:
                    posList.append(pos)
        return posList

    def hasEntanglementWithStrictlyMoreThan(self, number_of_links, remote_nodeID, fidelity_range=None):
        """
        Returns
        -------
        bool
        """
        if fidelity_range is None:
            fidelity_range = [0., 1.]
        counted_number_of_links = 0
        for pos in range(0, self.num_positions):
            if not self.isFree(pos=pos) and self.getRemoteNodeID(pos=pos) == remote_nodeID:
                link = self.getLink(pos=pos)
                if fidelity_range[0] <= link.estimated_fidelity <= fidelity_range[1]:
                    counted_number_of_links += 1
                    if counted_number_of_links > number_of_links:
                        return True
        return False

    def isEntangledWith(self, remote_nodeID):
        for pos in self._links:
            if self.getLink(pos=pos).remote_nodeID == remote_nodeID:
                return True
        return False

    def getAllFree(self, excluded=None):
        if excluded is None:
            excluded = []
        occupied_positions = set(self._links.keys())
        free_positions = set(range(self.num_positions)) - occupied_positions  # set complement
        return list(free_positions - set(excluded))

    def getRandomFree(self, num=1, excluded=None):
        return self.getFirstFree(num=num, excluded=excluded)

    def getFirstFree(self, num=1, excluded=None):
        if excluded is None:
            excluded = []
        all_free_positions = self.getAllFree(excluded=excluded)
        ret = all_free_positions[:num]
        if len(ret) == 0:
            return None
        elif len(ret) < num:
            raise Exception
        elif num == 1:
            return ret[0]
        else:
            return ret


class MemoryManager(MemoryTracker):

    def __init__(self, num_positions, discarding_strategy=None, procDev=None):
        super().__init__(num_positions=num_positions)
        self.procDev = procDev
        self.discarding_strategy = discarding_strategy
        self.communication_position = ELECTRON_POSITION

    def reset(self):
        self.__init__(num_positions=self.num_positions,
                      discarding_strategy=self.discarding_strategy,
                      procDev=self.procDev)

    def _update_estimated_fidelity(self, link):
        now = sim_time()
        link.last_accessed = now

        # TODO update estimated fidelity as function of dt, using
        # both the T1 and T2 time, and whether the position is the electron_position
        # (get T1 and T2 for both carbon and electron from self.procDev)
        link.estimated_fidelity = link.estimated_fidelity

    def update_estimated_fidelities(self):
        for __, link in self._links.items():
            self._update_estimated_fidelity(link=link)

    def discard(self, pos):
        self.setFree(pos=pos)
        if self.procDev is not None:
            self.procDev.pop(positions=[pos])

    def discard_bad_entanglement(self, fidelity_threshold=0., excluded_positions="default"):
        if self.discarding_strategy is None:
            return
        if excluded_positions == "default":
            excluded_positions = []
        to_be_discarded = []
        for pos, link in self._links.items():
            if self.discarding_strategy.should_discard(link=link) and\
                    pos not in excluded_positions:
                to_be_discarded.append(pos)
        for pos in to_be_discarded:
            self.discard(pos=pos)

    def estimate_remaining_lifetime(self, pos):
        link = self.getLink(pos=pos)
        self._update_estimated_fidelity(link=link)
        # TODO estimate lifetime given its estimated_fidelity
        # and the T1 and T2 time, and whether the position is the electron_position
        # (get T1 and T2 for both carbon and electron from self.procDev)
        return None
