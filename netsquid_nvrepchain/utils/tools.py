from netsquid.qubits.operators import X, Y, Z
from netsquid.components.instructions import INSTR_X, INSTR_Y, INSTR_Z
from netsquid import pydynaa
from netsquid.qubits.ketstates import b00, b01, b10, b11
import numpy as np


###################
# Bell state tools
###################

COR_PAULI_TO_OPERATOR = {1: X, 2: Y, 3: Z}  # the identity gate is omitted on purpose
COR_PAULI_TO_INSTRUCTION = {1: INSTR_X, 2: INSTR_Y, 3: INSTR_Z}


def commutes_with_Z(pauli_index):
    return pauli_index == 0 or pauli_index == 3


def commutes_with_X(pauli_index):
    return pauli_index == 0 or pauli_index == 1


def conjugate_with_hadamard(pauli_index):
    if not (pauli_index in [0, 1, 2, 3]):
        raise ValueError
    if pauli_index == 0 or pauli_index == 2:
        return pauli_index
    elif pauli_index == 1:
        return 3
    elif pauli_index == 3:
        return 1
    else:
        raise ValueError


def bell_index_to_stabilizer_coefficients(bell_index):
    r"""
    Each of the four Bell states can be written using two generators of their
    stabilizer group: :math:`a \cdot X \otimes X` and :math:`b \cdot Z \otimes Z`
    where :math:`X` and :math:`Z` are the X- and Z-pauli-matrices, respectively,
    and :math:`a, b` take values {+1, -1}.

    Parameters
    ----------
    bell_index : int
        Translation: 0=Phi^+, 1=Psi^+, 2=Phi^-, 3=Psi^-

    Returns
    -------
    (int, int)
        The coefficients :math:`(a, b)`.
    """
    if not (bell_index in [0, 1, 2, 3]):
        raise ValueError("Bell index not among 0, 1, 2, 3")
    else:
        if bell_index == 0:
            return (1, 1)
        elif bell_index == 1:
            return (1, -1)
        elif bell_index == 2:
            return (-1, 1)
        else:
            return (-1, -1)


def stabilizer_coefficients_to_bell_index(stabilizer_coefficients):
    r"""
    Each of the four Bell states can be written using two generators of their
    stabilizer group: :math:`a \cdot X \otimes X` and :math:`b \cdot Z \otimes Z`
    where :math:`X` and :math:`Z` are the X- and Z-pauli-matrices, respectively,
    and :math:`a, b` take values {+1, -1}.

    Parameters
    ----------
    stabilizer_coefficients: (int, int)
        The coefficients :math:`(a, b)`.


    Returns
    -------
    bell_index : int
        Translation: 0=Phi^+, 1=Psi^+, 2=Phi^-, 3=Psi^-
    """
    if not isinstance(stabilizer_coefficients, tuple) \
            or len(stabilizer_coefficients) != 2:
        raise TypeError(
            "Parameter `stabilizer_coefficients` should be a tuple of length two.")
    (x, z) = stabilizer_coefficients
    if not (x in [-1, 1] and z in [-1, 1]):
        raise ValueError("Both coefficients should be either +1 or -1")
    else:
        if x == 1:
            return 0 if z == 1 else 1
        else:
            return 2 if z == 1 else 3


def apply_Hadamard_on_each_qubit(bell_index):
    (c_X, c_Z) = bell_index_to_stabilizer_coefficients(bell_index)
    # apply hadamards means flipping the coefficients
    return stabilizer_coefficients_to_bell_index((c_Z, c_X))


def apply_Pauli_to_one_side_of_bell_state(bell_index, pauli_index):
    (c_X, c_Z) = bell_index_to_stabilizer_coefficients(bell_index)
    if not commutes_with_X(pauli_index):
        c_X *= -1
    if not commutes_with_Z(pauli_index):
        c_Z *= -1
    return stabilizer_coefficients_to_bell_index((c_X, c_Z))


def bell_index_to_ket_state(bell_index):
    """
    Parameters
    ----------
    bell_index : int
        Translation: 0=Phi^+, 1=Psi^+, 2=Phi^-, 3=Psi^-

    Returns
    -------
    :obj:`~netsquid.qubits.kettools.KetState`
    """
    if not (bell_index in [0, 1, 2, 3]):
        raise ValueError("Bell index not among 0, 1, 2, 3")
    else:
        if bell_index == 0:
            return b00
        elif bell_index == 1:
            return b01
        elif bell_index == 2:
            return b10
        else:
            return b11


def index_of_Pauli_product(Pauli_index_A, Pauli_index_B):
    """
    Returns the index of the product of two single-qubit
    Pauli operators, while ignoring their phase. The
    translation is made as 0=Id, 1=X, 2=Y, 3=Z.

    Example
    -------
    Since X.Y = iZ (with i the imaginary unit),
    calling
    >>> index_of_Pauli_product(1, 2)
    >>> # returns 3

    Parameters
    ----------
    Pauli_index_A : int
    Pauli_index_B : int

    Returns
    -------
    int
    """
    for pauli_index in [Pauli_index_A, Pauli_index_B]:
        if not (pauli_index in [0, 1, 2, 3]):
            raise ValueError("Pauli index not among 0, 1, 2, 3")
    if Pauli_index_A == 0:
        # A = Id so A * B = B
        return Pauli_index_B
    elif Pauli_index_B == 0:
        # B = Id so A * B = A
        return Pauli_index_A
    elif Pauli_index_A == Pauli_index_B:
        # for every Pauli P, we have P^2 = Id
        return 0
    elif Pauli_index_A == 1:
        # A = X so:
        return 3 if Pauli_index_B == 2 else 2
    elif Pauli_index_A == 2 and Pauli_index_B == 3:
        return 1
    else:
        return index_of_Pauli_product(Pauli_index_B, Pauli_index_A)


def pauli_index_to_get_to_psiplus(bell_index):
    if not (bell_index in [0, 1, 2, 3]):
        raise ValueError("Parameter bell_index not among 0, 1, 2, 3")
    if bell_index == 0:
        return 1  # Phi^+ -> X
    elif bell_index == 1:
        return 0  # Psi^+ -> Id
    elif bell_index == 2:
        return 2  # Phi^- -> Y
    else:
        return 3  # Psi^- -> Z


def does_precisely_one_commute_with_Z(pauli_index_A, pauli_index_B):
    does_A_commute = commutes_with_Z(pauli_index_A)
    does_B_commute = commutes_with_Z(pauli_index_B)
    return (does_A_commute and not does_B_commute) or \
           (does_B_commute and not does_A_commute)


def stop_simulation():
    pydynaa.DynAASim().stop()


def _check_direction_correct(direction):
    if direction not in ["L", "R"]:
        raise TypeError


def isOutermostNode(number_of_nodes, index, direction=None):
    if direction is None:
        return True in [isOutermostNode(number_of_nodes=number_of_nodes, index=index, direction=direction)
                        for direction in ["L", "R"]]
    _check_direction_correct(direction=direction)
    if direction == "L":
        return index == 0
    elif direction == "R":
        return index == number_of_nodes - 1


def getNeighbourIndex(number_of_nodes, index, direction="L"):
    _check_direction_correct(direction=direction)
    pm = 1 if direction == "R" else -1
    return None if isOutermostNode(number_of_nodes=number_of_nodes, index=index, direction=direction) else index + pm


def get_possible_directions(number_of_nodes, index):
    directions = []
    for direction in ["L", "R"]:
        if not isOutermostNode(number_of_nodes=number_of_nodes, index=index, direction=direction):
            directions.append(direction)
    return directions


def reversedirection(direction):
    assert(direction in ["L", "R"])
    return "L" if direction == "R" else "R"


def contains_multiple_messages(connection_content):
    return isinstance(connection_content[0], tuple)


def compute_fidelity(dm, reference_ket, squared=False):
    """
    Parameters
    ----------
    dm : numpy array
        Density matrix.
    reference_ket
    squared : bool

    Notes
    -----
    See also `~netsquid.qubits.qubitapi.fidelity`.
    """
    fid = (reference_ket.conj().T @ dm @ reference_ket)[0, 0]
    if not squared:
        fid = np.sqrt(fid)
    return np.real(fid)
