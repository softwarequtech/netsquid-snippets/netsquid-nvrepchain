from netsquid.nodes.node import Node
from netsquid_nv.delft_nvs.delft_nv_2019 import NVParameterSet2019 as entgenparams
from netsquid.components.cchannel import ClassicalChannel
from netsquid_nv.magic_distributor import NVMagicDistributor
from netsquid_nvrepchain.utils.qprocessor_factory_tools import create_QPD
import numpy as np


class NVMagicDistributorWithCallbacks(NVMagicDistributor):
    """
    Subclass of :obj:`~netsquid_nv.magic_distributor.NVMagicDistributor`
    with the changed functionality that callbacks are now node-dependent.
    That is, when entanglement is magically delivered, only the nodes
    involved in the entanglement will be notified.

    Note
    ----
    Only the methods in :obj:`~netsquid_nv.magic_distributor.NVMagicDistributor`
    in which the callbacks occur are altered in this class.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._callbacks = [{}]  # every MD has a dictionary of callbacks

    def add_callback(self, callback, node_id, md_index=0):
        """
        Parameters
        ----------
        callback : callback function with delivery_id as argument
        node_id : ID of the node who adds the callback
        md_index : int (default 0 for when magic distributor has not been merged yet)
            Index of original magic distributor and its callbacks.

        Notes
        -----
        Each callback gets called at every individual node delivery. Thus, for every time
        :meth:`~MagicDistributor.add_delivery` is called, each callback is called once for each node in
        :prop:`~MagicDistributor.nodes`.

        """
        self._callbacks[md_index][node_id] = callback

    def clear_all_callbacks(self):
        """Removes all set callback functions."""
        self._callbacks = [{}]

    def _handle_state_delivery(self, node_delivery, event):

        # BEGIN PART THAT IS DIFFERENT FROM super()._handle_state_delivery ###
        # let the calling node know that a state was created, only perform callbacks for that original MD
        delivery = node_delivery.delivery
        callbacks = self._callbacks[delivery.parameters['md_index']]
        if node_delivery.node_id in callbacks:
            callbacks[node_delivery.node_id](event=event)
        # END PART THAT IS DIFFERENT FROM super()._handle_state_delivery ###

        self._schedule_label_delivery_event(node_delivery=node_delivery)

        # ensure the same delivery will not be performed multiple times
        self._pop_state_delivery(event)


class LineNetwork:
    """
    Bare network of nodes in a chain, connected by classical channels and
    heralded connections.

    Each two adjacent nodes have the following structure:

.. code-block:: text

    +------------------+          class channel             +----------------------+
    |                  |   +------------------------>       |                      |
    |                  |                                    |                      |
    |   Node           |          class channel             |                      |
    |                  |    <------------------------+      |  Node                |
    |                  |                                    |                      |
    |                  |    +-------------------------+     |                      |
    |                  |    |     heralded connection |     |                      |
    +--+------------+--+    +-------------------------+     |                      |
       |QProcessor  |  |                                    ++---+--------------+--+
       |            |  |                                     |   |QProcessor    |
       +------+-----+  |                                     |   |              |
              ^        |                                     |   +------+-------+
              |        |                                     |          ^
              |        |                                     |          |
              |        +----->+                      +<------|          |
              |               |                      |                  |
              |               |                      |                  |
              |               |                      |                  |
              |               v                      v                  |
              |               +----------+-----------+                  |
              |               |                      |                  |
              |               |  Magic Distributor   |                  |
              +---------------+                      +----------------->+
                              |                      |
                              |                      |
                              +----------------------+


    Attributes
    ----------
    number_of_nodes
    qmem_config_file
    indices
    delay
    qnodes
    class_channels_send : dict with keys=[node index] and
                          values=[dict with keys=["L" or "R"] and
                                  values=[ClassicalChannel]]
    class_channels_receive: dict with keys=[node index] and
                            values=[dict with keys=["L" or "R"] and
                                    values=[ClassicalChannel]]
    heralded_connections: dict with keys=[node index] and
                          values=[dict with keys=["L" or "R"] and
                                  values=[MiddleHeraldedFibreConnection]]
    distributor_adaptors : list of
        :obj:`~repchain.network.magic_distributor_adaptor.MagicDistributorAdaptor`.
        Length of the list is precisely the number of
        segments (=number of nodes minus one)
    """

    def __init__(self, number_of_nodes, internode_distance=0.002,
                 tot_num_qubits=4,
                 qmem_config_file=None, qmem_config_params=None,
                 distributor_params=None):
        """
        Parameters
        ----------
        number_of_nodes : int
        internode_distance : float
            Distance between the nodes [kilometers].
        qmem_config_file : str
            Filename of JSON file.
        qmem_config_params : dict with keys=str and values=Any
            Parameters for the configuration of the QuantumProcessor.
            Will be passed on to
            `repchain.utils.qprocessor_factory_tools.create_QPD`
        distributor_params : dict of str as keys and Any as values
            Parameters for the magic entanglement distributor, i.e.
            the :obj:`~netsquid_magic.magic_distributor.MagicDistributor`.

        Note
        ----
        Internode distance is passed as argument to HeraldedConnection, which
        passes it on to :obj:`~netsquid.components.Channel.channel`, which
        computes its delay and assumes its length is in kilometers.
        """
        self.number_of_nodes = number_of_nodes
        self.indices = range(0, self.number_of_nodes)
        self.delay = \
            internode_distance / entgenparams.c * 10 ** 9  # in nanoseconds
        self.qmem_config_file = qmem_config_file
        if qmem_config_params is None:
            qmem_config_params = {}
        self.qmem_config_params = qmem_config_params
        self._distributor_params = distributor_params
        if self._distributor_params is None:
            self._distributor_params = {}
        self.tot_num_qubits = tot_num_qubits

        qmemorys = []
        for index in self.indices:
            qmemorys.append(self._get_single_qmemory(index=index))

        self.qnodes = [Node(name="Qnode{}".format(index),
                            ID=index,
                            qmemory=qmemorys[index])
                       for index in self.indices]

        # the following is an dictionary with as keys node indices and
        # values { direction : connection }
        self.class_channels_send = \
            self._get_chain_channels(channel_cls=ClassicalChannel,
                                     delay=self.delay)
        self.class_channels_receive = {index: {} for index in self.indices}
        for index in range(0, self.number_of_nodes - 1):
            self.class_channels_receive[index]["R"] = \
                self.class_channels_send[index + 1]["L"]
        for index in range(1, self.number_of_nodes):
            self.class_channels_receive[index]["L"] = \
                self.class_channels_send[index - 1]["R"]

#        self.heralded_connections = \
#            self._get_chain_connections(
#                connection_cls=MiddleHeraldedFibreConnection,
#                length=internode_distance,
#                use_time_window=True,
#                q_loss_model=None)

#        for heralded_connection_dict in \
#                list(self.heralded_connections.values()):
#            for heralded_connection in \
#                    list(heralded_connection_dict.values()):
#                heralded_connection.start()
#                heralded_connection.stop()

        # Ensure the internode distance is taken into
        # account in computing detection probability;
        # since detection probability p_det is probability at zero
        # distance

        # for the factor, see NetSquid netsquid.components.model.
        # lossmodels.FibreLossModel, method prob_item_lost
        if "p_det" in self._distributor_params:

            # the photon only travels to the midpoint, which
            # is positioned precisely halfway in between the nodes
            distance_that_photon_travels = internode_distance / 2

            factor = np.power(
                10,
                -1. * distance_that_photon_travels * self._distributor_params["p_loss_lengths_with_conversion"] / 10)
            self._distributor_params["p_det"] *= factor

        # add the delay of the photon travel and the heralding message
        # travel to the cycle time
        if 'cycle_time' in self._distributor_params:
            self._distributor_params["cycle_time"] += self.delay

        self.distributor_adaptors = []
        self._distributors = []
        for node_index in range(self.number_of_nodes - 1):
            distributor = \
                NVMagicDistributorWithCallbacks(
                    nodes=[self.qnodes[node_index], self.qnodes[node_index + 1]],
                    component=None,
                    **self._distributor_params)
            adaptor = distributor
            self.distributor_adaptors.append(adaptor)
            self._distributors.append(distributor)

    def _get_single_qmemory(self, index):
        if self.qmem_config_file is not None:
            raise NotImplementedError("constructing QProcessor from configuration file not supported")
        else:
            qmem = create_QPD(name="QProcessor{}".format(index),
                              tot_num_qubits=self.tot_num_qubits,
                              **self.qmem_config_params)
        qmem.fallback_to_nonphysical = False
        qmem.set_program_fail_callback(
            callback=lambda event, index=index:
                self._raise_error(index=index), index=index, once=False)
        return qmem

    def _raise_error(self, index):
        print('quantumprocessor fail: ID={}'.format(index))
#        raise Exception("QuantumProcessor fail: nodeID={}".format(index))

    def reset(self):
        all_channels = list(self.class_channels_send.values()) + list(self.class_channels_receive.values())
        for channel_dictionary in all_channels:
            for __, channel in channel_dictionary.items():
                channel.reset()
#        for channel_dictionary in list(self.heralded_connections.values()):
#            for __, channel in channel_dictionary.items():
#                channel.channel_A_to_M.reset()
#                channel.channel_B_to_M.reset()
#                channel.channel_M_to_A.reset()
#                channel.channel_M_to_B.reset()
        for distributor in self._distributors:
            distributor.reset()
        for qnode in self.qnodes:
            qnode.qmemory.reset()

    def _get_chain_connections(self, connection_cls, **kwargs):
        """
        Parameters
        ----------
        connection_cls : class that is a child of Connection
        kwargs : (optional) additional arguments passed on to
                 the initialization of `connection_cls`

        Returns
        -------
        List of objects of type `connection_cls`
            Contains the (number of nodes - 1) connections between the nodes.
        """
        connections = {}
        for index in range(0, self.number_of_nodes - 1):
            # all indices expect for the last one
            connection = connection_cls(nodeA=self.qnodes[index],
                                        nodeB=self.qnodes[index + 1],
                                        **kwargs)
            connections[index] = {"R": connection}
        # add a dictionary for the last node
        connections[self.number_of_nodes - 1] = {}
        for index in range(1, self.number_of_nodes):
            # all indices expect for the first one
            connections[index]["L"] = connections[index - 1]["R"]
        return connections

    def _get_chain_channels(self, channel_cls, **kwargs):
        channels = {index: {} for index in self.indices}
        for index in range(0, self.number_of_nodes - 1):
            # all indices expect for the last one
            channels[index]["R"] = \
                channel_cls(name="Channel{}>{}".format(index, index + 1),
                            **kwargs)
        # add a dictionary for the last node
        for index in range(1, self.number_of_nodes):
            # all indices expect for the first one
            channels[index]["L"] = \
                channel_cls(name="Channel{}>{}".format(index, index - 1),
                            **kwargs)
        return channels
