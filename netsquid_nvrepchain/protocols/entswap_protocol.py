from netsquid_nvrepchain.utils.message import getNewMessage
from netsquid_nvrepchain.protocols.subprotocol import SubProtocol
from netsquid_nvrepchain.utils.tools import index_of_Pauli_product
import numpy as np
from netsquid_nvrepchain.protocols.move_protocol import reverse_move_using_CXDirections, magical_move_rotated_target
from netsquid.components.instructions import (
    INSTR_CNOT, INSTR_H, INSTR_MEASURE,
    INSTR_ROT_Z, INSTR_CXDIR, INSTR_ROT_Y,
    INSTR_INIT)
from netsquid.components.qprogram import QuantumProgram
from functools import reduce
from abc import ABCMeta

"""
Protocol for entanglement swapping
"""


class IBellStateMeasurementProgram(QuantumProgram, metaclass=ABCMeta):
    """
    Attributes
    -----------
    outcome : int
           The outcome of the Bell-state-measurement using
           the following translaten: 0=Phi^+, 1=Psi^+, 2=Phi^-, 3=Psi^-.

    Internal working
    ----------------
    A few private attributes:
      * _NAME_OUTCOME_CONTROL : str
      * _NAME_OUTCOME_TARGET : str
      * OUTCOME_TO_BELL_INDEX : dict with keys (int, int) and values int
           Indicates how the two measurement outcomes are related to the
           state that is measured. Its keys are tuples of the two measurement
           outcomes (control, target) and its values is the Bell state index
           using the following translaten: 0=Phi^+, 1=Psi^+, 2=Phi^-, 3=Psi^-.
    """

    default_num_qubits = 2
    _NAME_OUTCOME_CONTROL = "control-qubit-outcome"
    _NAME_OUTCOME_TARGET = "target-qubit-outcome"
    _OUTCOME_TO_BELL_INDEX = {(x, y): None for x in [0, 1] for y in [0, 1]}

    @property
    def get_outcome_as_bell_index(self):
        m_outcome_control = self.output[self._NAME_OUTCOME_CONTROL][0]
        m_outcome_target = self.output[self._NAME_OUTCOME_TARGET][0]
        return self._OUTCOME_TO_BELL_INDEX[(m_outcome_control, m_outcome_target)]


class UnrestrictedBSMProgram(IBellStateMeasurementProgram):
    """
    QuantumProgram that represents a quantum circuit which measures
    in the Bell basis by applying a CNOT and a Hadamard, followed
    by measuring both qubits in the standard basis.
    """

    _OUTCOME_TO_BELL_INDEX = {(0, 0): 0, (0, 1): 1, (1, 0): 2, (1, 1): 3}

    def program(self):
        control, target = self.get_qubit_indices(2)
        self.apply(INSTR_CNOT, [control, target])
        self.apply(INSTR_H, control)
        for (qubit, name) in [(control, self._NAME_OUTCOME_CONTROL),
                              (target, self._NAME_OUTCOME_TARGET)]:
            self.apply(INSTR_MEASURE, qubit, name, inplace=False)
        yield self.run()


class RestrictedBSMProgram(IBellStateMeasurementProgram):
    """ Entanglement distillation (2017), Kalb et al.
    Reverse of fig. 2a"""

    _OUTCOME_TO_BELL_INDEX = {(0, 0): 2, (0, 1): 1, (1, 0): 0, (1, 1): 3}
    IS_MOVE_MAGICAL = False

    def program(self):
        electron, carbon = self.get_qubit_indices(2)
        self.apply(INSTR_ROT_Z, carbon, angle=np.pi / 2)
        self.apply(INSTR_CXDIR, [electron, carbon], angle=np.pi / 2)
        self.apply(INSTR_ROT_Z, carbon, angle=-np.pi / 2)
        self.apply(INSTR_ROT_Y, electron, angle=np.pi / 2)
        self.apply(INSTR_MEASURE, electron, self._NAME_OUTCOME_CONTROL, inplace=False)
        self.apply(INSTR_INIT, electron)
        # map the carbon state onto the electron
        if self.IS_MOVE_MAGICAL:
            magical_move_rotated_target(q_program=self,
                                        control=electron,
                                        target=carbon)
        else:
            reverse_move_using_CXDirections(q_program=self,
                                            control=electron,
                                            target=carbon)
        # We apply an additional hadamard to compensate for the fact that the
        # carbon state lives in a rotates basis (the Hadamard basis instead
        # of the computational basis)
        self.apply(INSTR_H, electron)
        self.apply(INSTR_MEASURE, electron, self._NAME_OUTCOME_TARGET, inplace=False)
        yield self.run(lazy=True)


class EntSwapProtocol(SubProtocol):
    """
    Notes
    -----
      * does NOT inherit from NetSquid's `Protocol` nor from EasySquid's `EasyProtocol`
      * No need to restart when using again on different qubit positions, just call the trigger function
      * The Pauli that needs to be applied by one of the other two nodes
      that is involved in the swap, is sent to the **right** node of these two
      (not the left one).

    Parameters
    ----------
    swap_update_receivers: None or list of int
        Inteded to be used only for the swap-only protocol, in which case
        the swap_update_receivers are the end nodes of the chain.
        Example: for a chain of 4 nodes, swap_update_receivers=[0, 3],
        where 0 and 3 are the node indices of the end nodes.
        If None, then the swap updates are sent to the two nodes that share
        the two links on which the swap acts.
    """

    _BSM_OUTCOME_TO_CORRECTION_PAULI_TO_PSIPLUS = {0: 1, 1: 0, 2: 2, 3: 3}

    def __init__(self, index, qmemory, memmanager, class_channels, swap_update_receivers=None,
                 respect_NV_structure=True, is_move_magical=True):
        super().__init__(name="SWAP",
                         index=index,
                         memmanager=memmanager,
                         class_channels=class_channels,
                         heralded_connections=None)
        if swap_update_receivers is not None:
            if not isinstance(swap_update_receivers, list) or \
                    len(swap_update_receivers) != 2:
                raise TypeError("swap_update_receivers should be list of two node indices")
            for node_id in swap_update_receivers:
                if not isinstance(node_id, int) and node_id >= 0:
                    raise ValueError("Swap update receivers should be int")
        self._swap_update_receivers = swap_update_receivers

        if respect_NV_structure:
            self.prgm = RestrictedBSMProgram()
            self.prgm.IS_MOVE_MAGICAL = is_move_magical
        else:
            self.prgm = UnrestrictedBSMProgram()
        self._qmemory = qmemory
        self._run_time = None  # time that the BSM-QuantumProgram takes
        self.cor_pauli = 0  # only used in case of swap-only

    def start(self):
        self.cor_pauli = 0

    def trigger(self, qmem_posA, qmem_posB):
        """
        Performs entanglement swap on the two qubits on the positions
        in memory as specified in the parameters.

        Parameters
        ----------
        qmem_posA : int
        qmem_posB : int
        """
        # some checks
        assert(qmem_posA != qmem_posB)
        assert(qmem_posA is not None)
        assert(qmem_posB is not None)
        # start the Bell-state measurement
        self._qmemory.set_program_done_callback(callback=self._sendSwapUpdate,
                                                qmem_posA=qmem_posA,
                                                qmem_posB=qmem_posB)
        self._run_time = self._qmemory.execute_program(self.prgm,
                                                       qubit_mapping=[qmem_posA, qmem_posB])

    def _sendSwapUpdate(self, qmem_posA, qmem_posB):
        linkA = self.memmanager.getLink(pos=qmem_posA)
        linkB = self.memmanager.getLink(pos=qmem_posB)

        # compute the correction Pauli
        outcome_as_bell_index = self.prgm.get_outcome_as_bell_index
        cor_pauli = self._BSM_OUTCOME_TO_CORRECTION_PAULI_TO_PSIPLUS[outcome_as_bell_index]
        paulis_to_be_multiplied = [cor_pauli, linkA.cor_Pauli, linkB.cor_Pauli]
        cor_pauli = reduce(index_of_Pauli_product, paulis_to_be_multiplied)

        # Sending SWAP update messages
        if self._swap_update_receivers is None:
            # the swap updates should not be sent to specific nodes,
            # so they will be sent to the holders of the swapped
            # links
            for (link_self, link_remote, pauli) in \
                    [(linkA, linkB, 0), (linkB, linkA, cor_pauli)]:
                self._send_swap_msg(intended_receiver=link_self.remote_nodeID,
                                    new_remote_nodeID=link_remote.remote_nodeID,
                                    linkID=link_self.linkID,
                                    additional_content=[pauli, self._run_time])
        else:
            # the swap updates should be sent to specific nodes
            # NOTE this should only be used in the case of the swap-only
            # protocol. In that case, the swap_update_receivers are
            # the end nodes of the chain and will ignore all information
            # other than the correction Pauli. For this reason, we plug in
            # dummy values for the other elements of the swap update message.
            dummy_value = 142
            for (receiver, pauli) in \
                    zip(self._swap_update_receivers, [0, cor_pauli]):
                self._send_swap_msg(
                    intended_receiver=receiver,
                    new_remote_nodeID=dummy_value,
                    linkID=dummy_value,
                    additional_content=[pauli, self._run_time])

        # updating local memorymanager
        for pos in [qmem_posA, qmem_posB]:
            self.memmanager.discard(pos=pos)
        self._schedule_now(self.evtype_finished)
        self._schedule_now(self.EVT_COLLECT_DATA)

    def _send_swap_msg(self, intended_receiver, new_remote_nodeID,
                       linkID, additional_content):
        swap_msg = getNewMessage(sender=self.index,
                                 header_type="SWAP",
                                 content_type=(linkID, new_remote_nodeID),
                                 additional_content=additional_content,
                                 intended_receiver=intended_receiver)
        direction = "L" if self.index > intended_receiver else "R"
        self.class_channels[direction].send(items=swap_msg)

    def process_incoming_message(self, msg):
        """ Receives SWAP update messages,
            and, if the message specifies so, applies
            a correction Pauli"""
        # get data from message
        cor_pauli = msg.additional_content[0]
        (linkID, new_remote_nodeID) = msg.content

        if self._swap_update_receivers is None:
            # update the (classically tracked) correction Pauli
            pos = self.memmanager.findPos(remote_nodeID=msg.sender,
                                          linkID=linkID)
            link = self.memmanager.getLink(pos=pos)
            updated_cor_pauli = \
                index_of_Pauli_product(link.cor_Pauli, cor_pauli)

            # update the memorymanager
            self.memmanager.setFree(pos=pos)
            self.memmanager.setEntangled(pos=pos,
                                         remote_nodeID=new_remote_nodeID,
                                         cor_Pauli=updated_cor_pauli)
        else:
            # TODO currently, we hardcode that in the swap-only protocol, the
            # end nodes hold a single link, which is located at the position
            # of the electron spin
            self.cor_pauli = index_of_Pauli_product(self.cor_pauli, cor_pauli)
