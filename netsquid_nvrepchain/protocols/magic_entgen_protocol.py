import netsquid as ns
from netsquid_nvrepchain.protocols.subprotocol import SubProtocol


def single_click_midpoint_outcome_to_cor_Pauli(midpoint_outcome):
    PSI_MINUS = 1  # the label of NVMagicDistributor; PSI_PLUS = 0
    (__, midpoint_outcome) = midpoint_outcome
    cor_Pauli = 3 if midpoint_outcome == PSI_MINUS else 0
    return cor_Pauli


class MagicEntGenProtocol(SubProtocol):
    """
      * the rightmost node triggers the production of the qubits
      * "production of the qubits" = the qubits magically pop into existence
      * the production of the qubits and schedulling of the 'finished' event
        type is outsourced to the MagicEntGenDistributor
    """
    _EVTYPE_ENTGEN = \
        ns.EventType("ENTGENMSG", "Message received for entanglement generation")

    def __init__(self, index, memmanager, magic_distributor_L,
                 magic_distributor_R, class_channels,
                 delivery_params=None,
                 midpoint_outcome_to_cor_Pauli_fn=None):
        super().__init__(name="ENTGEN_MAGIC",
                         index=index,
                         memmanager=memmanager,
                         class_channels=class_channels,
                         heralded_connections=None)
        self.magic_distributor_L = magic_distributor_L
        self.magic_distributor_R = magic_distributor_R
        self._evhandler_entgen = ns.EventHandler(self._entanglement_generated)
        for distributor in [self.magic_distributor_L, self.magic_distributor_R]:
            if distributor is not None:
                # TODO currently, this protocol waits for a while to mimic the
                # waiting time for the delay from the midpoint. This functionality
                # is also present in the MagicDistributor and should be used from there
                # instead of making this part of the protocol
                cycle_time = distributor.fixed_delivery_parameters[0]['cycle_time']
                callback_fn = \
                    lambda event, cycle_time=cycle_time, event_type=self._EVTYPE_ENTGEN:\
                    self._schedule_after(cycle_time, event_type)
                distributor.add_callback(callback=callback_fn, node_id=self.index)
        self.delivery_params = delivery_params
        if midpoint_outcome_to_cor_Pauli_fn is None:
            self.midpoint_outcome_to_cor_Pauli_fn = single_click_midpoint_outcome_to_cor_Pauli
        else:
            self.midpoint_outcome_to_cor_Pauli_fn = midpoint_outcome_to_cor_Pauli_fn
        self.reset()

    def start(self):
        self._wait(self._evhandler_entgen,
                   event_type=self._EVTYPE_ENTGEN,
                   entity=self)

        # counters for computing the duration of the protocol
        self._start_time = ns.sim_time()
        self._duration = 0
        self._current_request_data = None

    def trigger(self, remoteID, free_qmem_pos):
        distributor = self._get_distributor(remoteID=remoteID)
        self._start_time = ns.sim_time()
        distributor.add_pair_request(senderID=self.index,
                                     remoteID=remoteID,
                                     pos_sender=free_qmem_pos,
                                     port_sender=None,
                                     delivery_params=self.delivery_params)
        self._current_request_data = (free_qmem_pos, remoteID)

    def _get_distributor(self, remoteID):
        if remoteID > self.index:
            return self.magic_distributor_R
        else:
            return self.magic_distributor_L

    def _entanglement_generated(self, event):
        (free_qmem_pos, remoteID) = self._current_request_data
        if remoteID < self.index:
            distributor = self._get_distributor(remoteID=remoteID)
            midpoint_outcome = distributor.get_label()
            cor_Pauli = self.midpoint_outcome_to_cor_Pauli_fn(midpoint_outcome)
        else:
            cor_Pauli = 0

        # notify the memorymanager of the new pair
        self.memmanager.setEntangled(pos=free_qmem_pos,
                                     remote_nodeID=remoteID,
                                     cor_Pauli=cor_Pauli)

        self._last_handled_pair = (ns.sim_time(), remoteID, free_qmem_pos)
        self._duration += (ns.sim_time() - self._start_time)
        self._schedule_now(self.EVT_COLLECT_DATA)
        self._schedule_now(self.evtype_finished)

    @property
    def last_duration(self):
        return self._duration
