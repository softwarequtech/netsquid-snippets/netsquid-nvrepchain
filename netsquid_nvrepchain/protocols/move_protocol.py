from netsquid_nvrepchain.protocols.subprotocol import SubProtocol
from netsquid.components.qprogram import QuantumProgram
import netsquid_physlayer.quantum_program_library as qProgLib
from netsquid.components.instructions import (
    INSTR_CNOT, INSTR_ROT_Y, INSTR_CXDIR,
    INSTR_ROT_X, INSTR_MEASURE, INSTR_ROT_Z,
    INSTR_SWAP, INSTR_H, INSTR_INIT)
from abc import ABCMeta
import numpy as np


def move_using_three_CNOTs(q_program, control=0, target=1):
    q_program.apply(INSTR_CNOT, [control, target])
    q_program.apply(INSTR_CNOT, [target, control])
    q_program.apply(INSTR_CNOT, [control, target])


def move_using_CXDirections_with_nonphysical_measurement(q_program, control=0,
                                                         target=1, inplace=False):
    qProgLib.move_using_CXDirections(q_program=q_program,
                                     control=control, target=target)
    # finish with a nonphysical measurement of the control qubit;
    # we do this because the qubit will be initialized and thus
    # discarded later on, but by doing so now already we
    # shrink the state size.
    # In addition, the statetapper will pick up the state correctly;
    # without removing the control-qubit state, it will live on without
    # a qubit after its is assigned a new QState by the MagicDistributor;
    # somehow the statetapper does not pick up the state correctly
    # after that and will give false fidelities
    q_program.apply(INSTR_MEASURE, control, physical=False, inplace=inplace)


def reverse_move_using_CXDirections(q_program, control=0, target=1,
                                    inplace=False):
    """
    The reverse of the circuit defined in
    :obj:`~easysquid.qProgramLibrary.move_using_CXDirections`.
    """
    q_program.apply(INSTR_ROT_Y, control, angle=np.pi / 2)
    q_program.apply(INSTR_CXDIR, [control, target], angle=-np.pi / 2)
    q_program.apply(INSTR_ROT_X, control, angle=-np.pi / 2)
    q_program.apply(INSTR_ROT_Z, target, angle=-np.pi / 2)
    q_program.apply(INSTR_CXDIR, [control, target], angle=-np.pi / 2)
    q_program.apply(INSTR_ROT_Z, target, angle=np.pi / 2)
    # finish with a nonphysical measurement of the target qubit;
    # we do this because the qubit will be initialized and thus
    # discarded later on, but by doing so now already we
    # shrink the state size
    # In addition, the statetapper will pick up the state correctly;
    # without removing the control-qubit state, it will live on without
    # a qubit after its is assigned a new QState by the MagicDistributor;
    # somehow the statetapper does not pick up the state correctly
    # after that and will give false fidelities
    q_program.apply(INSTR_MEASURE, target, physical=False, inplace=inplace)


def magical_move(q_program, control=0, target=1):
    q_program.apply(INSTR_SWAP, [control, target])


def magical_move_rotated_target(q_program, control=0, target=1):
    """ maps electron -> carbon"""
    physical = False  # TODO should be 'false'
    # q_program.apply(INSTR_H, target, physical=physical)
    q_program.apply(INSTR_SWAP, [control, target])
    q_program.apply(INSTR_H, target, physical=physical)


def reverse_magical_move_rotated_target(q_program, control=0, target=1):
    """ maps electron -> carbon"""
    physical = False  # TODO should be 'false'
    q_program.apply(INSTR_H, target, physical=physical)
    q_program.apply(INSTR_SWAP, [control, target])
    # q_program.apply(INSTR_H, target, physical=physical)


class MoveProgram(QuantumProgram, metaclass=ABCMeta):
    default_num_qubits = 2
    MOVE_FUNCTION = None

    def __init__(self, num_qubits=None, parallel=True, inplace=False):
        super().__init__(num_qubits=num_qubits,
                         parallel=parallel)
        self.reset()
        self.initialize_target_before = False
        self.inplace = inplace

    def program(self):
        control, target = self.get_qubit_indices(2)
        if self.initialize_target_before:
            self.apply(INSTR_INIT, [target])
        q_program = self
        q_program.MOVE_FUNCTION(control=control,
                                target=target)
        yield self.run(lazy=True)


class ThreeCNOTMoveProgram(MoveProgram):
    MOVE_FUNCTION = move_using_three_CNOTs


class TwoCNOTMoveProgram(MoveProgram):
    """ Maps the target (carbon) state onto the control (electron)"""
    MOVE_FUNCTION = qProgLib.move_using_CNOTs


class CXDirectionMoveProgram(MoveProgram):
    """ Maps the control (electron) state onto the target (carbon), modulo
    a Hadamard gate on the target (carbon)"""
    MOVE_FUNCTION = move_using_CXDirections_with_nonphysical_measurement

    def program(self):
        control, target = self.get_qubit_indices(2)
        if self.initialize_target_before:
            self.apply(INSTR_INIT, [target])
        q_program = self
        q_program.MOVE_FUNCTION(control=control,
                                target=target,
                                inplace=self.inplace)
        yield self.run(lazy=True)


class MagicalMoveProgram(MoveProgram):
    MOVE_FUNCTION = magical_move


class MagicalMoveRotatedTargetProgram(MoveProgram):
    """ Maps the control (electron) state onto the target (carbon)
    and vice versa, modulo a Hadamard gate on
    the target (carbon)"""
    MOVE_FUNCTION = magical_move_rotated_target


class ReverseMagicalMoveRotatedTargetProgram(MoveProgram):
    MOVE_FUNCTION = reverse_magical_move_rotated_target

    def program(self):
        control, target = self.get_qubit_indices(2)
        if self.initialize_target_before:
            self.apply(INSTR_INIT, [control])
        q_program = self
        q_program.MOVE_FUNCTION(control=control,
                                target=target)
        yield self.run()


class ReverseCXDirectionMoveProgram(ReverseMagicalMoveRotatedTargetProgram):
    MOVE_FUNCTION = reverse_move_using_CXDirections

    def program(self):
        control, target = self.get_qubit_indices(2)
        self.apply(INSTR_INIT, [control])
        q_program = self
        q_program.MOVE_FUNCTION(control=control,
                                target=target,
                                inplace=self.inplace)
        yield self.run(lazy=True)


class MoveProtocol(SubProtocol):

    def __init__(self, index, qmemory, memmanager, respect_NV_structure=True, is_move_magical=True):
        """
        Parameters
        ----------
        respect_NV_structure : bool
            See `is_move_magical`.
        is_move_magical : bool
            Determines, together with `respect_NV_structure`, which quantum circuit
            to use for swapping qubits from position.
        """
        super().__init__(name="MOVE",
                         index=index,
                         memmanager=memmanager,
                         class_channels=None,
                         heralded_connections=None)
        self.qmemory = qmemory
        self.respect_NV_structure = respect_NV_structure

        # choose the correct quantum circuits
        if respect_NV_structure:
            if is_move_magical:
                self.prgm = MagicalMoveRotatedTargetProgram()
                self.reverse_prgm = ReverseMagicalMoveRotatedTargetProgram()
            else:
                self.prgm = CXDirectionMoveProgram()
                self.reverse_prgm = ReverseCXDirectionMoveProgram()
        else:
            self.prgm = MagicalMoveProgram()
            self.reverse_prgm = self.prgm

    def trigger(self, old_pos, new_pos):
        """
        Performs a MOVE-operation between memory positions;
        i.e. the qubit on position `old_pos` is swapped with
        the qubit on `new_pos`.

        Note
        ----
        If `respect_NV_structure` is set to True while `is_move_magical`
        is set to False, then the underlying quantum registers are merged
        in this process, which might result in a memory overflow error.
        """
        self.qmemory.set_program_done_callback(
            callback=lambda: self._schedule_now(self.evtype_finished),
            once=True)
        self.memmanager.swap(posA=old_pos, posB=new_pos)
        if old_pos == self.memmanager.communication_position:
            prgm = self.prgm
            control = old_pos
            target = new_pos
        else:
            if self.respect_NV_structure:
                assert(new_pos == self.memmanager.communication_position)
            prgm = self.reverse_prgm
            control = new_pos
            target = old_pos
#            control = old_pos
#            target = new_pos
        prgm.initialize_target_before = self.respect_NV_structure
        self.qmemory.execute_program(prgm, qubit_mapping=[control, target])
