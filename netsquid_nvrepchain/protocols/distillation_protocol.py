"""
This file contains the following classes:
  * `IDistillationProgram`, which represents the interface for
    a quantum program (a circuit) which implements the local operations
    for DEJMPS distillation (http://doi.org/10.1103/PhysRevLett.77.2818)
  * `UnrestrictedDistillationProgram`: the quantum program that
    performs a CNOT, followed by a measurement in Z. This is the
    most straightforward implementation of the local operations
    for the DEJMPS distillation protocol.
  * `RestrictedDistillationProgram`: the quantum program which
    performs a more complex sequence of operations which respect
    the constraints of the NV center.
    Circuit is explained in http://doi.org/10.1126/science.aan0070
  * `LocalDistillationProtocol`:
"""

from netsquid_nvrepchain.protocols.subprotocol import SubProtocol
from netsquid_nvrepchain.utils.message import getNewMessage
from netsquid.components.instructions import (
    INSTR_CNOT, INSTR_MEASURE, INSTR_ROT_Y,
    INSTR_CXDIR, INSTR_ROT_X)
from netsquid.components.qprogram import QuantumProgram
from netsquid_nvrepchain.utils.tools import COR_PAULI_TO_INSTRUCTION, \
    index_of_Pauli_product, does_precisely_one_commute_with_Z
import netsquid as ns
import numpy as np
import abc
import logging


class IDistillationProgram(QuantumProgram, metaclass=abc.ABCMeta):
    """
    Abstract base class for the quantum circuit that implements
    the local operations for entanglement distillation.
    The two qubits that the local operations act upon are
    called 'keep' and 'lose', indicating whether the qubit
    is measured ('lose') or not ('keep').

    The local operations consists of (in this order):
      * pre distillation: applying a correction Pauli to the 'keep' qubit
      * performing a fixed sequence of gates and measurements

    Using the class method `get_correction_Pauli_keep`, one can find out
    the classically-tracked Pauli operator belonging to the 'keep' qubit
    after this protocol has finished.

    Note
    ----
    Takes two qubits.
    Convention: the *first* qubit is always the one that
    is measured (the 'lose' qubit).
    """
    default_num_qubits = 2
    _MEASUREMENT_OUTCOME_NAME = "distillation_outcome"
    _KEEP_MEASURED_QUBIT = False  # for testing purposes

    def __init__(self, num_qubits=None, parallel=True):
        super().__init__(num_qubits=num_qubits,
                         parallel=parallel)
        self.reset()
        self._pre_correction_Pauli = 0

    @property
    def pre_correction_Pauli(self):
        """
        Index of the single-qubit Pauli operator that will
        be applied to qubit 'lose' before the local distillation
        operations (0=Id, 1=X, 2=Y, 3=Z).

        Returns
        -------
        int

        """
        return self._pre_correction_Pauli

    @pre_correction_Pauli.setter
    def pre_correction_Pauli(self, correction_Pauli):
        """
        :param int correction_Pauli: int

        Index of the single-qubit Pauli operator that will
        be applied to qubit 'lose' before the local distillation
        operations (0=Id, 1=X, 2=Y, 3=Z).
        """
        if correction_Pauli not in [0, 1, 2, 3]:
            raise TypeError("Parameters for correction Paulis should indicate\
                             a Pauli matrix (0=Id, 1=X, 2=Y, 3=Z)")
        self._pre_correction_Pauli = correction_Pauli

    @property
    def outcome(self):
        """
        int

        Measurement outcome of the local distillation operations.
        """
        return self.output[self._MEASUREMENT_OUTCOME_NAME][0]

    def program(self):
        super().program()
        q_lose, q_keep = self.get_qubit_indices(2)
        if self._pre_correction_Pauli != 0:
            self.apply(
                COR_PAULI_TO_INSTRUCTION[self._pre_correction_Pauli],
                q_lose)
        return q_lose, q_keep

    @classmethod
    def was_distillation_successful(cls, local_outcome, remote_outcome):
        """
        :param int local_outcome:
        :param int remote_outcome:

        :rtype: bool
        :return:
            Whether the two outcomes correspond to a successfull
            entanglement distillation attempt.

        """
        pass

    @classmethod
    @abc.abstractmethod
    def get_correction_pauli_keep(cls,
                                  is_master,
                                  correction_Pauli_local,
                                  correction_Pauli_remote):
        """
        Compute the correction Pauli for the 'keep' qubit after
        the distillation experiment.

        :param bool is_master:
            Whether the current node is the 'master' of the two nodes
            involved in the distillation.
        :param int correction_Pauli_local:
            Correction Pauli operator of the 'keep' before start of
            the distillation protocol of the node 'self'.
        :param int correction_Pauli_remote:
            Correction Pauli operator of the 'keep' before start of
            the distillation protocol of the remote node.

        :rtype: int
        :return:
            Index of the Pauli operator (0=Id, 1=X, 2=Y, 3=Z).
        """
        # check input
        if not isinstance(is_master, bool):
            raise TypeError("Parameter `is_master` determines if the calling node\
                             is the `master` or `slave` of the two and should\
                             thus be a bool")
        for correction_Pauli in [correction_Pauli_local,
                                 correction_Pauli_remote]:
            if correction_Pauli not in [0, 1, 2, 3]:
                raise TypeError("Parameters for correction Paulis should indicate\
                                 a Pauli matrix (0=Id, 1=X, 2=Y, 3=Z)")

        pass


class UnrestrictedDistillationProgram(IDistillationProgram):
    """
    Quantum program that performs a CNOT, followed by a
    measurement in Z. This is the
    most straightforward implementation of the local operations
    for the DEJMPS distillation protocol.
    """

    def program(self):
        q_lose, q_keep = super().program()
        self.apply(INSTR_CNOT, [q_keep, q_lose])
        self.apply(instruction=INSTR_MEASURE,
                   qubit_indices=q_lose,
                   output_key=self._MEASUREMENT_OUTCOME_NAME,
                   inplace=self._KEEP_MEASURED_QUBIT)
        yield self.run()

    @classmethod
    def was_distillation_successful(cls, local_outcome, remote_outcome):
        return local_outcome == remote_outcome

    @classmethod
    def get_correction_pauli_keep(cls, is_master, correction_Pauli_local,
                                  correction_Pauli_remote):
        super().get_correction_pauli_keep(
            is_master=is_master,
            correction_Pauli_local=correction_Pauli_local,
            correction_Pauli_remote=correction_Pauli_remote)
        if is_master:
            if does_precisely_one_commute_with_Z(
                    correction_Pauli_local,
                    correction_Pauli_remote):
                return 1  # the X Pauli matrix
            else:
                return 0  # the Identity matrix
        return 0  # the Identity matrix


class NVDistillationProgram(IDistillationProgram):
    """
    The set of gates for distillation as found in
    Fig. 1c, circuit 4 (purple) in http://doi.org/10.1126/science.aan0070
    """
    _include_measurement = True

    def program(self):
        q_lose, q_keep = super().program()
        electron = q_lose
        carbon = q_keep
        self.apply(INSTR_ROT_Y, electron, angle=np.pi / 2)
        self.apply(INSTR_CXDIR, [electron, carbon], angle=-np.pi / 2)
        self.apply(INSTR_ROT_X, electron, angle=np.pi / 2)
        if self._include_measurement:
            self.apply(instruction=INSTR_MEASURE,
                       qubit_indices=electron,
                       output_key=self._MEASUREMENT_OUTCOME_NAME,
                       inplace=self._KEEP_MEASURED_QUBIT)
        yield self.run(lazy=True)

    @classmethod
    def was_distillation_successful(cls, local_outcome, remote_outcome, is_lowest_level):
        if is_lowest_level:
            return local_outcome == 0 and remote_outcome == 0
        else:
            return local_outcome == remote_outcome

    @classmethod
    def get_correction_pauli_keep(cls,
                                  is_master,
                                  correction_Pauli_local,
                                  correction_Pauli_remote):
        super().get_correction_pauli_keep(
            is_master=is_master,
            correction_Pauli_local=correction_Pauli_local,
            correction_Pauli_remote=correction_Pauli_remote)
        if is_master:
            if does_precisely_one_commute_with_Z(
                    correction_Pauli_local,
                    correction_Pauli_remote):
                return 2  # the Y Pauli matrix
            else:
                return 0  # the Identity matrix
        return 0  # the Identity matrix


class LocalDistillationProtocol(SubProtocol):
    """
    Performs the DEJMPS distillation protocol locally. That is:

      1. first performs the local operations needed for DEJMPS;
      2. the local operations include a measurement. The measurement
         outcome is sent to the other node performing the distillation;
      3. receives the remote measurement outcome and compares it to
         the measurement outcome of the node itself to determine
         whether the distillation was successful

    Note: operation (3) might occur before, after or simultaneously with (1) or (2).

    This protocol does not actively listen to messages on the classical channel:
    the remote measurement outcome must be passed on to this protocol manually by
    using the function `process_incoming_message`.

    When the protocol has finished (i.e. when steps 1-3 above have finished,
    regardless of whether the distillation succeeded), it updates the memorymanager,
    removes the 'keep' qubits from the QuantumProcessor in case of failure,
    and signals the `evtype_finished` event type.

    Notes
    -----
    The original DEJMPS distillation protocol was presented in:
    http://doi.org/10.1103/PhysRevLett.77.2818
    """

    def __init__(self, index, qmemory, memmanager, class_channels, respect_NV_structure=True):
        """
        Parameters
        ----------
        index : int
            Node ID of the node performing this local protocol.
        qmemory : :obj:`~netsquid.components.qprocessor.QuantumProcessor`
        memmanager : :obj:`~repchain.utils.memory_manager.MemoryManager`
        class_channels : dictionary with keys=["L" or "R"] and
                         values=
                         [:obj:`~netsquid.components.cchannel.ClassicalChannel]
            Classical channels that the measurement outcome will be sent over.
            Note: for sending; the protocol doesn't pick up anything that
            it receives from it!
        respect_NV_structure : bool
            Determines whether a generic (CNOT-measure) quantum circuit is used
            for the local operations of distillation, or one that is tailored
            to the NV center (see
            :obj:`~repchain.protocols.distillation_protocol.
            NVDistillationProgram`).
        """
        super().__init__(name="DIST",
                         index=index,
                         memmanager=memmanager,
                         class_channels=class_channels,
                         heralded_connections=None)
        self._qmemory = qmemory
        if respect_NV_structure:
            self._prgm = NVDistillationProgram()
        else:
            self._prgm = UnrestrictedDistillationProgram()
        self.reset()

    def reset(self):
        super().reset()
        self._myoutcome = None
        self._otheroutcome = None
        self._remote_nodeID = None
        self._pos_keep = None
        self._pos_lose = None
        self._link_keep = None
        self._remote_pauli_index_keep = None
        self._prgm.pre_correction_Pauli = 0
        self._run_time = None  # time that the CNOT + measurement operations takes

    def _add_Pauli_correction_to_program(self):
        """
        Ensuring that pauli_lose * pauli_keep will be
        applied to the `lose`-qubit.
        """
        virtual_pauli_index_lose = \
            self.memmanager.getLink(pos=self._pos_lose).cor_Pauli
        self._link_keep = self.memmanager.getLink(pos=self._pos_keep)
        virtual_pauli_index_keep = self._link_keep.cor_Pauli
        self._prgm.pre_correction_Pauli = \
            index_of_Pauli_product(virtual_pauli_index_lose,
                                   virtual_pauli_index_keep)

    def trigger(self, pos_lose, pos_keep):
        """
        Perform local DEJMPS distillation steps on qubits in position
        `pos_lose` and `pos_keep`, including sending the measurement
        outcome to the remote node.
        Here, `pos_lose` is the qubit that is measured during the
        distillation protocol and thus lost.

        :param int pos_lose:
            Memory position of the qubit that will be measured
            and thus will be lost.
        :param int pos_keep:
            Memory position of the qubit that will *not* be
            measured and will thus *not* be lost.
        """
        if pos_lose == pos_keep:
            raise ValueError("Needs at least two links for distillation")
        # setting variables
        self._pos_keep = pos_keep
        self._pos_lose = pos_lose
        self._remote_nodeID = self.memmanager.getRemoteNodeID(pos_lose)
        logging.info("{}:DIST->{}:(pos:{})".format(self.index, self._remote_nodeID, (pos_lose, pos_keep)))
        # perform the distillation program
        self._add_Pauli_correction_to_program()
        self._qmemory.set_program_done_callback(callback=self._send_info_to_peer)
        self._run_time = self._qmemory.execute_program(self._prgm, qubit_mapping=[pos_lose, pos_keep])
        # TODO also consider QuantumProcessor.get_program_duration

    def _send_info_to_peer(self):
        direction = "L" if self._remote_nodeID < self.index else "R"
        self._myoutcome = self._prgm.outcome
        info = (self._myoutcome, self._link_keep.cor_Pauli)
        msg = getNewMessage(sender=self.index,
                            header_type="DIST",
                            content_type="DISTOUTCOME",
                            additional_content=info,
                            intended_receiver=self._remote_nodeID)
        self.class_channels[direction].send(items=msg)
        self._check_finished()

    def process_incoming_message(self, msg):
        """
        Parses the message containing the distillation outcome
        from the remote node.

        Parameters
        ----------
        msg : :py:class:`~repchain.utils.Message`
            Message containing the distillation outcome.
        """
        assert(msg.isOfType(header_type="DIST", content_type="DISTOUTCOME"))
        (self._otheroutcome, self._remote_pauli_index_keep) = msg.additional_content
        self._check_finished()

    def _check_finished(self):
        if self._myoutcome is not None and self._otheroutcome is not None:
            # data collection
            self._last_handled_pair = (ns.sim_time(), self._remote_nodeID, self._pos_keep)
            self._schedule_now(self.EVT_COLLECT_DATA)
            # update the memorymanager
            is_lowest_level = True if abs(self._remote_nodeID - self.index) == 1 else False
            if isinstance(self._prgm, NVDistillationProgram):
                success = self._prgm.was_distillation_successful(self._myoutcome,
                                                                 self._otheroutcome,
                                                                 is_lowest_level)
            else:
                success = self._prgm.was_distillation_successful(self._myoutcome,
                                                                 self._otheroutcome)
            if success:
                self._update_estimated_fidelity(pos=self._pos_keep, run_time=self._run_time)
                self._update_correction_Pauli_keep()
            else:
                self.memmanager.discard(pos=self._pos_keep)
            self.memmanager.setFree(pos=self._pos_lose)

            # "close" this run of the protocol
            self.reset()
            self._schedule_now(self.evtype_finished)

    def _update_correction_Pauli_keep(self):
        is_master = \
            LocalDistillationProtocol._is_master_updater_for_correction_Pauli(
                pauli_index_self=self._link_keep.cor_Pauli,
                pauli_index_remote=self._remote_pauli_index_keep)

        self._link_keep.cor_Pauli = \
            self._prgm.get_correction_pauli_keep(
                is_master=is_master,
                correction_Pauli_local=self._link_keep.cor_Pauli,
                correction_Pauli_remote=self._remote_pauli_index_keep)

    @staticmethod
    def _is_master_updater_for_correction_Pauli(pauli_index_self, pauli_index_remote):
        """
        Defines a decision on whether this node should take the role of 'master'
        in updating its local correction Pauli or not.

        Note
        ----
        If both input arguments are different, then it is guarantueed that this
        function outputs True for one node and False for its peer.
        If both input arguments are equal, then it is irrelevant what this function
        outputs since ????
        """
        return pauli_index_self >= pauli_index_remote

    def _update_estimated_fidelity(self, pos, run_time=0.):
        link = self.memmanager.getLink(pos=pos)
        # Avoid fidelity decay over time is also applied during the operation
        # link.last_accessed += run_time
        # TODO Apply imperfect gate fidelities
        link.estimated_fidelity = 0.8
