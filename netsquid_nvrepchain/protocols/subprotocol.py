from abc import ABCMeta
import netsquid as ns


class SubProtocol(ns.Entity, metaclass=ABCMeta):

    EVT_COLLECT_DATA = ns.EventType("COLLECT_DATA", "Data should be collected")
    evtype_finished = ns.EventType("FIN", "Subprotocol has finished")

    def __init__(self,
                 name,
                 index,
                 memmanager,
                 class_channels=None,
                 heralded_connections=None):
        super().__init__()
        self.name = name  # just used for logging purposes
        self.index = index
        self.memmanager = memmanager
#        self.evtype_finished = ns.EventType("FIN", "Subprotocol has finished")
        if class_channels is not None:
            self.class_channels = class_channels
        if heralded_connections is not None:
            self.heralded_connections = heralded_connections

    def __str__(self):
        return self._to_string()

    def _to_string(self):
        return "{}_{}".format(self.name, self.uid)

    @property
    def last_handled_pair(self):
        """
        :type: tuple (float, int, int)

        Returns a tuple (time, remote node ID, position of the 'keep' qubit).
        This variable is not used by the protocol and is only present for
        logging purposes.
        """
        return self._last_handled_pair

    def reset(self):
        # for data collection purposes only
        self._last_handled_pair = None

    def trigger(self, **kwargs):
        pass

    def process_incoming_message(self, msg):
        pass
