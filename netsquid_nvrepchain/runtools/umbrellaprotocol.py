"""
Main function in this file is 'create_umbrellaprot', which returns
a :obj:`~repchain.runtools.container.Container` object which is a basket
where subprotocols and logic are connected to the physical network and
to each other.
"""

# the container which will hold everything
from netsquid_nvrepchain.runtools.container import Container

# the four subprotocols
from netsquid_nvrepchain.protocols.entswap_protocol import EntSwapProtocol
from netsquid_nvrepchain.protocols.magic_entgen_protocol import MagicEntGenProtocol
from netsquid_nvrepchain.protocols.distillation_protocol import LocalDistillationProtocol
from netsquid_nvrepchain.protocols.move_protocol import MoveProtocol

# the decision logic
from netsquid_nvrepchain.logic.umbrellalogic import UmbrellaLogic

# memory tracking
from netsquid_nvrepchain.utils.memorymanager import MemoryManager

# miscellaneous
from netsquid_nvrepchain.utils.tools import get_possible_directions


def _register_classical_handler(container, direction, class_channel):
    # TODO clean up
    callback_fn = lambda message, class_channel=class_channel, direction=direction: \
        container['logic'].process_msgs(direction=direction,
                                        msglist=message.items)
    class_channel.ports['recv'].bind_output_handler(callback_fn)


def create_umbrellaprot(index, linenetwork,
                        # logic choices
                        use_swap_only,
                        respect_NV_structure=True,
                        delivery_params="default",
                        distillation_strategy="default",
                        swap_fidelity_threshold=0.8,
                        action_ordering="default",
                        is_move_magical=False,
                        use_two_node_NV=False
                        ):
    """
    :rtype: :obj:`~repchain.runtools.container.Container`
    """

    if delivery_params == "default":
        delivery_params = {'alpha': 0.1}

    container = Container()

    # setting 'elementary' parameters
    container.add_simple_item('index', index)
    container.add_simple_item('number_of_nodes', linenetwork.number_of_nodes)
    container.add_simple_item('qmemory', linenetwork.qnodes[index].qmemory)
    container.add_simple_item('procDev', container['qmemory'])
    container.add_simple_item('num_positions', container['qmemory'].num_positions)
    container.add_simple_item('class_channels', linenetwork.class_channels_send[index])

    # get an arbitrary channel in order to find the
    # delay between sending and receiving of a message
    cchannel = list(container['class_channels'].values())[0]
    container.add_simple_item('sleep_delay', cchannel.delay_mean)
    distributor_L = linenetwork.distributor_adaptors[index - 1] if index != 0 else None
    container.add_simple_item('magic_distributor_L', distributor_L)
    distributor_R = linenetwork.distributor_adaptors[index] if index != container['number_of_nodes'] - 1 else None
    container.add_simple_item('magic_distributor_R', distributor_R)
    container.add_simple_item('respect_NV_structure', True)
    container.add_simple_item('is_move_magical', is_move_magical)
    container.add_simple_item('heralded_connections', None)
    container.add_simple_item('use_swap_only', use_swap_only)
    if use_swap_only:
        receivers = [0, linenetwork.number_of_nodes - 1]
    else:
        receivers = None
    container.add_simple_item('swap_update_receivers', receivers)

    # set logical choices
    container.add_simple_item('discarding_strategy', None)
    container.add_simple_item('distillation_strategy', distillation_strategy)
    container.add_simple_item('swap_fidelity_threshold', swap_fidelity_threshold)
    container.add_simple_item('action_ordering', action_ordering)
    container.add_simple_item('delivery_params', delivery_params)
    container.add_simple_item('use_two_node_NV', use_two_node_NV)

    # add memorymanager
    container.add_cls('memorymanager', MemoryManager)
    container.add_simple_item('memmanager', container['memorymanager'])
    container.add_simple_item('midpoint_outcome_to_cor_Pauli_fn', None)

    # add subprotocols
    container.add_cls('entswapprot', EntSwapProtocol)
    container.add_cls('moveprot', MoveProtocol)
    container.add_cls('distilprot', LocalDistillationProtocol)
    container.add_cls('entgenprot', MagicEntGenProtocol)

    # add logic
    container.add_cls('logic', UmbrellaLogic)
#    container.add_callback_during_start(container['logic'].trigger)

    # ensure the items received on the channels will be
    # forwarded to the logic
    # TODO clean up
    container.add_simple_item('class_channels_receive', linenetwork.class_channels_receive[index])
    for direction in get_possible_directions(number_of_nodes=container['number_of_nodes'], index=index):
        cchannel = container['class_channels_receive'][direction]
        if cchannel is not None:
            fn = lambda container=container, direction=direction, class_channel=cchannel: \
                _register_classical_handler(container, direction, class_channel)
            container.add_callback_during_start(callback_fn=fn)

    return container
