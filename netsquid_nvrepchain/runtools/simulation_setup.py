import logging
from netsquid_nvrepchain.network.linenetwork import LineNetwork
import netsquid as ns
from netsquid_nvrepchain.runtools.umbrellaprotocol import create_umbrellaprot
from netsquid_nvrepchain.utils.tools import index_of_Pauli_product


class SimulationSetup(ns.Entity):
    """
    Class that supports easy setup and running of simulations.

    :param linenetwork_params: dict of keys=str and values=Any
        Dictionary of parameters that will be passed on as input
        to the :obj:`~repchain.network.linenetwork.LineNetwork`
    :param logic_params: dict of keys=str and values=Any
        Dictionary of parameters that will be passed on to the
        method `~repchain.runtools.umbrellaprototocol.create_umbrellaprot`
    """

    def __init__(self, linenetwork_params, logic_params):
        # ns.set_qstate_formalism(ns.QFormalism.DM)

        ns.sim_reset()

        # set up the physical network
        self.ln = LineNetwork(**linenetwork_params)

        # set up the protocols
        self.protocols = []
        for index in range(self.ln.number_of_nodes):
            protocol = create_umbrellaprot(index=index,
                                           linenetwork=self.ln,
                                           **logic_params)
            self.protocols.append(protocol)

        self.datatappers = []
        self._stop_handler = ns.EventHandler(lambda event: self._check_stop())
        self._automatic_stop = False
        self.reset()
        self.simulation_start_times = []

    def reset(self):
        """
        Calls the `reset()` function of:

          * linenetwork
          * umbrellaprotocols
        """
        self.ln.reset()
        for index in self.ln.indices:
            self.protocols[index].reset()
        for datatapper in self.datatappers:
            datatapper.reset()
            datatapper.set_collect_on_as_previous()
        if self._automatic_stop:
            self._register_stop_handler()
        self._number_of_stopped_end_nodes = 0

    def _post_data_gathering(self):
        if self.protocols[0]['use_swap_only']:
            n = self.ln.number_of_nodes - 1
            for node_id, neighbour_id, other_id in \
                    [(0, 1, n),
                     (n, n - 1, 0)]:
                prot = self.protocols[node_id]
                memorymanager = prot['memmanager']
                pos = memorymanager.getAllEntanglementWith(neighbour_id)[0]
                link = memorymanager.getLink(pos)
                link.remote_nodeID = other_id
                entswapprot = prot['entswapprot']
                link.cor_Pauli = \
                    index_of_Pauli_product(entswapprot.cor_pauli,
                                           link.cor_Pauli)

        for datatapper in self.datatappers:
            datatapper.perform_postsimulation_collection()

    def _check_stop(self):
        self._number_of_stopped_end_nodes += 1
        if self._number_of_stopped_end_nodes == 2:
            logging.info('stopping on purpose')
            ns.sim_stop()

    def add_datatapper(self, datatapper):
        self.datatappers.append(datatapper)

    def reset_datatappers(self):
        for datatapper in self.datatappers:
            datatapper.reset()

    def set_automatic_stop(self):
        """
        Makes the node stop automatically once the two end nodes
        in the chain both have stopped (i.e. their `have_stopped`
        properties are both True).
        """
        self._automatic_stop = True
        self._register_stop_handler()

    def _register_stop_handler(self):
        self._wait(self._stop_handler,
                   event_type=self.protocols[0]['logic'].EVT_FINISHED)

    def perform_run(self):
        """
        Calls `netsquid.sim_run()`. After NetSquid has finished
        running, call the `perform_postsimulation_collection`
        method of all
        :obj:`~repchain.runtools.data_collection_tools.DataTapper` objects
        which were added using `add_datatapper`.
        """
        ns.sim_run()
        self._post_data_gathering()

    def _remove_all_qubits(self):
        # This function is only here because
        # NetSquid's sim_reset() function
        # resets all components, but while
        # resetting the quantum processor,
        # first applies T1T2 noise to all qubits,
        # which yields an error since the
        # last_accessed time of those qubits
        # is later than 0, the time to which
        # the netsquid simulator is reset.
        # In order to avoid this, we remove
        # all qubits manually
        for node in self.ln.qnodes:
            for mem_position in node.qmemory.mem_positions:
                mem_position.busy = False
                mem_position.get_qubit(remove=True, skip_noise=True)

    def perform_many_runs(self, number_of_runs, suppress_output=False):
        """
        Calls `perform_run` many times and resets
        both NetSquid and itself afterwards.
        """
        for run_index in range(number_of_runs):
            if not suppress_output:
                print("-run{}/{}-".format(run_index, number_of_runs),
                      end="",
                      flush=True)
            self.simulation_start_times.append(ns.sim_time())
            self.perform_run()
            # print(self.protocols[0]['memorymanager'])
            # print(self.protocols[self.ln.number_of_nodes - 1]['memorymanager'])
            # print('\n\n' + '-' * 100 + '\n\n')
            if run_index != number_of_runs - 1:
                self._remove_all_qubits()
                self.reset()
                ns.sim_reset()
                self.reset()
