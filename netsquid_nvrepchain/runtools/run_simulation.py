import netsquid as ns
from netsquid_nvrepchain.runtools.simulation_setup import SimulationSetup
from netsquid_nvrepchain.utils.repchain_parameter_tools import (
    filter_on_qmem_config_params,
    filter_on_magic_distributor_params)
from netsquid_nv.delft_nvs.delft_nv_2019 import NVParameterSet2019


def run_many_simulations(
        number_of_nodes,
        tot_num_qubits,
        internode_distance,
        electron_init_depolar_prob,
        electron_single_qubit_depolar_prob,
        prob_error_0,
        prob_error_1,
        carbon_init_depolar_prob,
        carbon_z_rot_depolar_prob,
        ec_gate_depolar_prob,
        electron_T1,
        electron_T2,
        carbon_T1,
        carbon_T2,
        prob_dark_count,
        prob_detect_excl_transmission_with_conversion_with_cavities,
        prob_detect_excl_transmission_no_conversion_no_cavities,
        visibility,
        product_tau_decay_delta_w,
        photon_emission_delay,
        p_double_exc,
        p_fail_class_corr,
        coherent_phase,
        std_phase_interferometric_drift,
        initial_nuclear_phase,
        alpha,
        use_swap_only,
        swap_fidelity_threshold,
        p_loss_lengths_with_conversion=NVParameterSet2019().p_loss_lengths_with_conversion,
        p_loss_lengths_no_conversion=NVParameterSet2019().p_loss_lengths_no_conversion,
        distillation_strategy="default",
        respect_NV_structure=True,
        action_ordering="default",
        is_move_magical=False,
        use_two_node_NV=False):
    """
    Returns
    -------
    :obj:`repchain.runtools.simulation_setup.SimulationSetup`

    Intended usage
    --------------
      * Add datatappers to the return object using
        `sim_setup.add_datatapper(datatapper)`
      * Perform runs using
        `sim_setup.perform_many_runs(number_of_runs)`
      * Obtain the datatappers and store their data
    """

    # get a list of all input parameter names
    linenetwork_params, logic_params = group_param_values(**locals())

    ns.set_qstate_formalism(ns.QFormalism.DM)

    # Setup the simulation
    return SimulationSetup(linenetwork_params=linenetwork_params,
                           logic_params=logic_params)


def group_param_values(**kwargs):
    """
    Parameters
    ----------
    see input parameters to `repchain.runtools.run_simulation.run_simulation`.

    Returns
    -------
    tuple dictionaries (str: float or bool)
        linenetwork_params, logic_params
    """

    qmem_config_params = filter_on_qmem_config_params(nv_param_dict=kwargs)
    distributor_params = \
        filter_on_magic_distributor_params(nv_param_dict=kwargs)

    linenetwork_params = {
        "number_of_nodes": kwargs["number_of_nodes"],
        "internode_distance": kwargs["internode_distance"],
        "tot_num_qubits": kwargs["tot_num_qubits"],
        "qmem_config_file": None,
        "qmem_config_params": qmem_config_params,
        "distributor_params": distributor_params}

    # logic_params from
    logic_params = {
        "respect_NV_structure": kwargs["respect_NV_structure"],
        "delivery_params": {"alpha": kwargs["alpha"]},
        "distillation_strategy": kwargs["distillation_strategy"],
        "use_swap_only": kwargs["use_swap_only"],
        "swap_fidelity_threshold": kwargs["swap_fidelity_threshold"],
        "action_ordering": kwargs["action_ordering"],
        "is_move_magical": kwargs["is_move_magical"],
        "use_two_node_NV": kwargs["use_two_node_NV"]}

    return linenetwork_params, logic_params
