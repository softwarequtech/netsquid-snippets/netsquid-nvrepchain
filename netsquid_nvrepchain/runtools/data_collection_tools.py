import numpy as np
import netsquid as ns
from netsquid_nvrepchain.utils.tools import (
    index_of_Pauli_product, apply_Pauli_to_one_side_of_bell_state,
    bell_index_to_ket_state, compute_fidelity)
from netsquid_nvrepchain.utils.memorymanager import ELECTRON_POSITION
from collections import namedtuple
from collections.abc import MutableMapping


class DataTapper(ns.util.datacollector.DataCollector):
    """
    Extension to :obj:`netsquid.util.datacollector.DataCollector`
    which is able to collect information when the simulation has finished.
    Parameters
    ----------
    get_data_function : function
        Function that has 'event' as first parameter and returns a dict.
        Identical to input of :obj:`netsquid.util.datacollector.DataCollector`.
    collect_postsimulation : bool
        Whether also should collect data once the simulation has finished.
        If set to `False`, then identical to the
        :obj:`netsquid.util.datacollector.DataCollector`.
    """

    def __init__(self, get_data_function, name, collect_postsimulation=True):
        super().__init__(get_data_function=get_data_function)
        self.priority = 1
        self.name = name
        self._collect_postsimulation = collect_postsimulation
        self._collect_on_params = []

    def collect_on(self, triggers, combine_rule=None):
        kwargs = {"triggers": triggers, "combine_rule": combine_rule}
        self._collect_on_params.append(kwargs)
        super().collect_on(**kwargs)

    def reset(self):
        pass

    def set_collect_on_as_previous(self):
        super().reset()
        for kwargs in self._collect_on_params:
            super().collect_on(**kwargs)

    def perform_postsimulation_collection(self):
        if self._collect_postsimulation:
            # NOTE the code below is directly copied from
            # netsquid.util.datacollector.DataCollector.callback
            # The reason for not calling this function is that
            # it takes an event as input which should have a source
            data = self._get_data_function(event=None)
            source_name = "entity"
            if data is None:
                return
            elif isinstance(data, MutableMapping):
                if self._include_entity_name:
                    data[self.column_name_entity_name] = source_name
                if self._include_time_stamp:
                    data[self.column_name_time_stamp] = ns.sim_time()
                if len(data) > 0:
                    self._data_buffer.append(data)
            # end copy of code

            # TODO does this work in general, or do we need a
            # general EVT_SIMULATION_FINISHED event type?
            # a hack would be:
            #   event = self._schedule_now(EVT_SIMULATION_FINISHED)
            #   self.get_data_function(event=event)


###############
# "Factories" #
###############


def get_simstoptapper(check_fn):
    """
    Factory method for a datatapper which stops the simulation
    (i.e. it calls `netsquid.sim_stop()` when the evaluation
    of `check_fn` yields True.

    **Note:** the method `collect_on` should still be used in order
    to determine when `check_fn` will be called.

    Parameters
    ----------
    check_fn : function that takes no arguments and returns a bool

    Returns
    -------
    :obj:`repchain.runtools.data_collection_tools.DataTapper`
    """
    def _stop_simulation(event, check_fn=check_fn):
        if check_fn():
            ns.sim_stop
        return {}

    return DataTapper(get_data_function=_stop_simulation,
                      name="SimStopTapper",
                      collect_postsimulation=False)


def get_total_entgen_duration_tapper(sim_setup):
    s = "total_entgen_duration"
    # sum of all durations of 'entgenprot', divided by 2 since every
    # entanglement generation takes 2 nodes, so we shouldn't count
    # the same attempts twice
    get_data_function = lambda event: {s: sum([prot['entgenprot'].last_duration for prot in sim_setup.protocols]) / 2}
    return DataTapper(get_data_function=get_data_function,
                      name="TotalEntGenDurationTapper",
                      collect_postsimulation=True)


def get_waitingtimetapper(collect_postsimulation=True):
    """
    Factory method for a datatapper which records timestamps.

    Returns
    -------
    :obj:`repchain.runtools.data_collection_tools.DataTapper`
    """
    return DataTapper(get_data_function=lambda event: {"time": ns.sim_time()},
                      name="WaitingTimeTapper",
                      collect_postsimulation=collect_postsimulation)


def get_memorytapper(node_index, memorymanager, number_of_nodes):

    def _get_counters_as_dict(node_index, memorymanager, number_of_nodes):
        ret = {}
        for remote_nodeID in [x for x in range(number_of_nodes) if x > node_index]:
            numlinkstring = "TOT_NUMBER_OF_CREATED_LINKS_{}_{}".format(node_index, remote_nodeID)
            if remote_nodeID in memorymanager._num_links:
                val = memorymanager._num_links[remote_nodeID]
            else:
                val = 0
            ret[numlinkstring] = val
        ret["MAX_MEMORY_USAGE_{}".format(node_index)] = memorymanager._max_number_of_pos_in_use
        return ret

    return DataTapper(get_data_function=lambda event, node_index=node_index, memorymanager=memorymanager,
                      number_of_nodes=number_of_nodes:
                      _get_counters_as_dict(node_index=node_index,
                                            memorymanager=memorymanager,
                                            number_of_nodes=number_of_nodes),
                      name="MemoryTapper",
                      collect_postsimulation=True)


def get_dmtapper(memorymanager_A, memorymanager_B,
                 qmemory_A, qmemory_B,
                 node_index_A, node_index_B,
                 collect_postsimulation,
                 apply_Hadamard_if_data_qubit=True):
    """
    Factory method for a datatapper which records the density
    matrix for each bipartite two-qubit state of which one qubit
    lives in `qemory_A` and the other in `qmemory_B`.

    Parameters
    ----------
    memorymanager_A : :obj:`repchain.utils.memorymanager.MemoryManager`
    memorymanager_B : :obj:`repchain.utils.memorymanager.MemoryManager`
    qmemory_A : :obj:`netsquid.components.qprocessor.QuantumProcessor`
    qmemory_B : :obj:`netsquid.components.qprocessor.QuantumProcessor`
    node_index_A : int
    node_index_B : int
    collect_postsimulation : bool
    apply_Hadamard_if_data_qubit : bool

    Returns
    -------
    :obj:`repchain.runtools.data_collection_tools.DataTapper`
    """
    def get_dm(event):
        lambda event: _DMTapperTools._get_states(
            memorymanager_A=memorymanager_A,
            memorymanager_B=memorymanager_B,
            node_index_A=node_index_A,
            node_index_B=node_index_B,
            qmemory_A=qmemory_A,
            qmemory_B=qmemory_B,
            apply_Hadamard_if_data_qubit=apply_Hadamard_if_data_qubit)
    return DataTapper(get_data_function=get_dm,
                      name="DMTapper",
                      collect_postsimulation=collect_postsimulation)


def get_statetapper(memorymanager_A, memorymanager_B,
                    qmemory_A, qmemory_B,
                    node_index_A, node_index_B,
                    collect_postsimulation,
                    apply_Hadamard_if_data_qubit=True):
    """
    DataTapper for collecting not only the density matrix of all links
    shared between two nodes, but also:

      * their memory positions in their respective quantum memories
      * the corresponding Pauli correction operations to make the
        state (in the absence of all noise) become :math:`Psi^+`
      * the time at which the state was created
      * the fidelity of the state with :math:`Psi^+`, after
        applying the Pauli corrections and performing
        the Hadamard rotation on the data qubits
        (the latter only when `apply_Hadamard_if_data_qubit` is `True`).

    Parameters
    ----------
    memorymanager_A : :obj:`repchain.utils.memorymanager.MemoryManager`
    memorymanager_B : :obj:`repchain.utils.memorymanager.MemoryManager`
    qmemory_A : :obj:`netsquid.components.qprocessor.QuantumProcessor`
    qmemory_B : :obj:`netsquid.components.qprocessor.QuantumProcessor`
    node_index_A : int
    node_index_B : int
    collect_postsimulation : bool
    apply_Hadamard_if_data_qubit : bool

    Returns
    -------
    :obj:`repchain.runtools.data_collection_tools.DataTapper`
    """
    def get_state(event):
        infos = _StateTapperTools._get_infos(
            memorymanager_A=memorymanager_A,
            memorymanager_B=memorymanager_B,
            node_index_A=node_index_A,
            node_index_B=node_index_B,
            qmemory_A=qmemory_A,
            qmemory_B=qmemory_B,
            apply_Hadamard_if_data_qubit=apply_Hadamard_if_data_qubit)
        return infos
    return DataTapper(get_data_function=get_state,
                      name="StateTapper",
                      collect_postsimulation=collect_postsimulation)


def get_statetapper_between_fixed_nodes(sim_setup,
                                        node_indices=None,
                                        collect_postsimulation=True,
                                        apply_Hadamard_if_data_qubit=True):
    """
    A statetapper that taps the state of all freshly generated links
    between the nodes with indices specified in `node_indices`.
    The statetapper is not yet  "activated", i.e. its function
    `collect_on` has not yet been called.

    Parameters
    ----------
    sim_setup : :obj:`repchain.simulation_setup.SimulationSetup`
    node_indices : list of int
    collect_postsimulation : bool
    apply_Hadamard_if_data_qubit : bool

    Returns
    -------
    :obj:`repchain.runtools.data_collection_tools.DataTapper`
    """

    # input checks
    if node_indices is None:
        node_indices = [0, sim_setup.ln.number_of_nodes - 1]

    if len(node_indices) != 2:
        raise ValueError
    for node_index in node_indices:
        if not isinstance(node_index, int) or node_index < 0:
            raise ValueError

    # set up the statetapper
    A = node_indices[0]
    B = node_indices[1]
    statetapper = get_statetapper(memorymanager_A=sim_setup.protocols[A]['memmanager'],
                                  memorymanager_B=sim_setup.protocols[B]['memmanager'],
                                  qmemory_A=sim_setup.ln.qnodes[A].qmemory,
                                  qmemory_B=sim_setup.ln.qnodes[B].qmemory,
                                  node_index_A=A,
                                  node_index_B=B,
                                  collect_postsimulation=collect_postsimulation,
                                  apply_Hadamard_if_data_qubit=apply_Hadamard_if_data_qubit)

    # NOTE we do *not* "activate" the statetapper, i.e.
    # we do not calls its function `collect_on`

    return statetapper


class _DMTapperTools:
    """
    Tools for collecting the density matrix between two nodes.
    """

    @staticmethod
    def _get_states(memorymanager_A, memorymanager_B,
                    node_index_A, node_index_B, qmemory_A,
                    qmemory_B, apply_Hadamard_if_data_qubit):
        position_tuples = \
            _DMTapperTools._identify_positions(
                memorymanager_A=memorymanager_A,
                memorymanager_B=memorymanager_B,
                node_index_A=node_index_A,
                node_index_B=node_index_B)
        states = []
        for (pos_A, pos_B) in position_tuples:
            state = \
                _DMTapperTools._position_tuple_to_state(
                    pos_A=pos_A,
                    pos_B=pos_B,
                    qmemory_A=qmemory_A,
                    qmemory_B=qmemory_B,
                    apply_Hadamard_if_data_qubit=apply_Hadamard_if_data_qubit)
            states.append(state)
        return {"states": states}

    @staticmethod
    def _identify_positions(memorymanager_A, node_index_A,
                            memorymanager_B, node_index_B):
        """
        Returns
        -------
        list of tuples (int, int)
            Each tuple `(pos_A, pos_B)` represents a single two-qubit entangled
            link, where `pos_A` (`pos_B`) is the qubit position
            at node A (B).
        """
        positions_at_A = \
            memorymanager_A.getAllEntanglementWith(remote_nodeID=node_index_B)
        positions_at_B = []

        # identify the corresponding memory position on Bs side
        for pos_at_A in positions_at_A:
            link_at_A = memorymanager_A.getLink(pos=pos_at_A)
            pos = memorymanager_B.findPos(remote_nodeID=node_index_A,
                                          linkID=link_at_A.linkID)
            positions_at_B.append(pos)
        return list(zip(positions_at_A, positions_at_B))

    @staticmethod
    def _position_tuple_to_state(pos_A, qmemory_A, pos_B, qmemory_B,
                                 apply_Hadamard_if_data_qubit):
        """
        Returns
        -------
        reduced density matrix
        """
        [qubit_A] = qmemory_A.peek(positions=[pos_A])
        [qubit_B] = qmemory_B.peek(positions=[pos_B])
        reduced_dm = ns.qubits.qubitapi.reduced_dm([qubit_A, qubit_B])

        # TODO should also make deep copy of reduced_dm in order to avoid
        # acting on the original state?

        # make fresh qubits in order not to change the QState
        # of the qubits mid-simulation
        [qA, qB] = ns.qubits.qubitapi.create_qubits(2)
        ns.qubits.qubitapi.assign_qstate(qubits=[qA, qB], qs_repr=reduced_dm)
        if apply_Hadamard_if_data_qubit:
            for (pos, qubit) in [(pos_A, qA), (pos_B, qB)]:
                if pos != ELECTRON_POSITION:
                    ns.qubits.qubitapi.operate(qubit, ns.qubits.operators.H)
        return ns.qubits.qubitapi.reduced_dm([qA, qB])


_STATEINFO_ATTRIBUTES = ['time',
                         'node_index_A',
                         'node_index_B',
                         'memory_position_A',
                         'memory_position_B',
                         'correction_Pauli_A',
                         'correction_Pauli_B',
                         'dm',
                         'fidelity']

_StateInfo = namedtuple('_StateInfo', _STATEINFO_ATTRIBUTES)


def _dm_to_list_of_floats(dm):
    """
    Example
    -------

    >>> import numpy as np
    >>> density_matrix = np.array([[1. + 1.j, 0.], [0., 1. + 1.j]])
    >>> print(_dm_to_list_of_floats(density_matrix))

    [1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0]
    """
    entries = [[np.real(entry), np.imag(entry)] for row in dm for entry in row]
    # flatten the list
    flattened = [val for tuple_list in entries for val in tuple_list]
    return flattened


def _dm_to_dict(dm):
    """
    Example
    -------

    >>> import numpy as np
    >>> dm = np.array([[1+2.j, 3+4.j, 5+6.j, 7+8.j],
    >>>                [11+12.j, 13+14.j, 15+16.j, 17+18.j],
    >>>                [21+22.j, 23+24.j, 25+26.j, 27+28.j],
    >>>                [31+32.j, 33+34.j, 35+36.j, 37+38.j]])
    >>> # output:

    {'dm00r': 1.0, 'dm00i': 2.0, 'dm01r': 3.0, 'dm01i': 4.0, 'dm02r': 5.0,
     'dm02i': 6.0, 'dm03r': 7.0, 'dm03i': 8.0, 'dm10r': 11.0, 'dm10i': 12.0,
     'dm11r': 13.0, 'dm11i': 14.0, 'dm12r': 15.0, 'dm12i': 16.0, 'dm13r': 17.0,
     'dm13i': 18.0, 'dm20r': 21.0, 'dm20i': 22.0, 'dm21r': 23.0, 'dm21i': 24.0,
     'dm22r': 25.0, 'dm22i': 26.0, 'dm23r': 27.0, 'dm23i': 28.0, 'dm30r': 31.0,
     'dm30i': 32.0, 'dm31r': 33.0, 'dm31i': 34.0, 'dm32r': 35.0, 'dm32i': 36.0,
     'dm33r': 37.0, 'dm33i': 38.0}

    """
    ret = {}
    flattened_dm = _dm_to_list_of_floats(dm)
    n = len(flattened_dm)
    if (n % 4) != 0:
        raise Exception("Density matrix should yield list of size 0 mod 4")
    for index in range(n):
        row_index = int(index / 8)
        column_index = int(np.floor((index % 8) / 2))
        ri = 'r' if index % 2 == 0 else 'i'
        namestring = 'dm_{}_{}_{}'.format(row_index, column_index, ri)
        ret[namestring] = flattened_dm[index]
    return ret


def _stateinfo_to_dict(stateinfo):
    ret = {}
    stateinfo_dict = stateinfo._asdict()
    for att in _STATEINFO_ATTRIBUTES:
        val = stateinfo_dict[att]
        if att == 'dm':
            ret.update(_dm_to_dict(val))
        else:
            ret[att] = val
    return ret


class _StateTapperTools(_DMTapperTools):

    @staticmethod
    def _get_infos(memorymanager_A, memorymanager_B, node_index_A,
                   node_index_B, qmemory_A, qmemory_B,
                   apply_Hadamard_if_data_qubit):
        """
        Returns
        -------
        list of _StateInfo objects
        """
        position_tuples = \
            _DMTapperTools._identify_positions(
                memorymanager_A=memorymanager_A,
                node_index_A=node_index_A,
                memorymanager_B=memorymanager_B,
                node_index_B=node_index_B)
        stateinfos = []
        for pos_A, pos_B in position_tuples:
            info = \
                _StateTapperTools._get_info(
                    pos_A=pos_A,
                    memorymanager_A=memorymanager_A,
                    qmemory_A=qmemory_A,
                    pos_B=pos_B,
                    memorymanager_B=memorymanager_B,
                    qmemory_B=qmemory_B,
                    apply_Hadamard_if_data_qubit=apply_Hadamard_if_data_qubit)
            stateinfo = _StateInfo(time=ns.sim_time(),
                                   node_index_A=node_index_A,
                                   node_index_B=node_index_B,
                                   memory_position_A=info[0],
                                   memory_position_B=info[1],
                                   correction_Pauli_A=info[2],
                                   correction_Pauli_B=info[3],
                                   dm=info[4],
                                   fidelity=info[5])
            stateinfos.append(_stateinfo_to_dict(stateinfo))
        first_stateinfo = stateinfos[0] if len(stateinfos) > 0 else {}
        return first_stateinfo

    @staticmethod
    def _get_info(pos_A, memorymanager_A, qmemory_A,
                  pos_B, memorymanager_B, qmemory_B,
                  apply_Hadamard_if_data_qubit):
        """
        Returns
        -------
        tuple (int, int, numpy array, int, float)
            Tuple is (memory position at A, memory position at B,
                      correction pauli at A, correction pauli at B,
                      density matrix,
                      fidelity)
        """

        # get density matrix
        dm = _DMTapperTools._position_tuple_to_state(pos_A=pos_A,
                                                     qmemory_A=qmemory_A,
                                                     pos_B=pos_B,
                                                     qmemory_B=qmemory_B,
                                                     apply_Hadamard_if_data_qubit=apply_Hadamard_if_data_qubit)

        # get correction Paulis
        pauli_A = memorymanager_A.getLink(pos=pos_A).cor_Pauli
        pauli_B = memorymanager_B.getLink(pos=pos_B).cor_Pauli

        # get product of correction Paulis
        pauli_product = index_of_Pauli_product(pauli_A, pauli_B)

        # get reference bell index from correction Paulis
        reference_bell_index = \
            apply_Pauli_to_one_side_of_bell_state(
                bell_index=1,
                pauli_index=pauli_product)

        # compute fidelity
        reference_ket = \
            bell_index_to_ket_state(bell_index=reference_bell_index)
        fidelity = \
            compute_fidelity(
                dm=dm,
                reference_ket=reference_ket,
                squared=True)

        return (pos_A, pos_B, pauli_A, pauli_B, dm, fidelity)
