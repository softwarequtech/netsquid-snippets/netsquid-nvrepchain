from inspect import getfullargspec


class Container:
    """
    Class that acts as a "container", a "basket" where
    objects are collected and find values for the parameters they
    need. It acts like a node and might thus be superfluous.

    There are two types of objects that can be added to the
    Container: parameters (simple objects) and class objects
    (advanced objects). Parameters are for example
    "dark_count_probability" and "index_in_the_chain",
    where class objects are e.g. a MemoryManager or
    LocalDistillationProtocol. The difference is, is that
    class objects need parameters as input, which they find
    in the Container.

    Additional functionality: adding callbacks that are
    called when Container.start() is called.

    Example
    -------

    >>> class MyClass:
    >>>
    >>>     def __init__(self, myparameter):
    >>>
    >>>    self.myparameter = myparameter
    >>>
    >>>    self.container.add_simple_item(name='myparameter', value=3)
    >>>    self.container.add_cls(name='myclass', cls=MyClass)
    >>>    myclass_object = self.container['myclass']
    >>>    print(myclass_object.myparameter)
    >>>    # output: 3
    """

    def __init__(self, items=None):
        """
        :param items: dict with keys=str and values=Any
        """
        if items is not None:
            self._items = items
        else:
            self._items = {}

        self._callbacks = []

    def add_callback_during_start(self, callback_fn):
        """
        :param callback_fn: function that does not take
                            any arguments
        """
        self._callbacks.append(callback_fn)

    def remove_item(self, name):
        """
        :param str name:
        """
        del self._items[name]

    def __setitem__(self, name, value):
        if name in self._items:
            raise ValueError("Name {} already among the items".format(name))
        self._items[name] = value

    def __getitem__(self, name):
        return self._items[name]

    def add_simple_item(self, name, value):
        """
        :param str name:
        :param Any value:
        """
        self[name] = value

    def add_cls(self, name, cls):
        """
        :param str name:
        :param cls: Class
        """

        # find names of the input parameters
        param_names = getfullargspec(cls.__init__).args

        # find values for input parameters
        params = {}
        for param_name in param_names:
            if param_name != 'self':
                params[param_name] = self._items[param_name]

        # initialize
        obj = cls(**params)
        self.add_simple_item(name=name, value=obj)

    def _perform_function_of_all(self, fn_name):
        for name in self._items:
            try:
                method = getattr(self._items[name], fn_name)
                method()
            except Exception:
                pass

    def start(self):
        self._perform_function_of_all(fn_name='start')
        for callback_fn in self._callbacks:
            callback_fn()

    def stop(self):
        self._perform_function_of_all(fn_name='stop')

    def reset(self):
        self.stop()
        self.start()
