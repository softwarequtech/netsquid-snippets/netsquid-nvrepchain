"""
The logic concerns the decisions that a node takes: i.e. whether to perform distillation, entanglement swapping, sending
classical messages, and so forth.

The logic of a node is split up in four parts:

* an *ActionGenerator*, the core part (file `actiongenerator.py`). The ActionGenerator decides which of the
  following three actions a node will perform: entanglement swapping, entanglement distillation or generating
  fresh entanglement with an adjacent node. These actions represent high-level decisions of the form "entangle
  with remote node 23" or "perform distillation on the entanglement on qubits on memory positions 5 and 19".
  The ActionGenerator should be queried as soon as the node is idle.
* methods that *split up the action into multiple tasks* (file `task_divider_functions.py`). Tasks can either
  be actions with more details (such as "before performing this task, wait for a classical message that confirms
  that this task can indeed be performed") or are additional operations such as classical message sending or
  sleeping (i.e. remain idle for a given amount of time).
* the *TaskExecutor* (file `taskexecutor.py`, which holds a list of tasks (a "to-do-list") which it performs
  sequentially. Tasks can be added to the list during runtime.
* a mailbox, i.e. a list of classical messages that were sent to this node. This mailbox is part of the
  `UmbrellaLogic` class (file `umbrellalogic.py`), which can be seen as a container or basket that holds the
  ActionGenerator and TaskExecutor and converts actions (obtained by querying the ActionGenerator together with
  the mailbox content) into tasks (that are performed by the TaskExecutor.

Actions and tasks follow a fixed format (interface) as specified in `task.py`.

Additional tools
----------------
The UmbrellaLogic and ActionGenerator can employ different strategies to determine which of the three actions to perform
next. Some of those strategies are put in the following separate modules:

* strategies for discarding entanglement once it is believed its fidelity is too low (file `discarding_strategy.py`)
* strategies for determining which entangled links to use for distillation (file `distillation_scheduling.py`)
* an ActionGenerator that holds two NV-centers intead of one (file `twoNVactiondirector.py`)
"""
