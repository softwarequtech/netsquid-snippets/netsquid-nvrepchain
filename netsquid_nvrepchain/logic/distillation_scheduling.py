from abc import ABCMeta


class DistillationSchedulingStrategy(metaclass=ABCMeta):
    """
    Class that represents a strategy to decide which two or more
    links from a larger set of links to use for distillation.

    Usage: initiate and use the method `choose_links_to_distill`.
    """

    def __init__(self, lower_threshold=0.5, upper_threshold=0.8):
        """
        Parameters
        ----------
        lower_threshold : float
            The fidelity threshold below which links are considered
            of such bad quality, that they are not worth any further
            processing. These links will not be selected for distillation.
        upper_threshold : float
            The fidelity threshold above which links no
            longer need distillation.
        """
        self.lower_threshold = lower_threshold
        self.upper_threshold = upper_threshold

    def choose_links_to_distill(self, links):
        """
        Parameters
        ----------
        links : list of :obj:`~repchain.util.memorymanager.Link`
            List of links from which two are chosen for distillation.

        Returns
        -------
        List of links.
            The two links that should be used for distillation. The number
            of items in this list is two for e.g. the DEJMPS protocol,
            but might be more.

        Note
        ----
            It is assumed that all links are links between the same nodes
            (this is not checked!)
        """
        interesting_links = [link for link
                             in links if self.lower_threshold <= link.estimated_fidelity < self.upper_threshold]
        return interesting_links


class GreedyDistillationStrategy(DistillationSchedulingStrategy,
                                 metaclass=ABCMeta):

    def _majorizes(self, fidelityA, fidelityB):
        pass

    def choose_links_to_distill(self, links):
        interesting_links = super().choose_links_to_distill(links=links)
        if len(interesting_links) < 2:
            return []
        else:
            best = interesting_links[0]
            second_best = interesting_links[1]
            for link in interesting_links[2:]:
                if self._majorizes(fidelityA=link.estimated_fidelity,
                                   fidelityB=best.estimated_fidelity):
                    second_best = best
                    best = link
                elif self._majorizes(fidelityA=link.estimated_fidelity,
                                     fidelityB=second_best.estimated_fidelity):
                    second_best = link
                else:
                    continue
            return [best, second_best]


class GreedyTopDownStrategy(GreedyDistillationStrategy):

    def _majorizes(self, fidelityA, fidelityB):
        return fidelityA > fidelityB


class GreedyBottomUpStrategy(GreedyDistillationStrategy):

    def _majorizes(self, fidelityA, fidelityB):
        return fidelityA < fidelityB


class BandedStrategy(DistillationSchedulingStrategy):

    def __init__(self, band_boundaries, lower_threshold=0.5, upper_threshold=0.8):
        """
        Parameters
        ----------
        band_boundaries : list of float
            All within the interval [0, 1].
        upper_threshold : float
            The fidelity threshold above which links no
            longer need distillation.

        Example
        -------
        if `band_boundaries` is [ ???
        """
        super().__init__(lower_threshold=lower_threshold, upper_threshold=upper_threshold)
        assert(len(band_boundaries) > 0)
        boundaries = [0.] + band_boundaries + [1.]
        self._bands = []
        for index in range(len(boundaries) - 1):
            band = (boundaries[index], boundaries[index + 1])
            self._bands.append(band)

    def _within_band(self, fidelityA, fidelityB):
        for (lower, upper) in self._bands:
            if fidelityA >= lower and\
               fidelityB >= lower and\
               fidelityA <= upper and\
               fidelityB <= upper:
                return True
        return False

    def choose_links_to_distill(self, links):
        if len(links) < 2:
            return []
        else:
            for indexA, linkA in enumerate(links):
                for indexB, linkB in enumerate(links):
                    if indexA > indexB and\
                       self._within_band(fidelityA=linkA.estimated_fidelity,
                                         fidelityB=linkB.estimated_fidelity):
                        return [linkA, linkB]
            return []
