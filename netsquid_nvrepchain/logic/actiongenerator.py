from netsquid_nvrepchain.utils.tools import (
    getNeighbourIndex,
    get_possible_directions,
    isOutermostNode)
from netsquid_nvrepchain.utils.bcdzfunctions import get_height, get_ngbh
from netsquid_nvrepchain.logic.distillation_scheduling import \
    GreedyTopDownStrategy
from netsquid_nvrepchain.logic.task import Action
import numpy as np
import netsquid as ns
from netsquid_nvrepchain.utils.memorymanager import ELECTRON_POSITION


class IActionGenerator(ns.Entity):
    """
    Abstract base class for objects that determines the next
    :obj:`~netsquid_nvrepchain.logic.task.Action` that a node should perform.

    Main functionality:
      * the method `get_action` returns the next action
      * the attributes `has_stopped` and `should_stop_fn` can be used to
        determine whether the node has stopped, which implies that
        the action `None` should be returned.
    """

    EVT_FINISHED = ns.EventType("FIN", "No more actions will be returned")

    def __init__(self):
        self._should_stop_fn = None
        self.start()

    @property
    def should_stop_fn(self):
        """
        :type: function that takes no parameters and returns a boolean
        """
        return self._should_stop_fn

    @should_stop_fn.setter
    def should_stop_fn(self, val):
        self._should_stop_fn = val

    @property
    def has_stopped(self):
        """
        :type: bool
        """
        if self._stopped:
            return True
        if self._should_stop_fn is None:
            return False
        else:
            if self._should_stop_fn():
                self._schedule_now(self.EVT_FINISHED)
                self._stopped = True
                return True
            else:
                return False

    def get_action(self):
        """
        :rtype: :obj:`~netsquid_nvrepchain.logic.task.Action` or None
        """
        return None

    def start(self):
        self._stopped = False


class SingleNVActionGenerator(IActionGenerator):
    """
    Decides which of the actions to perform next:

      * entanglement generation ("ENTGEN")
      * distillation ("DIST")
      * swapping ("SWAP")

    Assumes that the node has a single NV center as quantum processor.

    Parameters
    ----------
    number_of_nodes: int
    index: int
        Index of the node in the chain
    distillation_strategy: :obj:`~netsquid_nvrepchain.logic.distillation_strategy.DistillationStrategy`
        The default value `"default"` uses the
        :obj:`~netsquid_nvrepchain.logic.distillation_strategy.GreedyTopDownStrategy`
    swap_fidelity_threshold: float
        If two links reach an estimated fidelity of this threshold, they are eligible for swapping.
    action_ordering: list of str
        Determines the order in which swapping/distilling/entanglement
        generation/checking the mailbox of messages is performed.
        Example: if `action_ordering = ["SWAP", "MSG", "DIST", "ENTGEN"]`,
        then first, it is checked whether it is possible to perform a
        swap ("SWAP") operation, and if so, the corresponding action is
        returned by the `get_action` function. If not, then it is checked
        whether there are old messages ("MSG") which result in an action,
        and if so, then the corresponding action is returned. If not,
        then consider "DIST", followed by considering "ENTGEN".
    """

    def __init__(self, number_of_nodes, index,
                 memorymanager,
                 distillation_strategy="default",
                 swap_fidelity_threshold=0.8,
                 action_ordering="default"):
        super().__init__()
        self._number_of_nodes = number_of_nodes
        self._index = index
        self._memorymanager = memorymanager
        self._swap_fidelity_threshold = swap_fidelity_threshold
        if distillation_strategy == "default":
            self.distillation_strategy = GreedyTopDownStrategy()
        else:
            self.distillation_strategy = distillation_strategy
        self._isOutermostNode = \
            isOutermostNode(number_of_nodes=number_of_nodes,
                            index=index)
        if action_ordering == "default":
            self._action_ordering = ["SWAP", "DIST", "MSG", "ENTGEN"]
        else:
            self._action_ordering = action_ordering
        self._ngbhs = {}
        self._directions = \
            get_possible_directions(number_of_nodes=self._number_of_nodes,
                                    index=self._index)
        for direction in self._directions:
            self._ngbhs[direction] = \
                getNeighbourIndex(number_of_nodes=self._number_of_nodes,
                                  index=self._index,
                                  direction=direction)

        # height of its table
        _height = self._get_height(number_of_nodes=number_of_nodes,
                                   index=index)

        self._is_initiator = (_height == 1)

    def _get_height(self, number_of_nodes, index):
        height = get_height(number_of_nodes=number_of_nodes, index=index)

        # the other nodes this node should watch out for
        self._toplevelngbhs = \
            {direction: int(get_ngbh(number_of_nodes=self._number_of_nodes,
                                     index=self._index,
                                     level=height,
                                     direction=direction,
                                     nonevalue=-1))
             for direction in ["L", "R"]}
        return height

    def get_action(self, first_msg_on_inbox):
        """
        Determines the next action to be performed.
        Should only be called if the node is idle.

        Parameters
        ----------
        first_msg_on_inbox: Message or None
            If None, then this is ignored. See more information
            on message format below.

        Returns
        -------
        tuple (None or :obj:`~netsquid_nvrepchain.logic.task.Action`, bool)
            The action together with a boolean stating whether the
            input message was used.

        **Notes on message formats**

        A DIST-ASK message should look like:
            * "header" : "DIST"
            * "sender" : the sender of the message
            * "intended receiver" : the intended receiver of the message
            * "content" : "ASK"
            * "additional content" : list of two link IDs of the links that
               should be distilled: first the one that will be measured and
               thus lost, and the second which is 'lost' only if distillation
               fails.

        An ENTGEN-ASK message should look like:
            * "header" : "ENTGEN"
            * "sender" : the sender of the message
            * "intended receiver" : the intended receiver of the message
            * "content" : "ASK"
        """
        if self.has_stopped:
            return None, False
        for topic in self._action_ordering:
            action, is_message_used = \
                self._get_next_action_from(
                    topic=topic,
                    first_msg_on_inbox=first_msg_on_inbox)
            if action is not None:
                return action, is_message_used
        return None, False

    def _get_next_action_from(self, topic, first_msg_on_inbox):
        """
        Parameters
        -----------
        topic : str
            Among "SWAP", "DIST", "ENTGEN", "MSG"

        Returns
        -------
        tuple (None or :obj:`~netsquid_nvrepchain.logic.task.Action`, bool)
            The action together with a boolean stating whether the
            input message was used.
        """
        action = None
        is_message_used = False
        if len(self._memorymanager.getAllFree()) < 2:
            action = None
        elif topic == "MSG":
            # Check if there are messages on the agenda
            # and if so, return the corresponding action
            if first_msg_on_inbox is not None:
                # We know that all messages on the message list
                # are ASK-messages, so we just perform the action
                # that it states
                is_message_used = True
                action = \
                    self._get_next_action_from_ask_message(
                        msg=first_msg_on_inbox)
        else:
            if self._isOutermostNode:
                action = None
            else:
                if topic == "SWAP":
                    # check whether can swap at the highest level
                    # and if possible, return the electron as the
                    # first of the two memory positions at which
                    # a swap is possible
                    swap_positions = self._can_swap(
                        preferred_control_position=ELECTRON_POSITION)

                    if swap_positions is not False:
                        action = \
                            Action(topic="SWAP",
                                   parameters={"qmem_posA": swap_positions[0],
                                               "qmem_posB": swap_positions[1]})
                    else:
                        action = None

                elif topic == "DIST":
                    # check whether can distill at the highest level
                    dist = self._can_distill()
                    if dist is not False:
                        action = Action(topic="DIST",
                                        parameters={"pos_lose": dist[0],
                                                    "pos_keep": dist[1],
                                                    "remote_nodeID": dist[2],
                                                    "is_initiator": True})

                elif topic == "ENTGEN":
                    # check whether we should create entanglement
                    ent = self._can_entangle()
                    if ent is not False:
                        action = Action(topic="ENTGEN",
                                        parameters={"remote_nodeID": ent,
                                                    "is_initiator": True})
                    else:
                        action = None
                else:
                    raise ValueError
        return action, is_message_used

    def _get_next_action_from_ask_message(self, msg):
        remote_nodeID = msg.sender
        if msg.isOfHeaderType(header_type="ENTGEN"):
            # TODO this goes wrong if the node does not
            # have any free memories left
            return Action(topic="ENTGEN",
                          parameters={"remote_nodeID": remote_nodeID,
                                      "is_initiator": False})

        elif msg.isOfHeaderType(header_type="DIST"):
            # We identify the corresponding qubit positions of `self`
            # that the distillation request from a remote node is about;
            # the link IDs of the remote node can be found in the message
            [linkID_lose, linkID_keep] = msg.additional_content
            pos_lose = self._memorymanager.findPos(remote_nodeID=remote_nodeID,
                                                   linkID=linkID_lose)
            pos_keep = self._memorymanager.findPos(remote_nodeID=remote_nodeID,
                                                   linkID=linkID_keep)
            return Action(topic="DIST",
                          parameters={"pos_keep": pos_keep,
                                      "pos_lose": pos_lose,
                                      "remote_nodeID": remote_nodeID,
                                      "is_initiator": False})
        else:
            return Exception

    def _can_swap(self, preferred_control_position=None):
        """
        Parameters
        ----------
        preferred_control_position : int or None
            Position that will be returned as the first element
            of the tuple, provided that a swap can be performed
            on this position. Will be ignored if None.

        Returns
        -------
        False or tuple (int, int)
            False in case swapping cannot be done. If swapping *can*
            be done, then returns the tuple, which consists of the two
            qubit memory positions on which the Bell state measurement
            has to be performed.
        """
        left = self._exists_swappable_link(direction="L",
                                           preferred_position=preferred_control_position)
        if left is not False:
            right = self._exists_swappable_link(direction="R",
                                                preferred_position=preferred_control_position)
            if right is not False:
                ret = sorted([left, right])
                if preferred_control_position is not None and\
                        ret[1] is preferred_control_position:
                    return tuple([ret[1], ret[0]])
                else:
                    return tuple(ret)

        return False

    def _can_distill(self):
        """
        Returns
        -------
        False or tuple (int, int, int, int)
            Returns false is distillation is not possible at the highest level.
            Otherwise return (qubit position, qubit position, remote nodeID,
            level at which this distillation occurs).
        """
        left = self._can_distill_with(direction="L")
        return self._can_distill_with(direction="R") if left is False else left

    def _can_entangle(self):
        """
        Check whether entanglement needs to and can be created.

        Returns
        -------
        False or int.
            Return false in case entanglement cannot or should not be
            created. Otherwise returns the ID of the remote node with
            whom to start entangling.
        """
        left = self._can_entangle_with(direction="L")
        return self._can_entangle_with(direction="R") \
            if left is False else left

    def _can_entangle_with(self, direction):
        if not self._is_initiator:
            return False
        else:
            remote_nodeID = self._ngbhs[direction]

            # Check if there already exists entanglement
            # of sufficient quality in this direction:
            has_swappable_links = \
                self._memorymanager.hasEntanglementWithStrictlyMoreThan(
                    number_of_links=0,
                    remote_nodeID=remote_nodeID,
                    fidelity_range=[self._swap_fidelity_threshold, 1.])
            if not has_swappable_links:
                return remote_nodeID
            else:
                return False

    def _exists_swappable_link(self, direction, fidelity_range="default",
                               remote_nodeID="default",
                               preferred_position=None):
        """
        Checks if there exists a link on the highest level in the
        direction of `direction` that has sufficiently large fidelity.

        Parameters
        ----------
        preferred_position : int or None
            Position that will be returned if a swappable link
            exists at this position. If None, then will be ignored.

        Returns
        -------
        False or int
        """
        if remote_nodeID == "default":
            remote_nodeID = self._toplevelngbhs[direction]
        if fidelity_range == "default":
            fidelity_range = [self._swap_fidelity_threshold, 1.]
        has_entanglement_with = \
            self._memorymanager.hasEntanglementWithStrictlyMoreThan(
                number_of_links=0,
                remote_nodeID=remote_nodeID,
                fidelity_range=fidelity_range)
        if has_entanglement_with:
            positions = \
                self._memorymanager.getAllEntanglementWith(
                    remote_nodeID=remote_nodeID,
                    fidelity_range=fidelity_range)
            if preferred_position is not None and \
                    preferred_position in positions:
                return preferred_position
            else:
                return positions[0]
        return False

    def _can_distill_with(self, direction):
        """
        Auxillary method for `can_distill()`.

        Returns
        -------
        False or tuple (int, int, int, int)
            Returns false is distillation is not possible at the highest level.
            Otherwise return (qubit position, qubit position, remote nodeID,
            level at which this distillation occurs).
        """
        remote_nodeID = self._toplevelngbhs[direction]
        links = [self._memorymanager.getLink(pos=pos)
                 for pos in self._memorymanager.getAllEntanglementWith(remote_nodeID=remote_nodeID)]
        links_to_distill = \
            self.distillation_strategy.choose_links_to_distill(links=links)
        if len(links_to_distill) > 0:
            distill_positions = \
                [self._memorymanager.link2pos(link=link)
                    for link in links_to_distill]
            level = \
                int(np.log(np.abs(remote_nodeID - self._index)) / np.log(2.)) + 1
            ret = (distill_positions[0],
                   distill_positions[1],
                   remote_nodeID,
                   level)
            return ret

        else:
            return False
