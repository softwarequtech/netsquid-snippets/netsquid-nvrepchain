from netsquid_nvrepchain.logic.task import Task


def _get_single_move_task(old_position, new_position):
    return Task(topic="MOVE",
                parameters={"old_pos": old_position, "new_pos": new_position})


def _get_move_tasks(old_positions, new_positions):
    """
    Example:
    >>> old_positions = [1, 2, 3]
    >>> new_positions = [4, 5, 6]
    >>> #Now 1 will be mapped to 4, followed by 2 onto 5, followed by 3 onto 6

    Note
    ----
    If two positions are the same, then no MOVE task is returned
    Example:
    >>> old_positions = [1, 2]
    >>> new_positions = [1, 3]
    >>> # will only return a MOVE task of 2 onto 3
    """
    tasks = []
    assert(len(old_positions) == len(new_positions))
    for index, old_position in enumerate(old_positions):
        new_position = new_positions[index]
        if old_position != new_position:
            tasks.append(_get_single_move_task(old_position=old_position,
                                               new_position=new_position))
    return tasks


def _divide_entgen_action_into_tasks(remote_nodeID, entgen_position,
                                     free_position, sleep_delay=None):
    """
    Parameters
    ----------
    entgen_position : int
        The memory position that will be used for entangling with a remote node.
    free_position : int
        A free memory position, not necessarily distinct from `entgen_position`.
    sleep_delay : float or None
        If it is None, then the node is assumed to be an initiator

    Note
    ----
    If `free_position` equals `entgen_position`, then no MOVE task is added; otherwise, a MOVE task
    is added that makes the existing qubit on `entgen_position` move to `free_position`.

    Returns
    -------
    list of Task objects
    """
    is_initiator = (sleep_delay is None)
    expiry_time = None  # TODO add expiry time if this is set
    # 1. possibly add a MOVE task
    tasks = _get_move_tasks(old_positions=[entgen_position],
                            new_positions=[free_position])
    # 2. send a message
    content = "ASK" if is_initiator else "CONFIRM"
    tasks.append(Task(topic="SENDMSG",
                      parameters={"header_type": "ENTGEN",
                                  "content_type": content,
                                  "remote_nodeID": remote_nodeID,
                                  "request_expires_after": expiry_time}))
    # 3. if the node is a responder: wait for a bit so that photon emission is synchronized
    if not is_initiator:
        tasks.append(Task(topic="SLEEP",
                          parameters={"duration": sleep_delay}))

    # 4. start entangling
    tasks.append(Task(topic="ENTGEN",
                      parameters={"free_pos": entgen_position,
                                  "remote_nodeID": remote_nodeID},
                      halt_until_confirm=is_initiator,
                      halt_expires_after=expiry_time))
    return tasks


def _divide_dist_action_into_tasks(position_keep, position_lose,
                                   communication_position,
                                   linkID_keep, linkID_lose,
                                   remote_nodeID, is_initiator,
                                   free_position):
    """
    Maps `communication_position` onto `free_position`
    followed by mapping `position_lose` onto `communication_position`
    (unless the corresponding memory positions are already equal).
    """
    expiry_time = None  # TODO add expiry time if this is set
    # possibly: move
    move_tasks = _get_move_tasks(old_positions=[communication_position, position_lose],
                                 new_positions=[free_position, communication_position])
    # send a message
    sendmsgtask = Task(topic="SENDMSG",
                       parameters={"header_type": "DIST",
                                   "content_type": "ASK" if is_initiator else "CONFIRM",
                                   "remote_nodeID": remote_nodeID,
                                   "request_expires_after": expiry_time,
                                   "additional_content": [linkID_lose, linkID_keep]})

    # 1. & 2. if initiator, then first MOVE, then send a message (vice versa if responder)
    # tasks = move_tasks + [sendmsgtask] if is_initiator else [sendmsgtask] + move_tasks
    tasks = move_tasks + [sendmsgtask]
    # (no need to wait)
    # track where the position_keep went (it might have been at communication_position)
    for move_task in move_tasks:
        if move_task.parameters["old_pos"] == position_keep:
            position_keep = move_task.parameters["new_pos"]
    # 3. start distillation
    distill_task = Task(topic="DIST",
                        parameters={"pos_keep": position_keep,
                                    "pos_lose": communication_position,
                                    "remote_nodeID": remote_nodeID},
                        halt_until_confirm=is_initiator,
                        halt_expires_after=expiry_time)
    # NOTE: on purpose, we don't add `level` as parameter, because I think it is not needed
    return tasks + [distill_task]


def _divide_swap_action_into_tasks(qmem_posA, qmem_posB, communication_position, free_position):
    # possibly: move
    move_tasks = _get_move_tasks(old_positions=[communication_position, qmem_posA],
                                 new_positions=[free_position, communication_position])
    # track where the other position went (it might have been at communication_position)
    for move_task in move_tasks:
        if move_task.parameters["old_pos"] == qmem_posB:
            qmem_posB = move_task.parameters["new_pos"]
    # start swapping
    swap_task = Task(topic="SWAP",
                     parameters={"qmem_posA": communication_position,
                                 "qmem_posB": qmem_posB})
    return move_tasks + [swap_task]


def _determine_positions_involved_in_entgen(memorymanager, respect_NV_structure):
    """
    Returns
    -------
    tuple (int, int)
        Tuple of memory position that will be used for entanglement generation,
        and a free memory position, not necessarily different: these two are
        the same precisely if `respect_NV_structure` is set to False, otherwise
        the free memory position is the position to which the qubit that is
        currently on the entanglement-generation position should be mapped
        onto.
    """
    entgen_position = memorymanager.communication_position
    if respect_NV_structure and\
            not memorymanager.isFree(entgen_position):
        free_position = memorymanager.getRandomFree(num=1,
                                                    excluded=[entgen_position])
    else:
        entgen_position = memorymanager.getRandomFree(num=1, excluded=[])
        free_position = entgen_position
    return entgen_position, free_position


def _determine_positions_involved_in_moving(pos, memorymanager, respect_NV_structure):
    """
    Returns
    -------
    tuple (int, int)
        Communication position, free memory position
    Free memory position is a real free memory position unless ....
    """
    comm_position = memorymanager.communication_position
    if respect_NV_structure:
        if pos != comm_position and\
           not memorymanager.isFree(comm_position):
            # and the electron is already is use
            free_position = memorymanager.getRandomFree(num=1, excluded=[comm_position])
        else:
            free_position = comm_position
    else:
        comm_position = pos
        free_position = comm_position
    return comm_position, free_position


def divide_action_into_tasks(action, sleep_delay, memorymanager, respect_NV_structure=False):
    """
    Parameters
    ----------
    action
    memorymanager
    sleep_delay : float or None
        If the node is an initiator of this action (given as parameter to the action),
        then `sleep_delay` is ignored.
    respect_NV_structure : bool

    Returns
    -------
    list of Task objects
    """
    if action.topic == "ENTGEN":
        remote_nodeID = action.parameters["remote_nodeID"]
        is_initiator = action.parameters["is_initiator"]
        entgen_position, free_position = \
            _determine_positions_involved_in_entgen(memorymanager=memorymanager,
                                                    respect_NV_structure=respect_NV_structure)
        return _divide_entgen_action_into_tasks(remote_nodeID=remote_nodeID,
                                                sleep_delay=None if is_initiator else sleep_delay,
                                                entgen_position=entgen_position,
                                                free_position=free_position)
    #
    elif action.topic == "DIST":
        remote_nodeID = action.parameters["remote_nodeID"]
        is_initiator = action.parameters["is_initiator"]
        position_keep = action.parameters["pos_keep"]
        position_lose = action.parameters["pos_lose"]
        # determine position shuffling
        comm_position, free_position = \
            _determine_positions_involved_in_moving(pos=position_lose,
                                                    memorymanager=memorymanager,
                                                    respect_NV_structure=respect_NV_structure)
        return _divide_dist_action_into_tasks(position_keep=position_keep,
                                              position_lose=position_lose,
                                              communication_position=comm_position,
                                              linkID_keep=memorymanager.getLink(pos=position_keep).linkID,
                                              linkID_lose=memorymanager.getLink(pos=position_lose).linkID,
                                              remote_nodeID=remote_nodeID,
                                              is_initiator=is_initiator,
                                              free_position=free_position)
    #
    elif action.topic == "SWAP":
        comm_position, free_position = \
            _determine_positions_involved_in_moving(pos=action.parameters["qmem_posA"],
                                                    memorymanager=memorymanager,
                                                    respect_NV_structure=respect_NV_structure)
        return _divide_swap_action_into_tasks(qmem_posA=action.parameters["qmem_posA"],
                                              qmem_posB=action.parameters["qmem_posB"],
                                              communication_position=comm_position,
                                              free_position=free_position)
    #
    else:
        raise Exception
