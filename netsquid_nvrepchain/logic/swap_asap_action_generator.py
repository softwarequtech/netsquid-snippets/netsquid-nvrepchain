from netsquid_nvrepchain.logic.actiongenerator import SingleNVActionGenerator


class SwapAsapActionGenerator(SingleNVActionGenerator):

    def __init__(self, number_of_nodes, index, memorymanager):
        super().__init__(number_of_nodes=number_of_nodes,
                         index=index,
                         memorymanager=memorymanager,
                         distillation_strategy=None,
                         swap_fidelity_threshold=0.0,
                         action_ordering=["SWAP", "MSG", "ENTGEN"]
                         )
        self._nodes_on_the_left = [x for x in list(range(number_of_nodes)) if x < index]
        self._nodes_on_the_right = [x for x in list(range(number_of_nodes)) if x > index]

    def get_action(self, first_msg_on_inbox):
        action = None
        was_message_used = False

        if not self._has_swapped:
            action, was_message_used = \
                super().get_action(first_msg_on_inbox=first_msg_on_inbox)
            if action is not None and action.topic == "SWAP":
                self._has_swapped = True

        return action, was_message_used

    def _get_height(self, number_of_nodes, index):
        return 1

    def _exists_swappable_link(self, direction, fidelity_range="default",
                               preferred_position=None):
        nodes = self._nodes_on_the_left if direction == "L" else self._nodes_on_the_right
        for remote_nodeID in nodes:
            exists = super()._exists_swappable_link(
                direction=direction,
                fidelity_range=fidelity_range,
                remote_nodeID=remote_nodeID,
                preferred_position=preferred_position)
            if exists is not False:
                return exists
        return False

    def start(self):
        super().start()
        self._has_swapped = False
