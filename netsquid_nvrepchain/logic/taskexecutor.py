from netsquid import Entity, EventType, EventHandler
from netsquid_nvrepchain.utils.message import getNewMessage


class TaskExecutor(Entity):
    """
    Usage:

    * append tasks to taskexecutor.todolist
    * taskexecutor.start()
    * call taskexecutor.continue_performing_tasks()

    Parameters
    ----------
    index: int
    entswapprot: :obj:`~netsquid_nvrepchain.protocols.entswap_protocol.EntSwapProtocol`
    distilprot: :obj:`~netsquid_nvrepchain.protocols.distillation_protocol.IDistillationProgram`
    entgenprot: :obj:`~netsquid_nvrepchain.protocols.magic_entgen_protocol.MagicEntGenProtocol`
    moveprot: :obj:`~netsquid_nvrepchain.protocols.move_protocol.MoveProtocol`
    class_channels: list of :obj:`~netsquid.components.cchannel.ClassicalChannel`
    """

    EVT_TODOLIST_EMPTY = EventType("TODOLIST_EMPTY", "Todo list is empty")
    _EVT_CHECK_DISCARD_PENDING_TASK = EventType("DISCARD_PENDING", "Check if pending task should be discarded")
    STATUS_IDLE = 0
    STATUS_BUSY = 1

    def __init__(self, index, entswapprot, distilprot, entgenprot, moveprot, class_channels):
        self._index = index
        self._entswapprot = entswapprot
        self._distilprot = distilprot
        self._entgenprot = entgenprot
        self._moveprot = moveprot
        self._class_channels = class_channels
        self._evhandler_treat_next_todo = EventHandler(lambda event: self._treat_next_todo())

        # glue the event types together, so that the taskexecutor
        # is triggered to start a next task every time the previous one is done
        self._EVT_PREVIOUS_TASK_DONE = EventType("PREV_DONE", "Previous task finished")
        self._subprotocols = [self._entswapprot, self._distilprot, self._entgenprot, self._moveprot]
        for subprotocol in self._subprotocols:
            subprotocol.evtype_finished = self._EVT_PREVIOUS_TASK_DONE
        self._reset_parameters()

    def _reset_parameters(self):
        # only for logging purposes
        self._current_task = None
        # the list of tasks that should still be executed
        self.todolist = []
        self._pending_task = None
        self._confirmed_task = None
        # Setting the event handlers that deal with executing tasks, one after another:
        # self._dismiss(self._evhandler_treat_next_todo)

    def start(self):
        """
        Sets the status to 'idle' and moreover makes the
        TaskExecutor wait for the finishing of the four
        subprotocols, and treats the next task on the to-do-list
        when the "finishing event type" is signalled.
        """
        self._wait(self._evhandler_treat_next_todo,
                   event_type=self._EVT_PREVIOUS_TASK_DONE)
        self._status = self.STATUS_IDLE

    def stop(self):
        """
        Reset parameters, set status to 'idle' and
        dismiss event handlers.
        """
        self._reset_parameters()
        self._dismiss(self._evhandler_treat_next_todo)
        self._status = self.STATUS_IDLE

    def reset(self):
        """
        Stop() and start().
        """
        self.stop()
        self.start()

    @property
    def status(self):
        """
        Whether the TaskExecutor is 'idle' or 'busy'.
        """
        return self._status

    def _check_discard_pending_task(self, taskID):
        # whether the pending task should be discarded:
        if (self._pending_task is not None) and\
                taskID == self._pending_task.ID:
            self._pending_task = None
            self._schedule_now(self._EVT_PREVIOUS_TASK_DONE)

    def _set_expiry_handlers(self, task):
        expires_after = task.halt_expires_after
        if expires_after is not None:
            evhandler = \
                EventHandler(lambda event, taskID=task.ID:
                             self._check_discard_pending_task(taskID=taskID))
            self._wait_once(evhandler,
                            entity=self,
                            event_type=self._EVT_CHECK_DISCARD_PENDING_TASK)
            self._schedule_after(expires_after,
                                 self._EVT_CHECK_DISCARD_PENDING_TASK)

    def continue_performing_tasks(self):
        """
        Sets status to 'busy' and perform the tasks on
        the to-do-list.
        """
        self._status = self.STATUS_BUSY
        self._treat_next_todo()

    def _treat_next_todo(self):
        if self.status == self.STATUS_IDLE:
            raise Exception("Node {} has an idle taskexecutor".format(self._index))
        if len(self.todolist) != 0:
            task = self.todolist.pop(0)
            self._current_task = task
            if task.halt_until_confirm:
                # wait until we get a confirmation
                # (so we *don't* set the status back to BUSY)
                self._pending_task = task
                # ... or until the task has expired
                self._set_expiry_handlers(task=task)
            else:
                self._perform_task_now(task=task)
        else:
            self._status = self.STATUS_IDLE
            self._current_task = None
            self._schedule_now(self.EVT_TODOLIST_EMPTY)

    def _perform_task_now(self, task):
        if task.topic == "SWAP":
            self._entswapprot.trigger(qmem_posA=task.parameters["qmem_posA"],
                                      qmem_posB=task.parameters["qmem_posB"])
        elif task.topic == "DIST":
            self._distilprot.trigger(pos_keep=task.parameters["pos_keep"],
                                     pos_lose=task.parameters["pos_lose"])
        elif task.topic == "ENTGEN":
            self._entgenprot.trigger(remoteID=task.parameters["remote_nodeID"],
                                     free_qmem_pos=task.parameters["free_pos"])
        elif task.topic == "MOVE":
            self._moveprot.trigger(old_pos=task.parameters["old_pos"],
                                   new_pos=task.parameters["new_pos"])
        elif task.topic == "SLEEP":
            self._schedule_after(task.parameters["duration"], self._EVT_PREVIOUS_TASK_DONE)
        elif task.topic == "SENDMSG":
            # create an appropriate message
            remote_nodeID = task.parameters["remote_nodeID"]
            header_type = task.parameters["header_type"]
            content_type = task.parameters["content_type"]
            if header_type == "ASK":
                expires_after = task.parameters["request_expires_after"]
            else:
                expires_after = None
            msg = getNewMessage(sender=self._index,
                                header_type=header_type,
                                content_type=content_type,
                                intended_receiver=remote_nodeID,
                                expires_after=expires_after)
            if "additional_content" in task.parameters:
                msg.additional_content = task.parameters["additional_content"]
            direction = "L" if remote_nodeID < self._index else "R"
            self._class_channels[direction].send(items=msg)
            # immediately after sending a message,
            # we can perform any remaining tasks:
            # We could call the following:
            self._schedule_now(self._EVT_PREVIOUS_TASK_DONE)
            # But this is faster:
            # self._treat_next_todo()
        else:
            # should not come here
            raise Exception("Received unknown task type {}".format(task))

    def process_msgs(self, direction, msglist):
        """
        Forward the incoming messages to remote nodes
        or to the correct subprotocol.
        """
        for msg in msglist:
            self._treat_msg(direction, msg)

    def _treat_msg(self, direction, msg):
        if msg.intended_receiver != self._index:
            # message is not meant for this node,
            # so forward it
            forwarddirection = "L" if direction == "R" else "R"
            self._class_channels[forwarddirection].send(items=msg)
        else:
            if msg.isOfContentType("CONFIRM"):
                # assert this is a confirmation to a message we sent out earlier
                assert(msg.header == self._pending_task.topic)
                self._confirmed_task = self._pending_task
                self._pending_task = None
                self._perform_task_now(task=self._confirmed_task)

            elif msg.isOfContentType(content_type="DISTOUTCOME"):
                # message contains outcome of a measurement
                # that is part of the distillation protocol
                self._distilprot.process_incoming_message(msg=msg)
            elif msg.isOfHeaderType(header_type="SWAP"):
                # message is a swap update
                self._entswapprot.process_incoming_message(msg=msg)
            else:
                # code should not reach here
                raise Exception("Unknown message {}".format(msg))
