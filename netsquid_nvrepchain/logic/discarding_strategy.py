from abc import ABCMeta
import netsquid as ns
from netsquid_nvrepchain.utils.bcdzfunctions import get_level


class DiscardingStrategy(metaclass=ABCMeta):

    def should_discard(self, link):
        """
        Returns
        -------
        bool
          Whether the link should be discarded.
        """
        pass


class FixedCutoffTimeDiscardingStrategy(DiscardingStrategy):

    def __init__(self, cutoff_time):
        self.cutoff_time = cutoff_time

    def should_discard(self, link):
        return self._current_time() - link.creation_time > self.cutoff_time

    def _current_time(self):
        return ns.sim_time()


class LevelDependentFidelityBasedDiscardingStrategy(DiscardingStrategy):

    def __init__(self, node_index, level_fidelity_thresholds):
        """
        Parameters
        ----------
        level_fidelities : list of float
            If a link has fidelity smaller than the fidelity
            threshold for the level it lives in, it should be discarded.
            The first element (index 0) of the list holds the level_fidelity
            for neighbouring nodes (i.e. level 1).
        """
        self.node_index = node_index
        self.level_fidelity_thresholds = level_fidelity_thresholds

    def should_discard(self, link):
        level = get_level(index=self.node_index, remote_index=link.remote_nodeID)
        # if nodes are neighbours, then their level is 1, so:
        return link.estimated_fidelity < self.level_fidelity_thresholds[level - 1]
