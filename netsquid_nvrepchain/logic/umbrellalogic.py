from netsquid import EventHandler, Entity
from netsquid_nvrepchain.logic.actiongenerator import IActionGenerator, SingleNVActionGenerator
from netsquid_nvrepchain.logic.odd_initiate_swap_asap_action_generator import OddInitiateSwapAsapActionGenerator
from netsquid_nvrepchain.logic.task_divider_functions import divide_action_into_tasks
from netsquid_nvrepchain.logic.taskexecutor import TaskExecutor
import netsquid as ns
import logging


class UmbrellaLogic(Entity):

    EVT_FINISHED = IActionGenerator.EVT_FINISHED

    def __init__(self, number_of_nodes, index, memorymanager,
                 entswapprot, distilprot, entgenprot,
                 moveprot, class_channels,
                 sleep_delay, respect_NV_structure,
                 use_swap_only,
                 distillation_strategy, swap_fidelity_threshold,
                 action_ordering):
        self.swap_fidelity_threshold = swap_fidelity_threshold
        if use_swap_only:
            self._actiongenerator = OddInitiateSwapAsapActionGenerator(number_of_nodes=number_of_nodes,
                                                                       index=index,
                                                                       memorymanager=memorymanager)
        else:
            self._actiongenerator = SingleNVActionGenerator(number_of_nodes=number_of_nodes,
                                                            index=index,
                                                            memorymanager=memorymanager,
                                                            distillation_strategy=distillation_strategy,
                                                            swap_fidelity_threshold=swap_fidelity_threshold,
                                                            action_ordering=action_ordering)
        self._taskexecutor = \
            TaskExecutor(index=index,
                         entswapprot=entswapprot,
                         distilprot=distilprot,
                         entgenprot=entgenprot,
                         moveprot=moveprot,
                         class_channels=class_channels)
        self._index = index
        self._memorymanager = memorymanager
        self._sleep_delay = sleep_delay
        self._respect_NV_structure = respect_NV_structure
        self._evhandler_trigger = EventHandler(lambda event: self._trigger())

    @property
    def should_stop_fn(self):
        """
        :type: function that takes no parameters and returns a boolean
        """
        return self._actiongenerator.should_stop_fn

    @should_stop_fn.setter
    def should_stop_fn(self, val):
        self._actiongenerator.should_stop_fn = val

    @property
    def has_stopped(self):
        """
        :type: bool
        """
        return self._actiongenerator.has_stopped

    def reset(self):
        """
        Stop() and start().
        """
        self.stop()
        self.start()

    def start(self):
        """
        Query the ActionGenerator for a new action and also do this
        every time once the to-do-list of the TaskExecutor is empty.
        Also starts the ActionGenerator and TaskExecutor.
        """
        self._wait(self._evhandler_trigger,
                   entity=self._taskexecutor,
                   event_type=self._taskexecutor.EVT_TODOLIST_EMPTY)
        self._actiongenerator.start()
        self._taskexecutor.start()
        self._trigger()

    def stop(self):
        """
        Stops the TaskExecutor and dismiss event handlers.
        """
        self._list_of_ask_messages = []
        self._taskexecutor.stop()
        self._dismiss(self._evhandler_trigger)

    def _trigger(self):
        """
        Beware: `trigger` should only be used if the node is idle.
        """
        if self._taskexecutor.status != TaskExecutor.STATUS_IDLE:
            raise Exception("Node {} has a busy taskexecutor".format(self._index))
        self._discard_bad_entanglement(fidelity_threshold=0.5)
        if len(self._list_of_ask_messages) == 0:
            first_msg_on_inbox = None
        else:
            first_msg_on_inbox = self._list_of_ask_messages[0]

        action, is_message_used = self._actiongenerator.get_action(first_msg_on_inbox)
        logging.info("{}: {} gets action {} (is_message_used={})".format(
            ns.sim_time(), self._index, action, is_message_used))
        if is_message_used:
            self._list_of_ask_messages.pop(0)

        if action is not None:
            # get tasks
            tasks = divide_action_into_tasks(action=action,
                                             sleep_delay=self._sleep_delay,
                                             memorymanager=self._memorymanager,
                                             respect_NV_structure=self._respect_NV_structure)
            logging.debug("{} gets tasks {})".format(self._index, ",".join(map(str, tasks))))
            self._taskexecutor.todolist += tasks
            self._taskexecutor.continue_performing_tasks()

    def process_msgs(self, direction, msglist):
        """
        Appends ASK-messages to the internal 'message list'
        and passes all other messages on to the TaskExecutor.
        In case the node is 'idle', also trigger that the
        ActionGenerator is queried for a new action.
        """
        msgs_for_subprotocols = []
        for msg in msglist:
            if msg.intended_receiver == self._index and\
                    msg.isOfContentType(content_type="ASK"):
                self._list_of_ask_messages.append(msg)
            else:
                msgs_for_subprotocols.append(msg)
        if len(msgs_for_subprotocols) != 0:
            self._taskexecutor.process_msgs(direction=direction,
                                            msglist=msgs_for_subprotocols)
        if self._taskexecutor.status == TaskExecutor.STATUS_IDLE:
            self._trigger()

    def _discard_bad_entanglement(self, fidelity_threshold):
        # get the links that are topic of the ASK-messages
        excluded_positions = []
        for msg in self._list_of_ask_messages:
            if msg.isOfType(header_type="DIST", content_type="ASK"):
                for linkID in list(msg.additional_content):
                    pos = self._memorymanager.findPos(remote_nodeID=msg.sender,
                                                      linkID=linkID)
                    if pos is not None:
                        excluded_positions.append(pos)
        # now discard bad entanglement, while excluding the links that
        # were a topic of the ASK-messages
        self._memorymanager.discard_bad_entanglement(fidelity_threshold=fidelity_threshold,
                                                     excluded_positions=excluded_positions)
