class Action:
    """
    Possible topics:
      * "SWAP"
      * "DIST"
      * "ENTGEN"

    If the topic is "SWAP", then `parameters` has keys:
      * `qmem_posA`
      * `qmem_posB`

    If the topic is "DIST", then `parameters` has keys:
      * `pos_lose`
      * `pos_keep`
      * `remote_nodeID`
      * `is_initiator`

    If the topic is "ENTGEN", then `parameters` has keys:
      * `remote_nodeID`
      * `is_initiator`
    """

    def __init__(self, topic, parameters):
        self.topic = topic
        self.parameters = parameters

    def __str__(self):
        return "{}: {}".format(self.topic, self.parameters)


class Task:
    """
    Possible topics:

    * "SWAP"
    * "DIST"
    * "ENTGEN"
    * "SLEEP"
    * "SENDMSG"
    * "MOVE"

    The parameter `halt_until_confirm` indicates that the task, or any following
    task on the todo list, should not be executed until a CONFIRM-message is received
    or until the expiry time (as indicated in `halt_expires_after`) has passed, whichever
    comes first.

    In case `halt_until_confirm` is True, the parameter `halt_expires_after` indicates the time
    after which the "halting until a confirm message comes in" should be overruled by an expiry time:
    after that time, the task is thrown away and not performed. Its default value is None, which
    indicates an infinite expiry time, i.e. the "halting" never expires.

    See also documentation of class `Action`.

    If the topic is "ENTGEN": in addition to the parameters as described in
    the documentation of `Action`, the attribute `parameters` has the key

    * `free_pos`

    If the topic is "SLEEP", then `parameters` has keys:

    * `duration`

    If the topic is "MOVE", then `parameters` has keys:

    * `old_pos`
    * `new_pos`

    If the topic is "SENDMSG", then `parameters` has keys:

    * `header_type`
    * `content_type`
    * `remote_nodeID`
    * `request_expires_after`, which is a float or None (which stands for inifinite expiry time;
      the request never expires). Should only not equal None for "ASK"-messages.
    * `additional_content`, (only for DIST messages, in which case
      the value is a list of two link IDs)
    """

    def __init__(self, topic, parameters, halt_until_confirm=False, halt_expires_after=None):
        self.topic = topic
        self.parameters = parameters
        self.halt_until_confirm = halt_until_confirm
        self.halt_expires_after = halt_expires_after
        self.ID = id(self)

        # some sanity checks
        if topic == "SENDMSG" and halt_until_confirm:
            assert(parameters["content_type"] == "ASK")
#        if "request_expires_after" in self.parameters:
#            if parameters["content_type"] == "ASK":
#                assert(parameters["request_expires_after"] is not None)
#            elif parameters["content_type"] == "CONFIRM":
#                assert(parameters["request_expires_after"] is None)
        if halt_expires_after is not None:
            assert(halt_until_confirm)

    def similarTo(self, other):
        return self.topic == other.topic and\
            self.parameters == other.parameters and\
            self.halt_until_confirm == other.halt_until_confirm

    def __eq__(self, other):
        return self.ID == other.ID

    def __neq__(self, other):
        return not self == other

    def __str__(self):
        return "{}: {}, (halt={})".format(self.topic, self.parameters, self.halt_until_confirm)
