"""

Usage
-----
There are two ways to run this script.

First, one can extract all parameters from a file
```
python3 cli_simulation_runner_with_sql_output.py
        filebasename myoutputfilename $(<example_parameter_values/nonperfect_parameters.dat)
```
or specify all parameters individually
```
python3 cli_simulation_runner_with_sql_output.py filebasename myoutputfilename number_of_runs 10
        number_of_nodes 3 tot_num_qubits 20 ... et cetera

The parameters are `number_of_runs` and in addition all parameters
that should be inputted in the function
`netsquid_nvrepchain.runtools.run_simulation.run_many_simulations`
```
"""
from netsquid_nvrepchain.runtools.run_simulation import run_many_simulations
from netsquid_nvrepchain.utils.command_line_parser import parse_parameters_from_command_line
from netsquid_nvrepchain.runtools.data_collection_tools import (
    get_statetapper_between_fixed_nodes,
    get_waitingtimetapper,
    get_memorytapper,
    get_total_entgen_duration_tapper)
import sqlite3


def add_default_tappers(sim_setup):

    # Add data collection tools: recording fidelity once the protocol has finished
    statetapper = get_statetapper_between_fixed_nodes(sim_setup=sim_setup)
    sim_setup.add_datatapper(statetapper)

    # Ensuring the simulation stops as soon as a single link has been created
    node_index_A = 0
    node_index_B = sim_setup.ln.number_of_nodes - 1
    for (nodeX, nodeY) in [(node_index_A, node_index_B), (node_index_B, node_index_A)]:
        sim_setup.protocols[nodeX]["logic"].should_stop_fn = \
            lambda nodeX=nodeX, nodeY=nodeY: \
            len(sim_setup.protocols[nodeX]["memmanager"].getAllEntanglementWith(nodeY)) > 0
    sim_setup.set_automatic_stop()

    # Add recording of timestampts at which the first (between leftmost and middle node)
    # and second elementary link (between middle and rightmost node) were generated
    for node_index in [0, node_index_B]:
        waiting_time_tapper = get_waitingtimetapper(collect_postsimulation=False)
        entity = sim_setup.protocols[node_index]['entgenprot']
        evtype = entity.evtype_finished
        waiting_time_tapper.collect_on([(entity, evtype)])
        sim_setup.add_datatapper(waiting_time_tapper)

    # add entgen duration tapper
    entduration_tapper = get_total_entgen_duration_tapper(sim_setup=sim_setup)
    sim_setup.add_datatapper(entduration_tapper)

    # add memory tappers
    for node_index in range(sim_setup.ln.number_of_nodes):
        memorymanager = sim_setup.protocols[node_index]["memorymanager"]
        mtapper = \
            get_memorytapper(node_index=node_index,
                             memorymanager=memorymanager,
                             number_of_nodes=sim_setup.ln.number_of_nodes)
        sim_setup.add_datatapper(mtapper)

    return sim_setup.datatappers


if __name__ == "__main__":

    # plugging all input parameters except for number_of_runs into function run_many_simulations
    sim_setup = parse_parameters_from_command_line(
        lambda number_of_runs, filebasename, long_description=None, descr=None, **kwargs:
        run_many_simulations(**kwargs))()

    tappers = add_default_tappers(sim_setup=sim_setup)
    [statetapper, waiting_time_tapper_earlier, waiting_time_tapper_later, total_entgen_duration_tapper] = \
        tappers[0:4]
    memorytappers = tappers[4:]

    # getting number of runs from command line
    number_of_runs = parse_parameters_from_command_line(
        lambda number_of_runs, **kwargs: number_of_runs)()

    # Running the simulations
    sim_setup.perform_many_runs(number_of_runs=number_of_runs)

    # Printing the simulation results to the console
    dataframe = statetapper.dataframe
    filebasename = parse_parameters_from_command_line(lambda filebasename, **kwargs: filebasename)()

    # add parameter values
    parameters = parse_parameters_from_command_line(lambda **kwargs: kwargs)()
    for name, value in parameters.items():
        dataframe[name] = value

    # add a description
    long_description = parse_parameters_from_command_line(
        lambda long_description=None, **kwargs: long_description)()
    short_description = parse_parameters_from_command_line(
        lambda descr=None, **kwargs: descr)()

    dataframe['long_description'] = long_description
    dataframe['cycle_time'] = sim_setup.ln._distributor_params['cycle_time']
    dataframe['descr'] = short_description

    # add memory tapper values

    # TODO we cap the number of memory tappers to store
    # since an SQL database may not have more than approx. 4000 columns
    if sim_setup.ln.number_of_nodes > 5:
        memorytappers = memorytappers[:5]

    index = 0
    mtapper_dataframe = memorytappers[index].dataframe
    for mtapper in memorytappers:
        index += 1
        mtapper_dataframe = mtapper_dataframe.merge(mtapper.dataframe)
#                                    left_index=True,
#                                    right_index=True,
#                                    suffixes=('', '_{}'.format(index)))
    mtapper_dataframe.to_csv(filebasename + '_memory_usage.csv')

    dataframe = dataframe.join(total_entgen_duration_tapper.dataframe['total_entgen_duration'])

    dataframe.to_sql(name='data',
                     con=sqlite3.Connection(filebasename + '.db'))
