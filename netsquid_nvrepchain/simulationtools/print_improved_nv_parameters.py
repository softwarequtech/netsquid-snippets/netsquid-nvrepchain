#!/bin/python
"""
Script which prints an enumerate of improved parameter values to the console, where the ordering is such, that the
names of the parameters are alphabetically ordered

Usage:

```
python3 print_improved_nv_parameters.py include_param_names <0 or 1>
        include_param_values 1 improvement_factor <float>
```
where the <0 or 1> indicates whether the names/values of the parameter should be printed also,
and the <float> is the improvement factor (some number larger than 1).

One can also add "exceptions" to the improvement_factor, which is applied uniformly to all parameters, for example
by doing

```
python3 print_improved_nv_parameters.py include_param_names 1
        include_param_values 1 improvement_factor 2 total_detection_eff 0.2 carbon_init_depolar_prob 0.3
```
which uniformly improves all parameters by 50%, except for
the `total_detection_eff` (by 20%) and `carbon_init_depolar_prob` (by 30%).

"""
from netsquid_simulationtools.parameter_set import rootbased_improvement_fn
from netsquid_nv.delft_nvs.delft_nv_2020_near_term import NVParameterSet2020NearTerm as nvparameters
from netsquid_nvrepchain.utils.command_line_parser import parse_parameters_from_command_line


def _convert_dict_to_flat_list(dictionary, sort_alphabetically=False,
                               include_keys=True, include_vals=True):
    """
    Parameters
    ----------
    dictionary : dict (str: Any)
    sort_alphabetically : bool

    Returns
    -------
    list of str

    Example
    -------

    >>> mydictionary = {"b": 2, "a": 1, "c": 3}
    >>>
    >>> print(_convert_dict_to_flat_list(dictionary=mydictionary,
    >>>                                  sort_alphabetically=False))
    >>> # output: ["b", 2, "a", 1, "c", 3]
    >>>
    >>> print(_convert_dict_to_flat_list(dictionary=mydictionary,
    >>>                                  sort_alphabetically=True))
    >>> # output: ["a", 1, "b", 2, "c", 3]
    >>>
    >>> print(_convert_dict_to_flat_list(dictionary=mydictionary,
    >>>                                  include_keys=False))
    >>> # output: [2, 1, 3]
    >>>
    >>> print(_convert_dict_to_flat_list(dictionary=mydictionary,
    >>>                                  include_vals=False))
    >>> # output: ["b", "a", "c"]
    >>>
    >>> print(_convert_dict_to_flat_list(dictionary=mydictionary,
    >>>                                  include_vals=False,
    >>>                                  include_names=False))
    >>> # output: []
    """
    for key in dictionary:
        if not isinstance(key, str):
            raise TypeError

    if sort_alphabetically:
        sortednames = sorted(dictionary.keys(), key=lambda x: x.lower())
    else:
        sortednames = dictionary.keys()

    ret = []
    for name in sortednames:
        if include_keys:
            ret.append(name)
        if include_vals:
            ret.append(str(dictionary[name]))
    return ret


"""
Idea: the following function should be the only function to use
in the nv_parameter_tools.py file (named something like this).
If you want to do uniform improvement, then just don't add exceptions.
If you want to do uniform improvement, following by changing a
single parameter *at that point*, then just call this function
**twice**.
If you don't want uniform improvement, then just set `uniform_improvement_factor=1`
and use `exceptions` to set everything else.
"""


def improve_all_nv_params_but_some(param_dict,
                                   uniform_improvement_factor=1.0,
                                   exceptions=None,
                                   ignore_perfectless_parameters=False):
    """
    Improves all parameters in `param_dict` with the
    factor provided in `uniform_improvement_factor`,
    unless all parameters that for whom a different improvement
    factor is specified in `exceptions`.
    Only meant for NV parameters.

    Parameters
    ----------
    param_dict : dict (str: Any)
    uniform_improvement_factor : float
    exceptions : dict (str: float)
    ignore_perfectless_parameters : bool

    Returns
    -------
    dict (str: Any)

    Example
    -------

    >>> param_dict = {"carbon_init_depolar_prob": 0.5,
    >>>               "total_detection_eff": 0.2}
    >>> uniform = 2.0
    >>> exceptions = {"carbon_init_depolar_prob": 0.1}
    >>> output = improve_all_nv_params_but_some(
    >>>               param_dict=param_dict,
    >>>               uniform_improvement_factor=uniform,
    >>>               exceptions=exceptions,
    >>>               ignore_perfectless_parameters=True)
    >>> print(output)
    """

    if exceptions is None:
        exceptions = {}

    # set param_improvement dict
    param_improvement_dict = \
        {param: uniform_improvement_factor for param in param_dict}

    for name, perc in exceptions.items():
        if name not in param_dict:
            raise ValueError
        param_improvement_dict[name] = perc

    # use param_imrpvoement_dict
    return nvparameters.to_improved_dict(param_dict=param_dict,
                                         param_improvement_dict=param_improvement_dict,
                                         improvement_fn=rootbased_improvement_fn)


def auxillary_func_CL(improvement_factor=1.0,
                      include_param_names=1,
                      include_param_values=1,
                      ignore_perfectless_parameters=0,
                      **kwargs):
    """
    The 'command-line-friendly' version of `improve_all_nv_params_but_some`

    Returns
    -------
    dictionary (str: float)
    """

    for arg in [include_param_names, include_param_values, ignore_perfectless_parameters]:
        if arg not in [0, 1]:
            raise ValueError

    exceptions = {}
    for key, val in kwargs.items():
        exceptions[str(key)] = float(val)

    d = improve_all_nv_params_but_some(param_dict=nvparameters().to_dict(),
                                       uniform_improvement_factor=improvement_factor,
                                       exceptions=exceptions,
                                       ignore_perfectless_parameters=ignore_perfectless_parameters)

    return _convert_dict_to_flat_list(dictionary=d,
                                      sort_alphabetically=True,
                                      include_keys=bool(include_param_names),
                                      include_vals=bool(include_param_values))


if __name__ == "__main__":
    print_list = parse_parameters_from_command_line(auxillary_func_CL)()
    for element in print_list:
        print(element, end=" ")
